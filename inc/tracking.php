<?php
/**
 * Pixels, bacon, and such
 */
add_action( 'wp_footer', function() {
echo '<script src="http://tags.crwdcntrl.net/c/6442/cc_af.js"></script>';
} );


/**
 * URX.com integration for single pages
 */
add_filter( 'the_content', function( $content ) {
	if ( !is_singular() )
		return $content;

	ob_start();
?>
<img src="http://static.urx.io/units/web/urx-unit-loader.gif" style="display:none;" onload="var a=document.createElement('script');a.setAttribute('src','https://static.urx.io/units/web/babef125-e57e-4d69-a551-12929e899e3c.min.js'),this.parentNode.insertBefore(a,this);">
<?php

	$urx = ob_get_clean();

	return $urx . $content;

}, 5 );