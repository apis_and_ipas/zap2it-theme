<?php
/**
 * Adds the "Shows" Custom Post Type along with associated rewrite rules and taxonomies
 *
 * @package Tribune Media Zap2it
 */


/* ------------------------------------------------------------------
 Custom Post Type - Show Details
------------------------------------------------------------------ */
add_action( 'init', 'zap_init_shows_cpt' );
function zap_init_shows_cpt() {

    $args = array(
        'labels'             => zap_cpt_labels( 'Show Detail' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'rewrite'            => array( 'with_front' => false, 'slug' => 'show-details' ),
        'show_in_menu'       => true,
        'query_var'          => 'show-details',
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'thumbnail' ),
        'menu_icon'          => 'dashicons-tickets'
    );

    register_post_type( 'show-details', $args );

    register_taxonomy( 'show', [ 'post', 'gallery' ], array(
        'label' => __('TV Shows' ),
        'labels' => zap_cpt_labels( 'TV Show' ),
        'public' => true,
        'show_in_nav_menus' => true,
        'show_ui' => true,
        'show_tagcloud' => true,
        'hierarchical' => false,
        'update_count_callback' => '',
        'query_var' => 'show',
        'rewrite' => array(
            'slug' => 'tv-show',
            'with_front' => true,
            'hierarchical' => false,
        ),

        'capabilities' => array('manage_terms', 'edit_terms', 'delete_terms', 'manage_categories', 'assign_terms',),
    ) );
}




/**
 * Adds 'show_tab' query var
 */
add_filter( 'query_vars', 'zap_show_query_vars' );
function zap_show_query_vars( $vars ) {
    $vars[] = 'show_tab';
    $vars[] = 'show_extra';
    $vars[] = 'tmsId';
    return $vars;
}

function zap_display_show_tab() {
    $show_tab = get_query_var( 'show_tab' );

    if ( ! $show_tab )
        $show_tab = 'overview';

    get_template_part( 'template-parts/show', $show_tab );
}

/**
 * Adding genre meta for posts
 */
add_action( 'wp_head', function() {
    // This meta tag is only for posts currently
    if ( ! is_single() )
        return;

    $genres =  zap_get_genre_for_post( get_queried_object_id() );

    if ( $genres ) {
        echo '<meta name="genre" content="' . esc_attr( join( ', ', $genres ) ) . '"/>';
    }

}, 5 );


/**
 * [zap_get_genre_for_post description]
 * @param  integer $post_id [description]
 * @return [type]           [description]
 */
function zap_get_genre_for_post( $post_id = 0 ) {
    $post_id = $post_id ? (int) $post_id : get_the_ID();
    $genres = [];

    $terms = get_the_terms( $post_id, 'show' );

    // Bail if get_the_terms returns false/WP_Error
    if ( ! $terms || is_wp_error( $terms ) )
        return [];

    foreach( $terms as $term ) {
        $show_detail = zap_get_show_data( $term->slug );

        // Something went wrong, skipping
        if ( is_wp_error( $show_detail ) )
            continue;

        foreach( (array) $show_detail['terms']['genre'] as $genre ) {
            $genres[] = $genre->name;
        }
    }

    return array_unique( $genres );
}

/**
 * Helper function to get all show-related data
 * TODO: maybe implement extra caching
 * @param  string $slug Show slug
 * @return (array|WP_Error) array with show details - post itself, terms, and meta
 */
function zap_get_show_data( $slug = '' ) {
    $q = new WP_Query([
            'name' => $slug,
            'post_type' => 'show-details',
            'no_found_rows' => true,
            'posts_per_page' => 1
            ]
    );

    if ( ! $q->have_posts() )
        return new WP_Error( 'no-show' );

    $post = $q->posts[0];
    $terms = [];

    $tax = get_object_taxonomies( $post );

    foreach( $tax as $taxonomy ) {
        $terms[$taxonomy] = get_the_terms( $post, $taxonomy );
    }

    $meta = get_post_meta( $post->ID, 'post_meta', true );

    return [
        'post' => $post,
        'terms' => $terms,
        'meta' => $meta,
    ];
}

/**
 * Replace show-details permalinks as they're not intended to be public
 * We need to use show taxonomy instead
 */
add_filter( 'post_type_link', function( $permalink, $post, $leavename ) {
    if ( is_admin() )
        return $permalink;
    return str_replace( 'show-details', 'tv-show', $permalink );
}, 10, 3 );