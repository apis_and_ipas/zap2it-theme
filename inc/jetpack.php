<?php
/**
 * Jetpack Compatibility File
 * See: https://jetpack.me/
 *
 * @package Tribune Media Zap2it
 */

wpcom_vip_disable_post_flair();

/**
 * Add theme support for Infinite Scroll.
 * See: https://jetpack.me/support/infinite-scroll/
 */
function zap_jetpack_setup() {
	add_theme_support( 'infinite-scroll', array(
		'container' => 'infinite-stories',
		'render'    => 'zap_infinite_scroll_render',
		'footer'     => 'page',
		'footer_widgets' => false,
		'wrapper'     => false,
		'type' => 'click'
	) );
} // end function zap_jetpack_setup
add_action( 'after_setup_theme', 'zap_jetpack_setup' );

add_filter( 'infinite_scroll_js_settings', 'zap_infinite_scroll_js_settings' );
function zap_infinite_scroll_js_settings( $js_settings ) {
	$js_settings['text'] = 'Load More Stories';
	return $js_settings;
}

function zap_infinite_scroll_render() {
	$i = $x = 0;

?>
<div class="row" data-tb-region="Grid Stories Row <?php echo (int) ++$x ?>">
<?php
	while ( have_posts() ) {
		the_post();
		get_template_part( 'template-parts/content', 'grid-generic' );

	 if ( ++$i % 5 == 0 ) {
	 	get_template_part( 'template-parts/ad', '300x250-btf' );

	 	echo '</div><div class="row" data-tb-region="Grid Stories Row ' . (int) ++$x .'">';
	 	$i = 0;
	 	continue;
	 }

		if ( $i % 3 === 0 )
			echo '</div><div class="row" data-tb-region="Grid Stories Row ' . (int) ++$x .'">';
	}
?> </div> <?php
} // end function zap_infinite_scroll_render

/**
 * Display infinite scroll on front-page and any archive
 */
add_filter( 'infinite_scroll_archive_supported', function() {
		return is_home() || is_archive();
	}, 20 );

add_action( 'wp_enqueue_scripts', function() {
	wp_dequeue_style( 'the-neverending-homepage' );
 }, 999 );


/**
 * Jetpack oEmbed tweaks:
 */

// Example URL: https://www.facebook.com/VenusWilliams/posts/10151647007373076
wp_embed_register_handler( 'facebook', JETPACK_FACEBOOK_EMBED_REGEX, 'zap_jetpack_facebook_embed_handler', 5 );

// Example URL: https://www.facebook.com/permalink.php?id=222622504529111&story_fbid=559431180743788
wp_embed_register_handler( 'facebook-alternate', JETPACK_FACEBOOK_ALTERNATE_EMBED_REGEX, 'zap_jetpack_facebook_embed_handler', 5 );

// Photos are handled on a different endpoint; e.g. https://www.facebook.com/photo.php?fbid=10151609960150073&set=a.398410140072.163165.106666030072&type=1
wp_embed_register_handler( 'facebook-photo', JETPACK_FACEBOOK_PHOTO_EMBED_REGEX, 'zap_jetpack_facebook_embed_handler', 5 );

// Photos (from pages for example) can be at
wp_embed_register_handler( 'facebook-alternate-photo', JETPACK_FACEBOOK_PHOTO_ALTERNATE_EMBED_REGEX, 'zap_jetpack_facebook_embed_handler', 5 );

// Videos e.g. https://www.facebook.com/video.php?v=772471122790796
wp_embed_register_handler( 'facebook-video', JETPACK_FACEBOOK_VIDEO_EMBED_REGEX, 'zap_jetpack_facebook_embed_handler', 5 );
// Videos  https://www.facebook.com/WhiteHouse/videos/10153398464269238/
wp_embed_register_handler( 'facebook-alternate-video', JETPACK_FACEBOOK_VIDEO_ALTERNATE_EMBED_REGEX, 'zap_jetpack_facebook_embed_handler', 5 );

function zap_jetpack_facebook_embed_handler( $matches, $attr, $url ) {
	if ( false !== strpos( $url, 'video.php' ) || false !== strpos( $url, '/videos/' ) ) {
		$embed = sprintf( '<div class="fb-video" data-allowfullscreen="true" data-href="%s" data-width="600"></div>', esc_url( $url ) );
	} else {
		$embed = sprintf( '<fb:post href="%s" data-width="600"></fb:post>', esc_url( $url ) );
	}

	$embed = '<span class="oembed-wrapper">' . $embed . '</span>';

	// since Facebook is a faux embed, we need to load the JS SDK in the wpview embed iframe
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX && ! empty( $_POST['action'] ) && 'parse-embed' == $_POST['action'] ) {
		return $embed . '<script src="//connect.facebook.net/en_US/all.js#xfbml=1"></script>';
	} else {
		wp_enqueue_script( 'jetpack-facebook-embed', plugins_url( 'js/facebook.js', __FILE__ ), array( 'jquery' ) );
		return $embed;
	}
}