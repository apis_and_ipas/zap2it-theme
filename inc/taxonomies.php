<?php
/**
 * Registering custom taxonomies
 */

add_action( 'init', function() {
	/**
	 *  Genre
	 */
	register_taxonomy( 'genre', [ 'show-details' ], [
		'label' => 'Genres',
		'labels' => tribune_ui_labels( 'Genre', 'Genre' ),
		'public' => true,
		'show_in_nav_menus' => true,
		'show_ui' => true,
		'hierarchical' => true,
		'query_var' => 'genre',
		'rewrite' => array(
			'slug' => 'genre',
			'with_front' => true,
			'hierarchical' => false,
		),
		'capabilities' => [ 'manage_terms', 'edit_terms', 'delete_terms', 'manage_categories', 'assign_terms' ],
	]);

	/**
	 *  Event
	 */
	register_taxonomy( 'event', [ 'post', 'show' ], [
		'label' => 'Events',
		'labels' => tribune_ui_labels( 'Event', 'Event' ),
		'public' => true,
		'show_in_nav_menus' => true,
		'show_ui' => true,
		'hierarchical' => true,
		'query_var' => 'event',
		'rewrite' => array(
			'slug' => 'event',
			'with_front' => true,
			'hierarchical' => true,
		),
		'capabilities' => [ 'manage_terms', 'edit_terms', 'delete_terms', 'manage_categories', 'assign_terms' ],
	]);

	/**
	 * TV Networks
	 */
	register_taxonomy( 'network', [ 'post', 'show' ], [
		'label' => 'TV Networks',
		'labels' => tribune_ui_labels( 'TV Network', 'TV Networks' ),
		'public' => true,
		'show_in_nav_menus' => true,
		'show_ui' => true,
		'hierarchical' => true,
		'query_var' => 'network',
		'rewrite' => array(
			'slug' => 'network',
			'with_front' => true,
			'hierarchical' => false,
		),
		'capabilities' => [ 'manage_terms', 'edit_terms', 'delete_terms', 'manage_categories', 'assign_terms' ],
	]);

	/**
	 * Celebrities
	 */
	register_taxonomy( 'celebrity', [ 'post', 'show-details', 'gallery' ], [
		'label' => 'Celebrities',
		'labels' => tribune_ui_labels( 'Celebrity', 'Celebrities' ),
		'public' => true,
		'show_in_nav_menus' => true,
		'show_ui' => true,
		'hierarchical' => false,
		'query_var' => 'celebrity',
		'rewrite' => array(
			'slug' => 'celebrity',
			'with_front' => true,
			'hierarchical' => false,
		),
		'capabilities' => [ 'manage_terms', 'edit_terms', 'delete_terms', 'manage_categories', 'assign_terms' ],
	]);

	/**
	 * Characters
	 */
	register_taxonomy( 'character', [ 'post', 'show-details', 'gallery' ], [
		'label' => 'Characters',
		'labels' => tribune_ui_labels( 'Character', 'Characters' ),
		'public' => true,
		'show_in_nav_menus' => true,
		'show_ui' => true,
		'hierarchical' => false,
		'query_var' => 'character',
		'rewrite' => array(
			'slug' => 'character',
			'with_front' => true,
			'hierarchical' => false,
		),
		'capabilities' => [ 'manage_terms', 'edit_terms', 'delete_terms', 'manage_categories', 'assign_terms' ],
	]);

	/**
	 * Movie Title
	 */
	register_taxonomy( 'movie-title', [ 'post', 'show-details', 'gallery' ], [
		'label' => 'Movie Titles',
		'labels' => tribune_ui_labels( 'Movie Title', 'Movie Titles' ),
		'public' => true,
		'show_in_nav_menus' => true,
		'show_ui' => true,
		'hierarchical' => false,
		'query_var' => 'movie-title',
		'rewrite' => array(
			'slug' => 'movie-title',
			'with_front' => true,
			'hierarchical' => false,
		),
		'capabilities' => [ 'manage_terms', 'edit_terms', 'delete_terms', 'manage_categories', 'assign_terms' ],
	]);

});