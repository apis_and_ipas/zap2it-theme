<?php
/**
 * Custom functions that act independently of the theme templates
 *
 * Eventually, some of the functionality here could be replaced by core features
 *
 * @package Tribune Media Zap2it
 */

 /**
 *   Generate Labels for Custom Post types.
 *
 *   @param  string $singular
 *   @param  string $plural (optional)
 *   @return array
 *
 */
 function zap_cpt_labels( $singular, $plural = '' ) {
    if ( empty( $plural ) ) {
		$plural = $singular . 's';
	}

    return array(
		'name' 		 	  => __( $plural, 'zap' ),
		'singular_name'  	  => __( $singular, 'zap' ),
		'search_items' 	  => __( 'Search ' . $plural, 'zap' ),
		'all_items' 		  => __( 'All ' . $plural, 'zap' ),
		'edit_item' 		  => __( 'Edit ' . $singular, 'zap' ),
		'add_new_item' 	  => __( 'Add New ' . $singular, 'zap' ),
		'menu_name' 		  => __( $plural , 'zap' ),
		'new_item' 		  => __( 'New ' . $singular, 'zap' ),
		'view_item' 		  => __( 'View ' . $singular, 'zap' ),
		'not_found' 		  => __( 'No ' . $plural . ' found', 'zap' ),
		'not_found_in_trash' => __( 'No ' . $plural . ' found in Trash', 'zap' )
    );
 }



/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function zap_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	global $post;

	// adds "{post-type}-{post-slug}" to body class list.
    if ( isset( $post ) ) {
        $classes[] = $post->post_type . '-' . $post->post_name;
    }

	return $classes;
}
add_filter( 'body_class', 'zap_body_classes' );


if ( version_compare( $GLOBALS['wp_version'], '4.1', '<' ) ) :
	/**
	 * Filters wp_title to print a neat <title> tag based on what is being viewed.
	 *
	 * @param string $title Default title text for current view.
	 * @param string $sep Optional separator.
	 * @return string The filtered title.
	 */
	function zap_wp_title( $title, $sep ) {
		if ( is_feed() ) {
			return $title;
		}

		global $page, $paged;

		// Add the blog name
		$title .= get_bloginfo( 'name', 'display' );

		// Add the blog description for the home/front page.
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) ) {
			$title .= " $sep $site_description";
		}

		// Add a page number if necessary:
		if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
			$title .= " $sep " . sprintf( esc_html__( 'Page %s', 'zap' ), max( $paged, $page ) );
		}

		return $title;
	}
	add_filter( 'wp_title', 'zap_wp_title', 10, 2 );

	/**
	 * Title shim for sites older than WordPress 4.1.
	 *
	 * @link https://make.wordpress.org/core/2014/10/29/title-tags-in-4-1/
	 * @todo Remove this function when WordPress 4.3 is released.
	 */
	function zap_render_title() {
		?>
		<title><?php wp_title( '|', true, 'right' ); ?></title>
		<?php
	}
	add_action( 'wp_head', 'zap_render_title' );
endif;


/**
 * White lists the custom data attribute used for mounting React components
 * @param  [type] $allowed [description]
 * @param  [type] $context [description]
 * @return [type]          [description]
 */
add_filter('wp_kses_allowed_html', 'zap_filter_allowed_html', 10, 2);
function zap_filter_allowed_html($allowed, $context){

	if ( is_array( $context ) ) {
	    return $allowed;
	}

	if ( $context === 'post') {
        // Example case
	    $allowed['div']['data-react-mount'] = true;
	}

	return $allowed;
}
