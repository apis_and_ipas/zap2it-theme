<?php
if ( !defined( 'WP_CLI' ) || ! WP_CLI )
	return;

/**
 * Zap2it.com specific CLI commands
 */
class Zap2it_Command extends WPCOM_VIP_CLI_Command {

	protected $tms;

	function __construct() {
		global $zap_tms;
		$this->tms = $zap_tms ? $zap_tms : new Zap_TMS;
	}

	/**
	 * Update legacy albums/ album reviews and link them to each other
	 *
	 * @subcommand sync-shows
	 *
	 * @param [type]  $args       [description]
	 * @param [type]  $assoc_args [description]
	 * @return [type]             [description]
	 */
	function sync_show_from_gracenote( $args, $assoc_args ) {
		$time_pre = microtime(true);
		$dry_run = isset( $assoc_args['dry-run'] );
		if ( $dry_run )
			WP_CLI::line( "Doing dry-run" );
		else
			WP_CLI::line( "Syncing show details" );

		$offset = isset( $assoc_args['ignore-show-offset'] ) ? 0 : wlo_get_option( 'tms_show_offset', 0 );
		$term_args = [
			'number' => 100,
			'hide_empty' => false,
			'fields' => 'names',
			'offset' => $offset
		];

		while ( $terms = get_terms( 'show', $term_args ) ) {
			foreach ( $terms as $term_name ) {

				WP_CLI::line( "Searching for {$term_name}" );

				$response = $this->tms->search( $term_name );

				if ( is_wp_error( $response ) || !$response ) {
					WP_CLI::warning( 'Request failed for show: ' . $term_name );
					continue;
				}

				// Grab top match
				$item = $this->tms->_pick_best_match( $term_name, $response );

				if ( $dry_run ) {
					WP_CLI::line( "Should sync " . $term_name );
				} else {
					WP_CLI::line( "Syncing " . $term_name );
					$this->tms->sync_show( false, $item );
				}

				$term_args['offset']++;
			}

			if ( $dry_run )
				WP_CLI::line( "Should update term offset" . $term_args['offset'] );
			else
				wlo_update_option( 'tms_show_offset', $term_args['offset'] );

			// Give DB some rest
			WP_CLI::line( 'Clearing caches and giving db some rest' );

			if ( !$dry_run )
				sleep( 5 );

			$this->stop_the_insanity();
		}
		$time_post = microtime(true);

		WP_CLI::success( "All done!" );
		WP_CLI::line( "Took " . ( $time_post - $time_pre ) );
	}

}

WP_CLI::add_command( 'zap2it', 'Zap2it_Command' );
