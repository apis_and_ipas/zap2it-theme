<?php
/**
 * Submenus and pages
 */
fm_register_submenu_page( 'zap2it_options', 'options-general.php', 'Zap2it Options' );
add_action( 'fm_submenu_zap2it_options', function() {
		$fm = new Fieldmanager_Group( array(
				'name'     => 'zap2it_options',
				'children' => array(
					// This is a Tribune shared field, a lot of tribune-plugins shared code relies on it, so we have to set it in Zap2it theme as well
					'station_code' => new Fieldmanager_Textfield( 'Station Code' ),
					'facebook' => new Fieldmanager_Textfield( 'Facebook Page URL' ),
					'twitter' => new Fieldmanager_Textfield( 'Twitter URL' ),
					'googleplus' => new Fieldmanager_Textfield( 'Google+ URL' ),
					'pinterest' => new Fieldmanager_Textfield( 'Pinterest URL' ),
				)
			) );

		$fm->activate_submenu_page();
	} );

/**
 * Various grid options
 */
fm_register_submenu_page( 'zap2it_grid_options', 'options-general.php', 'Grid Options' );
add_action( 'fm_submenu_zap2it_grid_options', function() {
		$fm = new Fieldmanager_Group( array(
				'name'     => 'zap2it_grid_options',
				'children' => array(

					'featuredCallSign' => new Fieldmanager_Textfield( 'Promoted Channel Callsign' ),
				)
			) );

		$fm->activate_submenu_page();
	} );

/**
 * Post meta
 */
add_action( 'fm_post_post', function() {
		$fm = new Fieldmanager_Group( array(
				'name' => 'post_meta',
				'children' => array(
					'canonical' => new Fieldmanager_Textfield( 'Canonical URL' ),
				),
			) );
		$fm->add_meta_box( 'Post Meta Fields', 'post' );
	} );

add_action( 'fm_post_show-details', function() {
		$fm = new Fieldmanager_Group( array(
				'name' => 'post_meta',
				'children' => array(
					'release_year' => new Fieldmanager_Textfield( 'Release Year' ),
					'banner' => new Fieldmanager_Media( 'Banner', [ 'description' => 'Image should be 1600px wide for optimal results' ] ),
					'seriesId' => new Fieldmanager_Textfield( 'Series ID' ),
					'tmsId' => new Fieldmanager_Textfield( 'TMS ID' ),
					'rootId' => new Fieldmanager_Textfield( 'rootId' ),
					'seriesId' => new Fieldmanager_Textfield( 'seriesId' ),

				),
			) );
		$fm->add_meta_box( 'Show Meta Fields', 'show-details' );
	} );

/**
 * First pass at gallery
 */
add_action( 'fm_post_gallery', function() {
		$fm = new Fieldmanager_Group( array(
				'name' => 'gallery',
				'children' => [
					'items' => new Fieldmanager_Group([
						'limit' => 0,
						'label' => 'Image',
						'label_macro' => array( 'Slide: %s', 'title' ),
						'add_more_label' => 'Add slide',
						'collapsed' => false,
						'sortable' => true,
						'children' => array(
							'title' => new Fieldmanager_Textfield( 'Slide Title' ),
							'slide' => new Fieldmanager_Media( 'Slide', array( 'required' => true ) ),
							'caption' => new Fieldmanager_Textarea( 'Caption' ),
							'credit' => new Fieldmanager_Textfield( 'Credit' ),
						),
						]
					),
					'settings' => new Fieldmanager_Group([
						'label' => 'Settings',
						'children' => [
							'autoPlay' => new Fieldmanager_Checkbox( 'Auto play' ),
							'showThumbnails' => new Fieldmanager_Checkbox( 'Show Thumbnails' ),
						]
					])
				]
			) );

		$fm->add_meta_box( 'Gallery', 'gallery' );
	} );
