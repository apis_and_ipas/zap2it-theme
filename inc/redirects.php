<?php
/**
 * All kinds of redirects/rewrites/ query_vars tweaks
 */

/**
 * This is for MT imported galleries which have format of
 * photos/zap-slug-with-dashes-{year}{month}{day}
 *
 * and showcards
 */

add_filter( 'query_vars', function( $qv ) {
	$extra = [ 'gallery' ];
	return array_merge( $qv, $extra );
} );

add_action( 'init', function() {
	// Old blog posts and galleries
	add_rewrite_rule( '^photos\/zap-(.*)-([0-9]{4})([0-9]{2})([0-9]{2})\/?$', 'index.php?year=$matches[2]&monthnum=$matches[3]&name=$matches[1]&gallery=1', 'top' );
	add_rewrite_rule( '^blogs\/([-\w]+)-([0-9]{4})-([0-9]{2})/?$', 'index.php?year=$matches[2]&monthnum=$matches[3]&name=$matches[1]', 'top' );
	add_rewrite_rule( '^blog-post/(.*)/?$', 'index.php?name=$matches[1]', 'top' );

	// Showcards
	add_rewrite_rule ( '^tv/(.*)/(\w{14})/?$', 'index.php?show=$matches[1]&tmsId=$matches[2]', 'top' );
	add_rewrite_rule ( '^tv/(.*)/episodes/(\w{14})/?$', 'index.php?show=$matches[1]&tmsId=$matches[2]', 'top' );
});
/**
 * These are for blog Drupal entries which have
 * blogs/slug_with_underscores-{year}{month} format
 *
 */
add_action( 'template_redirect', function() {
	if ( !is_404() )
		return;

	$url = esc_url_raw( $_SERVER['REQUEST_URI'] );

	$base = 'blogs/';

	if ( ! wp_in( $base, $url ) )
		return;

	preg_match( '#/blogs/([-\w]+)-([0-9]{4})-([0-9]{2})/?$#i', $url, $url_vars );

	array_shift( $url_vars );

	if ( 3 === count( $url_vars ) ) {
		list( $slug, $year, $month ) = $url_vars;

		// Modify and sanitize the slug to conform to wp permastruct
		$slug = str_replace( [ $base, '_' ], [ '', '-' ], $slug );

		$redirect_url = home_url( 'blog-post/' . $slug . '/' );
		wp_safe_redirect( $redirect_url, 301 );
		// Terminate
		exit;
	}

}, 0 );

/**
 * We tried to redirect legacy links and failed, redirecting old links to front page
 */
add_action( 'template_redirect', function() {
	if ( is_404() )
		wp_safe_redirect( home_url(), 301 );

}, 1 );
