<?php

add_action( 'init', 'zap_init_galleries_cpt' );

function zap_init_galleries_cpt() {
	$args = array(
	    'labels'             => tribune_ui_labels( 'Gallery', 'Galleries' ),
	    'public'             => true,
	    'publicly_queryable' => true,
	    'show_ui'            => true,
	    // 'rewrite'            => array( 'with_front' => false ),
	    'show_in_menu'       => true,
	    'query_var'          => true,
	    'capability_type'    => 'post',
	    'has_archive'        => false,
	    'hierarchical'       => false,
	    'menu_position'      => null,
	    'supports'           => array( 'title', 'thumbnail', 'revisions' ),
	    'menu_icon'          => 'dashicons-images-alt'
	);

	register_post_type( 'gallery', $args );
}

add_shortcode( 'zap-gallery', function( $atts ) {

	extract( shortcode_atts( [
		'width' => 770,
		'id' => get_the_ID()
		], $atts ) );

	$gallery = get_post_meta( $id, 'gallery', true );

	$gallery['items'] = array_filter( $gallery['items'], function( $e ) { return isset( $e['slide'] ); } );

	$items = array_map( function( $e ) {
		$thumbnail = wp_get_attachment_image_src( $e['slide'], 'thumb-360-202' );
		$original = wp_get_attachment_image_src( $e['slide'], 'thumb-0-432');
		$e['thumbnail'] = $thumbnail[0];
		$e['original'] = $original[0];
		return $e;

	}, $gallery['items'] );

	ob_start();
?>
<script>var ZapGalleryData = {
	data: <?php echo wp_json_encode( $items ) ?>
};
</script>
<div id="gallery" data-react-mount="ZapGallery"></div>
<?php

return ob_get_clean();
} );