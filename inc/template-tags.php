<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Tribune Media Zap2it
 */

/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function zap_posted_on() {

	ob_start();
	if ( function_exists( 'coauthors_posts_links' ) )
		coauthors_posts_links();
	else
		the_author_posts_link();
	$author = ob_get_clean();

	$byline = sprintf(
		'by <span class="author vcard">%s</span>',
		wp_kses_post( $author )
	);

	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s on %3$s</time>';

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date( 'h:m A' ) ),
		esc_html( get_the_date( 'M j, Y' ) )
	);


	$posted_on = sprintf(
		esc_html_x( 'at %s', 'post date', 'zap' ),
		$time_string
	);

	echo '<span class="byline"> ' . $byline . '</span> <span class="posted-on">' . $posted_on . '</span>'; // WPCS: XSS OK
}

/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function zap_entry_footer() {
	// Hide category and tag text for pages.
	if ( 'post' == get_post_type() ) {
		/* translators: used between list items, there is a space after the comma */
		$categories_list = get_the_category_list( esc_html__( ', ', 'zap' ) );
		if ( $categories_list && zap_categorized_blog() ) {
			printf( '<div class="cat-links">' . esc_html__( 'Filed in %1$s', 'zap' ) . '</div>', $categories_list ); // WPCS: XSS OK
		}

		/* translators: used between list items, there is a space after the comma */
		$tags_list = get_the_tag_list( '', esc_html__( ', ', 'zap' ) );
		if ( $tags_list ) {
			printf( '<div class="tags-links">' . esc_html__( 'Other topics: %1$s', 'zap' ) . '</div>', $tags_list ); // WPCS: XSS OK
		}
	}

	edit_post_link( esc_html__( 'Edit', 'zap' ), '<span class="edit-link">', '</span>' );
}

/**
 * Display navigation to next/previous post when applicable.
 */
function zap_post_navigation() {
	// Don't print empty markup if there's nowhere to navigate.
	$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
	$next     = get_adjacent_post( false, '', false );

	$previous_post = '<div class="nav-previous pull-left">';
	$previous_post .= '<div class="nav-label">Previous Story</div>%link</div>';

	$next_post = '<div class="nav-next pull-right">';
	$next_post .= '<div class="nav-label">Next Story</div>%link</div>';

	if ( ! $next && ! $previous ) {
		return;
	}
?>
	<nav class="navigation post-navigation" role="navigation">
		<h2 class="screen-reader-text"><?php _e( 'Post navigation', 'zap' ); ?></h2>
		<div class="nav-links">
			<?php
	previous_post_link( $previous_post, '%title' );
	next_post_link( $next_post, '%title' );
?>
		</div><!-- .nav-links -->
	</nav><!-- .navigation -->
	<?php
}

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function zap_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'zap_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
				'fields'     => 'ids',
				'hide_empty' => 1,

				// We only need to know if there is more than one category.
				'number'     => 2,
			) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'zap_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so zap_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so zap_categorized_blog should return false.
		return false;
	}
}


/**
 * Wraps the output of the footer nav with static copyright info
 *
 * @return string
 */

function zap_footer_nav_wrap() {
	// open the <ul>, set 'menu_class' and 'menu_id' values
	$wrap  = '<ul id="%1$s" class="%2$s">';

	// get nav items as configured in /wp-admin/
	$wrap .= '%3$s';

	// the static content
	$wrap .= '<li class="menu-item"><a class="copyright"> Copyright &copy; ' . date( 'Y' ) . ', Zap2it/Tribune Broadcasting</a> </li>';
	$wrap .= '<li class="menu-item"><span>  ' . vip_powered_wpcom() . '</span></li>';

	// close the <ul>
	$wrap .= '</ul>';

	// return the result
	return $wrap;
}


/**
 * Flush out the transients used in zap_categorized_blog.
 */
function zap_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'zap_categories' );
}
add_action( 'edit_category', 'zap_category_transient_flusher' );
add_action( 'save_post',     'zap_category_transient_flusher' );


/**
 * output social media share btns
 * @return {[type]} [description]
 */
function zap_social_media_btns(){

	if ( function_exists( 'sharing_display' ) ) {
		sharing_display( '', true );
	}

	if ( class_exists( 'Jetpack_Likes' ) ) {
	    $custom_likes = new Jetpack_Likes;
	    echo $custom_likes->post_likes( '' );
	}

}


function zap_print_extra_for_module() {
	$categories = get_the_category();
	$output = '<div class="extra">';

	foreach ( $categories as $category ) {
		$output .= sprintf( '<a href="%s" title="View all posts in %s">%s</a> %s',
			 esc_url( get_category_link( $category->term_id ) ),
			 esc_attr( $category->name ),
			 esc_html( $category->name ),
			 $category !== end( $categories ) ? ' &bull; ' : ''
		 );
	}
	$output .= "</div>";
	if ( ( current_time( 'timestamp' ) - get_the_modified_time( 'U' ) ) < 7200 ) {
		$output .= '<div class="timestamp">' .
			esc_html( human_time_diff( get_the_modified_time( 'U' ), current_time( 'timestamp' ) ) ) . ' ago' .
			'</div>';
	}
	echo $output;
}

