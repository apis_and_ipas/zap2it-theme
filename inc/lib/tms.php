<?php
/**
 * Bare bones class to fetch information from onConnect TMS API
 */
class Zap_TMS {

	static $key = 'henntgdvdng6bavgvfbsfcd7';
	static $base_url = 'http://data.tmsapi.com/v1.1/';
	static $assets_url = 'http://zap2it.tmsimg.com/';
	static $show_tax = 'show';
	static $show_cpt = 'show-details';
	static $already_synced_slug = 'tms_synced_show_titles';
	static $cron_hook = 'fetch_show_details_from_gracenote';
	public $messages = '';

	function __construct() {

		// We need to make sure we don't run those in WP_CLI context
		if ( !defined( WP_CLI ) || ! WP_CLI ) {
			add_action( 'save_post', array( $this, 'action_save_post' ), 20, 3 );
			add_action( 'created_term', array( $this, 'action_created_term' ), 20, 3 );
		}

		add_action( self::$cron_hook, array( $this, 'check_show_details' ) );
		add_action( 'admin_init', array( $this, 'setup_cron_event' ) );

		add_filter( 'wpcom_vip_passthrough_cron_to_jobs', function( $whitelist ) {
	    $whitelist[] = self::$cron_hook;
	    return $whitelist;
		} );
	}

	/**
	 * Make a request and return parsed body
	 *
	 * @param string  $url [description]
	 * @return mixed
	 */
	function request( $endpoint = '', $method = '', $args = [] ) {
		$url = $this->build_request_url( $endpoint, $method, $args );

		if ( ! $url || ! wp_http_validate_url( $url ) )
			return new WP_Error( 'invalid-url' );

		$response = wp_remote_request( $url, [ 'method' => 'GET', 'timeout' => 10 ] );
		// TODO: verbose error handling
		if ( is_wp_error( $response ) ) {
			return $response;
		}

		return json_decode( $response['body'] );
	}

	/**
	 * Helper method to build api url
	 *
	 * @param string  $endpoint [description]
	 * @param string  $method   [description]
	 * @param array   $args     [description]
	 * @return string request url
	 */
	function build_request_url( $endpoint = '', $method = '', $args = [] ) {
		$args = wp_parse_args( $args, [ 'api_key' => self::$key ] );
		$url = add_query_arg( $args, self::$base_url . "{$endpoint}/{$method}" );
		return $url;
	}

	/**
	 * Search for program
	 *
	 * @param string  $term search term
	 * @return [type]       [description]
	 */
	function search( $term = '' ) {
		$response = $this->request( 'programs', 'search', [
			// Q arg should be urlencoded, otherwise api returns weird results
			'q' => urlencode( $term ),
			'entityType' => 'show',
			'imageSize' => 'Ms',
			'imageAspectTV' => '16x9'
			] );

		if ( is_wp_error( $response ) )
			return false;

		return $response->hits;
	}

	/**
	 * Various action hooks and filters
	 */

	/**
	 * Fetch the show details on show creation
	 */
	function action_save_post( $post_id, $post, $update ) {
		if ( ! $update || wp_is_post_revision( $post_id ) || get_post_status( $post_id ) === 'trash' || get_post_type( $post ) !== self::$show_cpt )
			return;

		remove_action( 'save_post', array( $this, __FUNCTION__ ), 20 );

		$response = $this->search( $post->post_title );

		if ( is_wp_error( $response ) || !$response )
			return;

		// Grab top match
		$item = $this->_pick_best_match( $post->post_title, $response );

		$this->sync_show( $post_id, $item, $post->post_title );
	}

	function _pick_best_match( $title, $list ) {
		if ( ! $list )
			return;

		foreach ( $list as $item ) {
			if ( $item->program->title === $title ) {
				$r = $item->program;
				break;
			}
		}

		return isset( $r ) ? $r : $list[0]->program;
	}

	/**
	 * Runs when show term is created, automatically creates show details card, or updates existing one
	 *
	 * @param [type]  $term_id  [description]
	 * @param [type]  $tt_id    [description]
	 * @param [type]  $taxonomy [description]
	 * @return [type]           [description]
	 */
	function action_created_term( $term_id, $tt_id, $taxonomy ) {
		if ( $taxonomy != 'show' )
			return;

		remove_action( 'created_term', array( $this, 'action_created_term' ), 20 );
		remove_action( 'save_post', array( $this, 'action_save_post' ), 20 );

		$term = get_term_by( 'term_taxonomy_id', $tt_id, self::$show_tax );

		$response = $this->search( $term->name );

		if ( is_wp_error( $response ) || !$response )
			return;

		// Grab top match
		$item = $this->_pick_best_match( $term->name, $response );

		$this->sync_show( false, $item, $term->name );
	}

	function setup_cron_event() {
		if ( !wp_next_scheduled( self::$cron_hook ) ) {
			// We're not going to run this on wp cron, using wpcom_vip_passthrough_cron_to_jobs
			// and sending cron job into vip jobs system
			wp_schedule_event( time(), 'hourly', self::$cron_hook );
		}

	}

	/**
	 * Parse and update show details
	 *
	 * @param object  $item Show object from onDemand TMS
	 * @return [type]       [description]
	 */
	function sync_show( $post_id = false, $item, $title = '' ) {
		$title = $title ? $title : $item->title;

		if ( ! $post_id ) {

			// First check if the show details exist
			$query = new WP_Query( [
				'post_type' => 'show-details',
				'posts_per_page' => 1,
				'name' => sanitize_title_with_dashes( $title )
				] );

			if ( $query->have_posts() ) {
				$post = $query->posts[0];
				$post_id = $post->ID;
			}

		} else {
			$post = get_post( $post_id );
		}

		$post_id = wp_insert_post( [
			'ID' => (int) $post_id,
			'post_title' => $title,
			'post_excerpt' => isset( $post ) && $post->post_excerpt ? $post->post_excerpt : wp_filter_post_kses( $item->shortDescription ),
			'post_content' =>  isset( $post ) && $post->post_content ? $post->post_content : wp_filter_post_kses( $item->longDescription ),
			'post_status' => 'publish',
			'post_type' => 'show-details',
			], true );

		// Just bail if something bad happened
		if ( is_wp_error( $post_id ) )
			return new WP_Error( 'couldnt-create-or-update-post' );

		$genres = array_map( function( $g ) { return sanitize_title_with_dashes( $g ); }, $item->genres );

		/**
		 * Make sure the genre term exists
		 */
		foreach ( $item->genres as $g ) {
			if ( !term_exists( $g, 'genre' ) ) {
				wp_insert_term( $g, 'genre');
			}
		}

		// Set genres for show
		wp_set_object_terms( $post_id, $genres, 'genre' , true );

		$pm = get_post_meta( $post_id, 'post_meta', true );

		$banner_id = 0;

		if ( isset( $item->preferredImage ) ) {
			$url = self::$assets_url . $item->preferredImage->uri;
			$banner_id = tribune_sideload_attachment( $url, $post_id );
		}

		$pm_arr = [
			'release_year' => (int) $item->releaseYear,
			'tmsId' => sanitize_text_field( $item->tmsId ),
			'rootId' => sanitize_text_field( $item->rootId ),
			'seriesId' => sanitize_text_field( $item->seriesId ),
			'banner' => (int) $banner_id,
		];

		update_post_meta( $post_id, 'post_meta', $pm_arr );

		$this->set_thumbnail( $item->tmsId, $post_id );

		return true;
	}

	function set_thumbnail( $tmsId, $post_id ) {
		// Bail if it's not an admin request
		if ( !is_admin() && ( ! defined( 'WP_CLI' ) && WP_CLI ) )
			return false;

		$show = $this->request( 'programs', $tmsId, [
			'imageSize' => 'Md',
			'imageAspectTV' => '16x9'
			] );

		if ( is_wp_error( $show ) )
			return false;

		$url = self::$assets_url . $show->preferredImage->uri;

		$thumb_id = tribune_sideload_attachment( $url, $post_id );

		if ( $thumb_id ) {
			$r = set_post_thumbnail( $post_id, $thumb_id );
		}

		return $thumb_id;
	}

	function check_show_details() {
		$synced_shows = wlo_get_option( Zap_TMS::$already_synced_slug, [] );

		$startTime = date( "Y-m-d\TH:i\Z", current_time( 'timestamp', true ) );

		$lineup = $this->request( 'lineups', 'USA-DFLTP/grid', [
			'excludeChannels' => 'music,ppv',
			'startDateTime' => $startTime,
			] );



		if ( is_wp_error( $lineup ) || isset( $lineup->errorMessage ) ) {
			return false;
		}

		foreach ( (array) $lineup as $lineup_item ) {
			if ( !isset( $lineup_item->airings ) ) {
				continue;
			}

			foreach ( (array) $lineup_item->airings as $airing ) {

				// Show already added, bail, if force-sync argument is not set
				if ( in_array( $airing->program->title, $synced_shows ) ) {
					continue;
				}

				// Make sure show term exists
				if ( !term_exists( $airing->program->title, Zap_TMS::$show_tax ) ) {
					wp_insert_term( $airing->program->title, Zap_TMS::$show_tax );
				}

				// Search for the actual show data ( lineups also return episodes with different descriptions and tmsIds)
				$response = $this->search( $airing->program->title );

				$item = $this->_pick_best_match( $airing->program->title, $response );

				$result = $this->sync_show( false, $item, $airing->program->title );

				// Indicate that the show is synced
				if ( ! is_wp_error( $result ) || ! $result ) {
					$synced_shows[] = $airing->program->title;
					wlo_update_option( Zap_TMS::$already_synced_slug, $synced_shows );
				}
			}
		}
	}

}

global $zap_tms;
$zap_tms = new Zap_TMS;
