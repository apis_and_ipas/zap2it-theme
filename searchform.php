<form action="/" method="get">
	<div class="form-group">
		<input type="text" name="s" id="search" class="form-control" value="<?php the_search_query(); ?>" placeholder="Your search query">
	</div>
	<button type="submit" class="btn btn-lg btn-primary">Search</button>
</form>
