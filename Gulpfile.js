var gulp        = require('gulp'),
    browserSync = require('browser-sync'),
    reload      = browserSync.reload,
    source      = require('vinyl-source-stream'),
    buffer      = require('vinyl-buffer'),
    browserify  = require('browserify'),
    watchify    = require('watchify'),
    reactify    = require('reactify'),
    brfs        = require('brfs'),
    $           = require('gulp-load-plugins')();

// Try to load .env file, but don't produce the warning if there's none present
require('dotenv').load({silent: true});

var config = {
    styles: {
        src: './assets/scss/styles.scss',
        editorSrc: './assets/scss/editor-styles.scss',
        files: './assets/scss/**/*.scss',
        dest: './assets/css',

        sass: {
            outputStyle: 'compressed',
        },
        autoprefixer: {
            browsers: ['last 2 versions'],
            cascade: false
        }

    },
    scripts: {
        src:    './src/main.jsx',
        files: ['./src/**/*.js','./src/**/*.jsx'],
        dest:   './assets/js',
        output: 'bundle.js',
    },
    sprites: {
        src: './assets/images/sprites/*.png',
        styleDest: './assets/scss/',
        sheetDest: './assets/images/',
        spritesmith: {
            retinaSrcFilter: ['./assets/images/sprites/*@2x.png'],
            imgName: 'sprites.png',
            retinaImgName: 'sprites@2x.png',
            cssName: '_sprites.scss',
            cssTemplate: './assets/images/sprites/sheetTemplate.mustache',
        }
    },
    browserSync: {
        proxy: process.env.BSPROXY || 'zap2it.dev',
        open: false,/* Don't open new windows */
        notify: {   /* Hide the notification. */
            styles: ['opacity: 0', 'position: absolute']
        }
    }
};


function handleErrors() {
    var args = Array.prototype.slice.call(arguments);

    $.notify.onError({
        title: "Compile Error",
        message: "<%= error.message %>"
    }).apply(this, args);

    this.emit('end'); // Keep gulp from hanging on this task
}

function buildScript(file, watch) {

    var props = {
        entries: [file], 
        debug: true,
        transform: [reactify, brfs]
    };

    var bundler = watch ? watchify(browserify(props)) : browserify(props);
 
    function rebundle() {
        var stream = bundler.bundle();
        return stream.on('error', handleErrors)
            .pipe( source( config.scripts.output ) )
            .pipe( buffer() )
            .pipe( $.sourcemaps.init({loadMaps: true}) )
                // Add transformation tasks to the pipeline here.
                .pipe( $.uglify() )
            .pipe( $.sourcemaps.write('./') )
            .pipe( gulp.dest( config.scripts.dest ) )
            .pipe( reload( { stream: true } ) )
    }

    bundler.on('update', function() {
        rebundle();
        $.util.log('Rebundle...');
    });

    return rebundle();
}



// Serve compiled site from the ./www/ directory
gulp.task('serve', function () {
    browserSync( config.browserSync );
});


// Compiles SASS, generates, Source Maps
gulp.task('styles', function(){
    gulp.src( config.styles.src )
        .pipe( $.plumber() )                                         // error handling w/o breaking streams
        .pipe( $.sourcemaps.init() )                                 // generate sourcemaps
        .pipe( $.sass( config.styles.sass ) )                        // preprocess css
        .pipe( $.autoprefixer( config.styles.autoprefixer ) )        // Handle Vendor Prefixes
        .pipe( $.sourcemaps.write( './' ) )                          // write sourcemaps
        .pipe( gulp.dest( config.styles.dest ) )                     // write compiles files
        .pipe( reload( { stream: true } ) );                         // BrowserSync reload

    gulp.src( config.styles.editorSrc )
        .pipe( $.plumber() )                                         // error handling w/o breaking streams
        .pipe( $.sass( config.styles.sass ) )                        // preprocess css
        .pipe( $.autoprefixer( config.styles.autoprefixer ) )        // Handle Vendor Prefixes
        .pipe( gulp.dest( config.styles.dest ) )                     // write compiles files                      // BrowserSync reload
});

gulp.task('sprites', function () {
    var spriteData = gulp.src( config.sprites.src )
        .pipe( $.spritesmith( config.sprites.spritesmith ) );

    // pipe image output to disk
    spriteData.img
        .pipe( gulp.dest( config.sprites.sheetDest ) );

    // pipe styles output to disk
    spriteData.css
        .pipe( gulp.dest( config.sprites.styleDest ) )
        .pipe( reload( { stream: true } ) );
});

gulp.task('scripts', /*['lint'],*/ function(){
    return buildScript( config.scripts.src, false );
});

 

// Runs a build and then starts the dev server,
gulp.task('watch', ['build', 'serve'], function(){

    /**
     * Watch for SASS changes and rerun `styles`
     */
    gulp.watch( config.styles.files, ['styles'] );

    /**
     * Watch for sprite changes and rerun `sprites`
     */
    gulp.watch( config.sprites.src, ['sprites'] );

    /**
     * Watch for JS changes and rerun `scripts`
     */
    return buildScript( config.scripts.src, true );
});

gulp.task('lint', function () {
    return gulp.src(['js/**/*.js*'])
        .pipe($.eslint())
        .pipe($.eslint.format())
        .pipe($.eslint.failOnError());
});


gulp.task('build', ['sprites', 'styles', 'scripts'])

/**
 * Default task, aliased to `gulp watch`,
 */
gulp.task('default', ['watch']);


