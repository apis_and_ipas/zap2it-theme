# tribune-zap2it

### Local Dev Setup
  1. Assure node.js/npm is installed locally
  2. Run `npm install` to install dependencies
  3. Run run the build use `npm run build` or `npm run watch` 

    This will:
    - watch `/src/**/*.js` and rebuild on change
    - watch `/assets/images/sprites/**/*.png` and rebuild spritesheet on change
    - watch `/assets/scss/**/*.scss` and rebuild stylesheet on change

    NOTE: Do not run `gulp` directly without setting `NODE_ENV=production`. 
    Building React apps in "production mode" decreases the build size of React


### Key Concepts  
[Using Flux and Backbone](http://www.toptal.com/front-end/simple-data-flow-in-react-applications-using-flux-and-backbone)  
[Flux + Backbone = FluxBone](https://gist.github.com/rattrayalex/d5e14e85702c0a6d78c6)  
[react-flux-backbone](https://github.com/jtangelder/react-flux-backbone)  
