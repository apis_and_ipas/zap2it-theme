<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Tribune Media Zap2it
 */

get_header(); ?>

<div class="" id="infinite-stories">
<?php zap_infinite_scroll_render(); ?>

<?php get_footer();