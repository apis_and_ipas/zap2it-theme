/**
 * Various wp specific tweaks
 */
var $ = require('jquery');
var detect = require('./shared/helpers/detect');
var _ = require('underscore');
var config = require('./config');

/**
 * Check if string contains fragment
 */
function strContains(string, fragment) {
	return string.indexOf(fragment) != -1;
}

$(function() {

	var GETZIP = {
		getLocation: function() {
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(GETZIP.getZipCode, GETZIP.error, {
					timeout: 7000
				}); //cache it for 10 minutes
			} else {
				GETZIP.error('Geo location not supported');
			}
		},
		index: 0,
		error: function(msg) {
			if (msg.code) {
				//this is a geolocation error
				switch (msg.code) {
					case 1:
						// permission denied
						break;
					case 2:
						// position unavailable
						break;
					case 3:
						GETZIP.index++;
						// timeout, retry
						navigator.geolocation.getCurrentPosition(GETZIP.getZipCode, GETZIP.error, {
							timeout: 7000
						});
						break;
					default:
						//nothing
				}
			} else {
				//this is a text error
			}

		},

		getZipCode: function(position) {
			var position = position.coords.latitude + "," + position.coords.longitude;
			$.getJSON(_.escape("http://maps.google.com/maps/api/geocode/json?latlng=" + position + "&sensor=false"), {
				type: "application/json"
			}, function(json) {
				//Find the zip code of the first result
				if (!(json.status == "OK")) {
					GETZIP.error('Zip Code not Found');
					return;
				}
				var found = false;
				$(json.results[0].address_components).each(function(i, el) {
					if ($.inArray("postal_code", el.types) > -1) {


						zipEl.val(_.escape(el.short_name));
						found = true;
						return;
					}
				});
				if (!found) {
					GETZIP.error('Zip Code not Found');
				}
			});
		}
	}

	var form = $('.contact-form');
	if (form.length === 0)
		return;

	var zipEl = form.find('[name$="zippostalcode"]');
	var providerEl = form.find('[name$="provider"]');

	var ua = detect.parse(navigator.userAgent);

	form.find('[name$="os"]').val(_.escape(ua.os.name));
	form.find('[name$="browser"]').val(_.escape(ua.browser.name));

	form.find("select[name$='subject']").find("option:eq(0)").val('').prop('selected', 'selected').prop('disabled', 'disabled');

	var messageDiv = document.createElement('small');

	form.find('label[for$="comment"]').append(document.createElement('br')).append($(messageDiv).text('Be as descriptive as possible. If you’re experiencing technical issues, or incorrect lineup details, please provide exact steps to reproduce the issue.'));

	form.find("select[name$='subject']").on('change', function(e) {
		var $this = $(this);
		var selectedOption = $this.find("option:selected");

		if (strContains(selectedOption.val(), 'Lineup') || strContains(selectedOption.val(), 'Technical')) {

			// Try to fetch users zipcode
			// If available - make provider a select, if not - keep it as a text field
			GETZIP.getLocation();

			zipEl.prop('required', 'required');
			providerEl.prop('required', 'required');
		} else {
			zipEl.prop('required', '');
			providerEl.prop('required', '');
		}

	});

});