var Model = require('../shared/store').Model;
var constants = require('../constants');

var ProviderSearchStore = Model.extend({

    defaults: {
        providerResults: [],
        currentQuery: ''
    },

    initialize: function() {
        Model.prototype.initialize.call(this, arguments);
    },

    handleDispatch: function(payload) {

        switch (payload.actionType) {

            case constants.FIND_PROVIDERS:
                this.set({
                    providerResults: []
                });
            break;

            case constants.FIND_PROVIDERS_SUCCESS:
                this.set({
                    currentQuery: payload.query,
                    providerResults: payload.data
                });
            break;
        }
    }

});

module.exports = window.providerSearchStore =  new ProviderSearchStore();
