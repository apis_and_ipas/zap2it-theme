var Model = require('../shared/store').Model;
var constants = require('../constants');
var UserStore = require('./UserStore');
var UserActions = require('../actions/UserActions');
var _ = require('underscore');

var PreferencesStore = Model.extend({

    defaults: {
        music: false, // show music channels
        ppv: false,   // show PPV channels
        nonhd: false, // Only Show HD channels
        timespan: 3,  // Grid timespan - 3/6
    },

    initialize: function() {
        Model.prototype.initialize.call(this, arguments);
    },

    /**
     * toggle boolean attributes on/off
     * @param  {String} key
     */
    toggle: function(key) {
        if (key === 'timespan') return;
        this.set(key, !this.get(key));
    },

    /**
     * Is `key` true/selected?
     * @param  {String}  key 
     * @return {Boolean}     
     */
    isChecked: function(key) {
        if (key === 'timespan') return;
        return !!this.get(key);
    },

    /**
     * outputs the current state of exclusions as a Comma seperated list 
     * @return {String} [description]
     */
    getExclusions: function() {
        var list = [];

        if ( !this.get('music') ) { list.push("music") } // !false = true = yes exclude this
        if ( !this.get('ppv') ) { list.push("ppv")   } // !false = true = yes exclude this
        if ( this.get('nonhd') ) { list.push("nonhd") }
        
        return list.join(',');
    },

    /**
     * outputs the current state of exclusions as an object
     * @return {Object}  
     */
    getExclusionsJSON: function(){
        return JSON.stringify({
            music: !this.get('music'), // !false = true = yes exclude this
            ppv:   !this.get('ppv'),   // !false = true = yes exclude this
            nonhd: this.get('nonhd')
        });
    },

     handleDispatch: function(payload) {

        switch (payload.actionType) {
            case constants.USER_INIT:

                var rawExlusions = payload.user.get('exclusions');
                var timespan = payload.user.get('timespan');
                var exclusions, payload;

                if (_.isObject(rawExlusions)){
                    exclusions = rawExlusions;
                } else if (_.isString(rawExlusions)){
                    exclusions = JSON.parse(rawExlusions);
                }
 
                if (!exclusions) {
                    payload = { 
                        music: true, 
                        ppv:  true, 
                        nonhd: false, 
                        timespan: timespan 
                    };
                } else {
                    payload = {
                        music: !exclusions.music,
                        ppv: !exclusions.ppv,
                        nonhd: exclusions.nonhd,
                        timespan: timespan
                    }
                }

                this.set(payload);

            break;
        }

    }

})

module.exports = window.prefStore  = new PreferencesStore();