var _ = require('underscore');
var constants = require('../constants');
var Dispatcher = require('../shared/dispatcher');
var Model = require('../shared/store').Model;
var MyShowsStore = require('../stores/MyShowsStore');
var GridParamsStore = require('../stores/GridParamsStore');
var NewTonightActions = require('../actions/NewTonightActions');

/*
    Returns a subset of a colleciton of shows 
    where the shows are also in <3 My Shows
 */
var applyMyShowsFilter = function(shows) {
    var myShowsArray = MyShowsStore.get('myShowsIds');
    return _.filter(shows, function(show) {
        return _.contains(myShowsArray, show.seriesId);
    });
};


var NewTonightStore = Model.extend({

    defaults: {
        allShows: [],
        filteredShows: [],
        filter: false
    },

    requeryData: function() {
        setTimeout(function(){
            NewTonightActions.init({
                lineupId: GridParamsStore.get('lineupId')
            });
        }, 1);
    },

    handleDispatch: function(payload) {

        switch (payload.actionType) {
 
            case constants.NEW_TONIGHT_INIT_SUCCESS:

                this.set({
                    allShows: payload.shows,
                    filteredShows: applyMyShowsFilter(payload.shows)
                });

                break;

            case constants.MY_SHOWS_FETCH_IDS_SUCCESS:
                this.set({
                    filteredShows: applyMyShowsFilter(this.get('allShows'))
                });
                break;

            case constants.NEW_TONIGHT_FILTER:
                
                var applyFilter = payload.filter;
                
                if (applyFilter) {
                    this.set({
                        filter: true
                    })
                } else {
                    this.set({
                        filter: false
                    })
                }

                break;

            case constants.SET_PROVIDER:
                Dispatcher.waitFor([GridParamsStore.dispatchId]);
                this.requeryData();
                break;
        }
    }



});


module.exports = window.newTonightStore = new NewTonightStore();