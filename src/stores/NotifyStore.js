var constants = require('../constants');
var Model = require('../shared/store').Model;
var _ = require('underscore');


var NotifyStore = Model.extend({

    defaults: {
        text: null,
        visible: false,
        closable: true,
        is202Error: false
    },

    alert: function(text, options) {


        if (!(text.length > 0)) {
            this.set({
                visible: false
            });
            return;
        }

        var defaults = {
            visible: true,
            closable: true,
            type: 'info',
            channel: 'default'
        };

        var settings = _.extend({}, defaults, {
            text: text
        }, options);

        this.set(settings);

    },


    error: function(text, options) {
        var settings = _.extend({}, {
            type: 'danger'
        }, options);

        this.alert(text, settings);
    },


    handleDispatch: function(payload) {
        switch (payload.actionType) {

            case constants.NOTIFY_HIDE:
                this.set({
                    visible: false
                });
                break;

            case constants.NOTIFY_SHOW:
                this.alert(payload.text, {
                    channel: payload.channel
                });
                break;

            case constants.NOTIFY_ERROR:
                this.error(payload.text, {
                    channel: payload.channel
                });
                break;

            case constants.USER_LOGIN_FAIL:
                this.error('Incorrect username and/or password.', {
                    channel: 'modal'
                });
                break;

            case constants.USER_SIGNUP_FAIL:
                var msg = "An error occured while signing up";

                this.set({
                    visible: true,
                    type: 'info',
                    channel: 'modal'
                });
        
                break;

            case constants.USER_PASSWORD_RESET_SUCCESS:
                this.alert('Your password reset has been requested', {
                    channel: 'modal'
                });
                break;

            case constants.USER_PASSWORD_RESET_FAIL:
                this.error('An error occured while resetting your password', {
                    channel: 'modal'
                });
                break;
        }
    }

});

module.exports = new NotifyStore();
