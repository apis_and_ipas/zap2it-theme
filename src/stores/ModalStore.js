var Model = require('../shared/store').Model;
var constants = require('../constants');
var _ = require('underscore');

var ModalStore = Model.extend({

    defaults:{
        modals: {},
        isOpen: false,
        currentModal: null
    },

    initialize: function() {
        Model.prototype.initialize.call(this, arguments);
    },

    registerModal: function(modalName, modalClass) {
        var modals  = this.get('modals');
        
        if ( ! _.has(modals, modalName) ) { // store an class factory 
            modals[modalName] = modalClass;
        }

        this.set({ modals: modals });
    },

    show: function(modalName) {

        var modals  = this.get('modals');

        if ( _.has(modals, modalName) ) {
            this.set({
                currentModal: modalName,
                isOpen: true
            });
        }
    },

    hide: function() {
        this.set({ isOpen: false });
    },

    getModalTitle: function() {
        var currentModal  = this.get('currentModal');
        var modals = this.get('modals');

        var modalTitle = modals[currentModal] 
            && modals[currentModal].getTitle 
            && modals[currentModal].getTitle();

        return modalTitle || 'Untitled';
    },

    /**
     * Handle Actions
     * @param payload
     */
    handleDispatch: function(payload) {

        switch (payload.actionType) {

            case constants.MODAL_SHOW:
                this.show(payload.modalName);
            break;

            case constants.MODAL_HIDE:
                this.hide();
            break;

        }

    }
});

module.exports = window.modalStore = new ModalStore();