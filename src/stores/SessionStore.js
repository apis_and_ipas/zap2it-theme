var log = require('../shared/helpers/logger');

function SessionStore(namespace) {
    if (!window.sessionStorage) {
        throw new Error('SessionStore requires window.sessionStorage, which could not be found.')
    }
    this.namespace = namespace;
    this._store = window.sessionStorage;
}


SessionStore.prototype.set = function (key, value) {
    var storeKey = this.namespace + '.' + key;

    try {
        this._store.setItem(storeKey, JSON.stringify(value));
    } catch (e) {
        log("SessionStore error:", e)
        log("SessionStore clearing session!")
        this._store.clear();
        this._store.setItem(storeKey, JSON.stringify(value));
    }
    
};

SessionStore.prototype.get = function (key) {
    var storeKey = this.namespace + '.' + key;
    var data = this._store.getItem(storeKey);
    var results;


    if (data) {
        try {
            results = JSON.parse(data);
        } catch (e) {
            log('Error:', e)
        }
        return results;
    }

    return false;
};

SessionStore.prototype.remove = function (key, value) {
    this._store.setItem(key, null);
};

SessionStore.prototype.removeAll = function () {
    this._store.clear();
};

module.exports = SessionStore;
