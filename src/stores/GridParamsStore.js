var Model = require('../shared/store').Model;
var constants = require('../constants');
var timestampRounder = require('../shared/helpers/timestampRounder');
var buildCacheKey = require('../shared/helpers/buildCacheKey');
var Dispatcher = require('../shared/dispatcher');
var PreferencesStore = require('./PreferencesStore')
var ProviderStore = require('./ProviderStore')

 
var GridParamsStore = Model.extend({

    defaults: {
        lineupId: ProviderStore.get('lineupId'),
        timespan: PreferencesStore.get('timespan'),
        excludeChannels: PreferencesStore.getExclusions(), 
        time: timestampRounder.round() // return timestamp rounded down by 30 mins
    },

    initialize: function() {
        Model.prototype.initialize.call(this, arguments);
    },

    getCacheSlug: function() {
        return buildCacheKey(this.attributes)
    },

    handleDispatch: function(payload) {

        var page =  payload.data;

        switch (payload.actionType) {

            case constants.GRID_PAGE_CHANGE:
                var timespanInMins =  60 * 60 * this.get('timespan');
                // in an effort to keep this DRY, the action specifies which method (increment, decrement) to fire on timestampRounder
                var newTime = timestampRounder[payload.method](this.get('time'), timespanInMins);
 
                this.set({ time: newTime });
                break;

 
            case constants.GRID_TIME_JUMP:
                this.set({ time: payload.time });
                break;


            case constants.GRID_SET_TIMESPAN:
                this.set('timespan', payload.timespan);
                break;

            case constants.USER_INIT:
            case constants.USER_LOGIN_SUCCESS:
            case constants.USER_LOGOUT:
            case constants.SET_PROVIDER:
                Dispatcher.waitFor([ProviderStore.dispatchId]);
                 
                this.set({
                    lineupId: ProviderStore.get('lineupId')
                })
                break;
 

            case constants.USER_UPDATE_PREFS: 
                Dispatcher.waitFor([PreferencesStore.dispatchId]);

                this.set({
                    timespan: payload.timespan,
                    excludeChannels: PreferencesStore.getExclusions()
                });
                break;

        }
    }

});

module.exports = window.gridParamsStore = new GridParamsStore();
