var ModalActions = require('../actions/ModalActions');
var constants = require('../constants');
var Model = require('../shared/store').Model;


var UserStore = Model.extend({

    initialize: function() {
        Model.prototype.initialize.call(this, arguments);
    },

    /**
     * Is the current users signed in?
     * @returns {Boolean}
     */
    isCurrentUserAuthorized: function() {
        return this.get('isAuthenticated');
    },

    setUserData: function(user) {

        var username = user.get('firstName') || user.get('username');

        var user = {
            'username': user.get('username'),
            'firstname': user.get('firstName'),
            'avatar': user.get('avatar'),
            'email': user.get('email'),
            'isAuthenticated': true
        };

        this.set(user);
    },

    resetUserData: function() {
        var user = {
            'username': null,
            'firstname': null,
            'avatar': null,
            'email': null,
            'isAuthenticated': false
        };
        this.set(user);
    },

    /**
     * Handle Actions
     * @param payload
     */
    handleDispatch: function(payload) {

        switch (payload.actionType) {

            case constants.USER_INIT:
                console.log('User store USER_INIT payload', payload)
                this.setUserData(payload.user);
                break;

            case constants.USER_LOGOUT:
                this.resetUserData();
                break;

            case constants.USER_LOGIN_SUCCESS:

                this.setUserData(payload.user);

                // hide login modal
                setTimeout(function() {
                    ModalActions.hide();
                }, 1);

                break;

        }
    }

});

window.userStore = module.exports = new UserStore();