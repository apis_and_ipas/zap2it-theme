var Model = require('../shared/store').Model;
var constants = require('../constants');
var guessTimezone = require('../shared/helpers/guessTimezone');
var getDefaultProvider = require('../shared/helpers/defaultProvider');
var UserActions = require('../actions/UserActions');
var _ = require('underscore');

guessTimezone.setWhitelist(); //return only US timezones

var ProviderStore = Model.extend({

    // timezone    - determined by Javascript Timezone offset 
    // lineupId    - default lineup based on timezone
    // postalCode  - default lineup postal code
    // countryCode - default lineup country code
    // name        - display name for provider
    // isDefaultProvider - is one of the default 'timezone' providers
     
    initialize: function() {
        Model.prototype.initialize.call(this, arguments);
        this.setDefaults();

        this.on('change', this.syncProvider.bind(this));
    },

    // Fire the update action whenever the store attribute changes
    // For logged in users this will persist to Parse
    syncProvider: function() {

        setTimeout(function(){
            UserActions.updateProvider(this.attributes);
        }.bind(this), 1);
        
    },

    /**
     * Sets default provider based on local timezone
     */
    setDefaults: function() {
        var timezone = guessTimezone.calc();
        var defaultProvider = getDefaultProvider(timezone);
        this.set(defaultProvider);
    },

    
    setProviderData: function(provider){
        this.set({
            timezone: provider.timezone || guessTimezone.calc(),
            lineupId: provider.lineupId,
            postalCode: provider.postalCode,
            countryCode: provider.countryCode,
            name: provider.name,
            isDefaultProvider: provider.isDefaultProvider || false
        });
    },

    /**
     * Handle Actions
     * @param payload
     */
    handleDispatch: function(payload) {

        switch (payload.actionType) {

            case constants.USER_INIT:
            case constants.USER_LOGIN_SUCCESS:
                var provider = payload.user.get('provider'); 
                if ( ! _.isUndefined( provider ) ) {
                    this.setProviderData(provider)
                }
            break;

            case constants.USER_LOGOUT:
                this.setDefaults();
            break;
            
            case constants.SET_PROVIDER:
                var provider  = payload.provider;
                console.log('provider', provider)
                this.setProviderData(provider);
            break;
 
        }

    }
});


module.exports = window.providerStore = new ProviderStore();