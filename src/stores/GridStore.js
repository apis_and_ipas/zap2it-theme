var constants = require('../constants');
var Dispatcher = require('../shared/dispatcher');
var Model = require('../shared/store').Model;
var dispatch = require('../shared/helpers/dispatch');
var GridActions = require('../actions/GridActions');
var GridParamsStore = require('./GridParamsStore');
var UserStore = require('./UserStore');
var getFilteredChannels = require('../shared/helpers/getFilteredChannels') ;
var _ = require('underscore');
var $ = require('jquery');

/**
 * looks up callSign of sponsored channel and retries channel listings from
 */
var setSponsoredChannel = function() {

    var featuredCallSign = window.Zap2itGridOptions && window.Zap2itGridOptions.featuredCallSign;

    // if a sponsoredChannel is set
    if (featuredCallSign) {
        var channels = this.get('allChannels');
        var sponsoredChannel = _.findWhere(channels, {callSign: featuredCallSign});
        return sponsoredChannel;
    } 
};

var GridStore = Model.extend({

    defaults: {
        isLoading: true,
        currentPage: null,
        activeChannel: null,
        activeShow: null,
        filterName: 'all',
        channels: [],
        allChannels: [],
        scrollTop: 0,
        isScrolling: false,
        sponsoredChannel: null,
        gridHeight: 600,
        stickyState: false
    },

    initialize: function() {
        Model.prototype.initialize.call(this, arguments);
    },


    // data fetch from cache or API
    fetchData: function() {

        // // grab the query params
        var params = GridParamsStore.toJSON(),
            cache_slug = GridParamsStore.getCacheSlug();

        // insure loading spinner is visible
        this.set({
            isLoading: true,
            currentPage: cache_slug
        });

        setTimeout(function () {
            GridActions.query(params);
        }, 0);
    },

    handleDispatch: function(payload) {

        switch (payload.actionType) {

            case constants.GRID_QUERY_SUCCESS:

                // // after Query Success, only update the Grid view if it matches the cache_slug as the new data
                if (payload.data.get('cache_slug') === this.get('currentPage')){

                    // preserve set filtering when loading new data
                    var filteredChannels = getFilteredChannels(payload.data.get('channels'), this.get('filterName'));

                    // update the store
                    this.set({
                        isLoading: false,
                        channels: filteredChannels,
                        allChannels: payload.data.get('channels'),
                        activeChannel: null, // unset select channel 
                        activeShow: null,     // unset open show detail,
                        sponsoredChannel: setSponsoredChannel.call(this)
                    });

                    // $(document).scrollTop(this.get('scrollTop'))
                }

                break;

            case constants.USER_LOGOUT:
            case constants.USER_LOGIN_SUCCESS:

                Dispatcher.waitFor([UserStore.dispatchId, GridParamsStore.dispatchId]);
                this.fetchData();
                break;


            // these cases fall through!
            // wait for the GridParamsStore, then requery the grid data.
            case constants.GRID_SET_TIMESPAN:
            case constants.GRID_TIME_JUMP:
            case constants.GRID_PAGE_CHANGE:
            case constants.USER_UPDATE_PREFS:
            case constants.SET_PROVIDER:

                Dispatcher.waitFor([GridParamsStore.dispatchId]);
                this.fetchData();

                break;

            case constants.GRID_TOGGLE_DETAIL:

                var currentActiveChannel = this.get('activeChannel');
                var currentActiveShow = this.get('activeShow');

                // if we're clicking on the currently select show, toggle the detail card
                if (
                    currentActiveChannel == payload.channel &&
                    currentActiveShow == payload.show
                ) {
                    this.set({
                        activeChannel: null,
                        activeShow: null
                    });
                } else {
                    // otherwise set the new activeChannel && activeShow
                    this.set({
                        activeChannel: payload.channel,
                        activeShow: payload.show
                    });

                }

                break;

            case constants.GRID_SET_FILTER:
                var filteredChannels = getFilteredChannels(this.get('allChannels'), payload.filterName);
    
                this.set({
                    filterName: payload.filterName,
                    emptyFilter: (filteredChannels.length === 0),
                    channels: filteredChannels
                }); 
                break;

            case constants.SET_STICKY_STATE:
                this.set({
                    stickyState: payload.stickyState
                });
                break;

 
        }
    }
});


window.gridStore = module.exports = new GridStore();
