var Model = require('../shared/store').Model;
var constants = require('../constants');
var UserActions = require('../actions/UserActions');
var UserStore = require('../stores/UserStore');
var _ = require('underscore');
var log = require('../shared/helpers/logger');

var FavoriteChannelsStore = Model.extend({

    defaults: {
        favChannels: [],
        hoveredChannel: null // gross
    },

    initialize: function () {
        Model.prototype.initialize.call(this, arguments);
    },

    fetchFavoriteChannels: function () {
        UserActions.fetchFavoriteChannels();
    },

    isStarred: function (channelId) {
        var favChannels = this.get('favChannels');
        return _.contains(favChannels, channelId);
    },

    /**
     * this would `ideally` be on the component itself,
     * but since its only ever one single item and
     * all those items are better implemented attached to this store, so be it.
     * @param  {Number}  channelId 
     * @return {Boolean}           
     */
    isHovered: function (channelId) {
        var hoveredChannel = this.get('hoveredChannel');
        return channelId === hoveredChannel;
    },

    /**
     * Handle Actions
     * @param payload
     */
    handleDispatch: function(payload) {

        switch (payload.actionType) {

            case constants.USER_INIT:
            case constants.USER_LOGIN_SUCCESS:

                // on user init, if we're signed in to parse, grab our favorite channels
                if (UserStore.isCurrentUserAuthorized()) {
                    this.fetchFavoriteChannels();
                }

                break;

            case constants.USER_LOGOUT:
                this.set('favChannels', []);
                break;

            case constants.USER_FETCH_FAV_CHANNELS:

                // build up array of channelId strings
                var channelIds = payload.favChannels.map(function (show) {
                    return show.get('channelId');
                });

                this.set('favChannels', channelIds);

                break;

            case constants.USER_FAV_CHANNEL:

                var channelIds = this.get('favChannels');
                channelIds.push(payload.channelId);
                this.set('favChannels', channelIds);

                break;

            case constants.USER_UNFAV_CHANNEL:

                var channelIds = this.get('favChannels');
                channelIds = _.without(channelIds, payload.channelId);    
                this.set('favChannels', channelIds);

                break;

            default:
                break;
        }
    }

});

 module.exports = window.favoriteChannelsStore = new FavoriteChannelsStore();
