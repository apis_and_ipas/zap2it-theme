var Model = require('../shared/store').Model;
var constants = require('../constants');

var ChannelStore = Model.extend({

    defaults:{
        channels: [],
        query: '',
        isLoading: false
    },

    initialize: function() {
        Model.prototype.initialize.call(this, arguments);
    },

    /**
     * Handle Actions
     * @param payload
     */
    handleDispatch: function(payload) {

        switch (payload.actionType) {

            //clear out cached channels when the Provider is changed.
            case constants.SET_PROVIDER:
                this.set('channels', []);
                break;

            case constants.FETCH_CHANNEL_LIST:
                this.set({
                    'isLoading': true
                });
                break;

            case constants.FETCH_CHANNEL_LIST_SUCCESS:
                this.set({
                    'channels': payload.channels,
                    'isLoading': false
                });
                break;
        }

    }

});

module.exports = window.channelStore = new ChannelStore();