var Model = require('../shared/store').Model;
var constants = require('../constants');

var AccountSettingsStore = Model.extend({


    defaults: {
        passwordMatchError: false,
        emailFormatError: false,
        successState: false
    },

    initialize: function() {
        Model.prototype.initialize.call(this, arguments);
    },

    /**
     * Handle Actions
     * @param payload
     */
    handleDispatch: function(payload) {

        switch (payload.actionType) {

            case constants.USER_UPDATE_PROFILE_SUCCESS:
                this.set({
                    successState: true,
                    passwordMatchError: false,
                    emailFormatError: false
                });
                break;

            case constants.USER_UPDATE_PROFILE_FAIL:

                break;

            default:
                break;
        }

    }


});

module.exports = new AccountSettingsStore();
