var Model = require('../shared/store').Model;
var constants = require('../constants');
var _ = require('underscore');
var Dispatcher = require('../shared/dispatcher');
var UserStore = require('../stores/UserStore');
var GridParamsStore = require('../stores/GridParamsStore'); 
var MyShowsActions = require('../actions/MyShowsActions');
var $ =  require('jquery');

var MyShowsStore = Model.extend({

    defaults: {
        showsData: [],
        myShowsIds: [],
        newFilter: false,
        isLoading: true
    },

    initialize: function () {
        Model.prototype.initialize.call(this, arguments);
        this.on('change:myShowsIds', this.fetchMyShowsAirings.bind(this));
    },

    fetchMyShows: function () {
        MyShowsActions.fetchMyShowsIds();
    },

    fetchMyShowsAirings: function () {
        var seriesIds = this.get('myShowsIds');
        var lineupId = GridParamsStore.get('lineupId');
        MyShowsActions.fetchMyShowsAirings(seriesIds, lineupId);
    },


    isHearted: function (seriesId) {
        var myShows = this.get('myShowsIds');
        return _.contains(myShows, seriesId);
    },

    /**
     * Handle Actions
     * @param payload
     */
    handleDispatch: function(payload) {

        switch (payload.actionType) {

            case constants.USER_INIT:
            case constants.USER_LOGIN_SUCCESS:

                Dispatcher.waitFor([UserStore.dispatchId]);
 
                if ( UserStore.isCurrentUserAuthorized()) {
                    this.fetchMyShows();
                }

                break;

            case constants.MY_SHOWS_FETCH_IDS_SUCCESS:

                // build up array of showId strings
                var seriesId = payload.myShowsIds.map(function (show) {
                    return show.get('seriesId');
                });
          
                this.set('myShowsIds', seriesId);
                break;

            case constants.MY_SHOWS_ADD:

                var showIds = this.get('myShowsIds');

                showIds.push(payload.seriesId);

                this.set('myShowsIds', showIds);

                break;

            case constants.MY_SHOWS_REMOVE:

                var showIds = this.get('myShowsIds');

                delete showIds[payload.seriesId];

                this.set('myShowsIds', showIds);

                break;

            case constants.MY_SHOWS_SHOW_DATA_FETCH_SUCCESS:

                this.set({
                    isLoading: false,
                    showsData: payload.shows
                });

                break;

            case constants.MY_SHOWS_TOGGLE_NEW_LISTINGS:

                this.set('newFilter', !this.get('newFilter'));

                break;

            case constants.SET_PROVIDER:
                // requery data when provider is changed.
                this.fetchMyShowsAirings();
                break;
        }
    }

});

 module.exports = window.myShowsStore = new MyShowsStore();
