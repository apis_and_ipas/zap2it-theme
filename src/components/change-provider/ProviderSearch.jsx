var React = require('react');

var ProviderSearchActions = require('../../actions/ProviderSearchActions');

var ProviderSearch = React.createClass({

    componentDidMount: function() {
        // auto-focus Zipcode input
        this.refs.postalCode.getDOMNode().focus();
    },

    handleSubmit: function(event) {
        event.preventDefault();

        var countryCode = this.refs.countryCode.getDOMNode().value;
        var postalCode = this.refs.postalCode.getDOMNode().value.trim();

        ProviderSearchActions.findProviders(countryCode, postalCode);
    },

    render: function() {
        return (
            <div className="provider-search">

                <h5>Find Providers</h5>

                <form className="form-inline">

                    <div className="form-group form-group-lg">
                        <label className="sr-only">Country</label>
                        <select ref="countryCode" className="form-control input-lg">
                            <option val="USA">USA</option>
                            <option val="CAN">CAN</option>
                        </select>
                    </div>

                    <div className="form-group form-group-lg">
                        <label className="sr-only">Postal Code</label>
                        <input autoFocus="true" type="text" ref="postalCode" className="form-control input-lg" placeholder="ZIP or Postal Code"/>
                    </div>

                    <div className="form-group form-group-lg">
                        <button type="submit" onClick={this.handleSubmit} className="btn btn-primary btn-lg">Find Providers</button>
                    </div>
                </form>

            </div>
        );
    }

});

module.exports = ProviderSearch;
