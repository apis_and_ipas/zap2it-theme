var React = require('react');
var GridActions = require('../../actions/GridActions');
var Button = require('react-bootstrap/lib/Button');
var getDefaultProvider = require('../../shared/helpers/defaultProvider');
var ProviderSearchActions = require('../../actions/ProviderSearchActions');
var ModalActions = require('../../actions/ModalActions')

var TimezoneSelect = React.createClass({

    handleClick: function(timezone, event) {

        var provider = getDefaultProvider(timezone);

        var query = {
            postalCode: provider.postalCode,
            countryCode: provider.countryCode
        }

        ModalActions.hide();
        
        setTimeout(function(){
            ProviderSearchActions.setProvider(provider, query);
        }, 0);
 
    },

    render: function() {
        return (
            <div className="timezone-select">
                <h5>Select a timezone</h5>
                <div className="row">
                    <div className="col-md-4">
                        <Button bsStyle='default' onClick={this.handleClick.bind(null, "America/New_York")}  className="btn-block">Eastern (ET)</Button>
                    </div>
                    <div className="col-md-4">
                        <Button bsStyle='default' onClick={this.handleClick.bind(null, "America/Chicago")} className=" btn-block">Central (CT)</Button>
                    </div>
                    <div className="col-md-4">
                        <Button bsStyle='default' onClick={this.handleClick.bind(null, "America/Denver")} className=" btn-block">Mountain (M)</Button>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-4">
                        <Button  bsStyle='default' onClick={this.handleClick.bind(null, "America/Los_Angeles")} className=" btn-block">Pacific (PT)</Button>
                    </div>
                    <div className="col-md-4">
                        <Button bsStyle='default' onClick={this.handleClick.bind(null, "America/Anchorage")} className=" btn-block">Alaskan (AT)</Button>
                    </div>
                    <div className="col-md-4">
                        <Button   bsStyle='default' onClick={this.handleClick.bind(null, "Pacific/Honolulu")} className=" btn-block">Hawaiian (HT)</Button>
                    </div>
                </div>

            </div>
        );
    }

});

module.exports = TimezoneSelect;
