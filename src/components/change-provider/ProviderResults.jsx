var React = require('react');
var Bootstrap = require('react-bootstrap');
var TabPane = Bootstrap.TabPane;
var TabbedArea = Bootstrap.TabbedArea;

var storeMixin = require('../../shared/mixins/storeMixin');
var ProviderSearchStore = require('../../stores/ProviderSearchStore');
var ProviderSearchActions = require('../../actions/ProviderSearchActions');
var ModalActions = require('../../actions/ModalActions')

// .ProviderItem--hover

var ProviderItem = React.createClass({

    handleSelect: function(event) {
        ModalActions.hide();

        //prevent dispatch collision
        var data = this.props.data;//dont lose this when the modal unmounts
        setTimeout(function(){ 
            ProviderSearchActions.setProvider(data);
        }, 1);
    },

    render: function() {
        return (
            <li className="provider-results__item" onClick={this.handleSelect}> {this.props.data.name}    {this.props.data.location ? "(" + this.props.data.location + ")": ''}</li>
        );
    }

});


var ProviderResults = React.createClass({

    mixins: [storeMixin(ProviderSearchStore)],

    getInitialState: function() {
        return ProviderSearchStore;
    },


    render: function() {

        // returns an object with cable, satellite and antenna arrays of providers
        var providerResultsByType = this.state.get('providerResults');
 
        return (
            <div className="provider-results">
                <div id="content">
                    <TabbedArea defaultActiveKey={1}>
                        <TabPane  eventKey={1} tab="Cable">
                            <ul className="provider-results__list">
                                {providerResultsByType['CABLE'] && providerResultsByType['CABLE'].map(function(item, index) {
                                     return <ProviderItem key={index} data={item}/>
                                }) || <div className="empty">No Cable Providers Found. Try searching above.</div>}
                            </ul>
                        </TabPane>
                        <TabPane  eventKey={2} tab="Satellite">
                            <ul className="provider-results__list">
                                {providerResultsByType['SATELLITE'] && providerResultsByType['SATELLITE'].map(function(item, index) {
                                    return <ProviderItem key={index} data={item}/>
                                })  || <div className="empty">No Satellite Providers Found. Try searching above.</div>}
                            </ul>
                        </TabPane>
                        <TabPane  eventKey={3} tab="Antenna">
                            <ul className="provider-results__list">
                                {providerResultsByType['OTA'] && providerResultsByType['OTA'].map(function(item, index) {
                                    return <ProviderItem key={index} data={item}/>
                                })  || <div className="empty">No Antenna Providers Found. Try searching above.</div>}
                            </ul>
                        </TabPane>
                    </TabbedArea>
                </div>
            </div>
        );
       
    }

});

module.exports = ProviderResults;
