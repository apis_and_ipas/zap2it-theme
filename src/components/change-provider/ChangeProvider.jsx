var React      = require('react');
var OverlayMixin  = require('react-bootstrap/lib/OverlayMixin');
var Modal  = require('react-bootstrap/lib/Modal');

var ProviderSearchStore = require('../../stores/ProviderSearchStore');

var Notify = require('./../Notify.jsx');
var ProviderSearch = require('./ProviderSearch.jsx');
var ProviderResults = require('./ProviderResults.jsx');
var TimezoneSelect = require('./TimezoneSelect.jsx');
var ModalActions = require('../../actions/ModalActions')
var ModalStore = require('../../stores/ModalStore')



ModalStore.registerModal('Change Your Provider Or Timezone', React.createClass({
    render: function(){
        return(
            <div className='modal-body'>

                <Notify channel='modal'/>

                <ProviderSearch/>
                <ProviderResults/>

                <hr className="or"/>

                <TimezoneSelect/>

            </div>
        )
    }
}));


var ChangeProvider = React.createClass({

    getInitialState: function() {
        return ProviderSearchStore;
    },

    handleToggle:function() {
        ModalActions.show('Change Your Provider Or Timezone')
    },

    render: function() {
        return (
            <button onClick={this.handleToggle} className="btn btn-default change-provider-btn">
                <i className="fa fa-map-marker"></i>
                <span>Change provider or timezone</span>
            </button>
        );
    }

});

module.exports = ChangeProvider;
