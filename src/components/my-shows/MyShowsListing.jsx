var React = require('react');
var AddToMyShows = require('../my-shows/AddToMyShows.jsx');
var MyShowsActions = require('../../actions/MyShowsActions');
var Spinner = require('../spinner.jsx');
var Episode = require('./Episode.jsx');
var slugify = require('../../shared/helpers/slugify');
var _ = require('underscore');
var blackListedTitles = require('../../shared/helpers/blackListedTitles');

var MyShowsListing = React.createClass({

    getInitialState: function() {
        return {
            isExpanded: false
        }
    },

    toggle: function(){
        this.setState({
            isExpanded: !this.state.isExpanded
        });
    },

    getButtonOutput: function(episodeCount) {
        // if we have 5 than fewer episodes, display no button
        if (episodeCount <= 5) return;

        // if we're expanded, show the 'Show fewer' button. Stannis will thank you.
        if (this.state.isExpanded) {
            return <button className="btn btn-default btn-block show-listings-expander" onClick={this.toggle}>Show fewer episodes</button>
        } 

        return <button className="btn btn-default btn-block show-listings-expander" onClick={this.toggle}>Show {episodeCount} more episodes</button>
    },

    render: function() {
        var showData = this.props.showData || {};
        var isExpanded = this.state.isExpanded;

        var episodes = showData.episodes;
        var output = [];
            

        for (var key in episodes) {
            if (episodes.hasOwnProperty(key)) {
                output.push( <Episode key={key} episodeData={episodes[key]}/> );
            }
        }

        // get episode count for display on expander button
        // before slicing the array below..
        var episodeCount = output.length;

        // if component isn't expanded only return the first five episodes
        // other return them all.
        output = isExpanded ? output : output.slice(0,2);

        var location  = window.location.origin;
        var showSlug, showPageUrl = "#";

        if (showData.title && !_.contains(blackListedTitles, showData.title)) {
            showSlug = slugify(showData.title);
            showPageUrl = location + '/tv/' + showSlug + '/' + showData.tmsId;   
        }  

        return (
            <div className="my-shows-listing ">

                <div className="my-shows-listing__header clearfix">
                    <div className="pull-left">
                        <a href={showPageUrl}><img src={showData.logo} alt=""/></a>
                        <a href={showPageUrl}><span className="show-title">{showData.title}</span></a>
                    </div>
                    <div className="pull-right">
                        <AddToMyShows seriesId={showData.seriesId}/>
                        <button className="btn btn-primary">
                            See all upcoming episodes
                        </button>
                    </div>
                </div>

                {output}
         
                <div className="row">
                    <div className="col-md-4 col-md-push-4">
                        {this.getButtonOutput(episodeCount)}
                    </div>
                </div>
                
                
            </div>
        );
    }

});

module.exports = MyShowsListing;