var React = require('react');
var moment = window.moment = require('moment');
var ProviderStore = require('../../stores/ProviderStore');
 
var Airing = React.createClass({

    renderChannelNo: function(channel){
        var isDefaultProvider = ProviderStore.get('isDefaultProvider');
        return !isDefaultProvider ? parseInt(channel, 10) : null;
    },

    render: function() {
        var timezone = ProviderStore.get('timezone');
        var displayTime = moment(this.props.time).tz(timezone).calendar();

        var airings = this.props.airingData.map(function(airing, key) {
            return <li key={key} className="episode-showing-station">
                <img src={airing.logo} alt=""/> 
                <div className="station-info">
                    <span className="channel">{this.renderChannelNo(airing.channel)}<br/></span> 
                    <span className="callSign">{airing.callSign}</span>
                </div> 
            </li>
        }, this);
 
        return (
            <li className="my-shows-listing__episode-showing clearfix">
                <div className="episode-time">{displayTime}</div>
                <ul className="episode-showing-stations">
                    <li className="episode-showing-station find-it-on">Find it on</li>
                    {airings}
                </ul>
            </li>
        );
    }

});


var Episode = React.createClass({

    renderFlag: function(flag){

        if (flag === "Live") {
            return <span style={{color: '#00a5dd', fontFamily: "OpenSans Bold"}}><strong>LIVE</strong></span>
        }

        if (flag === "New") {
            return <span style={{color: '#45b92e', fontFamily: "OpenSans Bold"}}><strong>NEW</strong></span>
        }

    },

    render: function() {
        var episode = this.props.episodeData;
        var airings = episode.airings;

        var output = [];
        for (var key in airings) {
            if (airings.hasOwnProperty(key)) {
                output.push( <Airing key={key} time={key} airingData={airings[key]} />  );
            }
        }

        return <div className="my-shows-listing__episode">
            <div className="my-shows-listing__episode-details">
                
                <h5 className="episode-title">{episode.episodeTitle}</h5>  
                
                <div className="episode-meta">
                    {episode.seasonNum ? "Season " + episode.seasonNum + " • ": null} 
                    {episode.episodeNum ? "Episode " + episode.episodeNum + " • ": null}
                    {episode.rating ? episode.rating + ' • '  : null}
                    {episode.isNew ? this.renderFlag("New") 
                        : episode.flag ? this.renderFlag(episode.flag) : null}
                </div>
               
                <p>{episode.description}</p>
            </div>

            <ul className="my-shows-listing__episode-showings">
                {output}
            </ul>
        </div>;

    }
});


module.exports = Episode;