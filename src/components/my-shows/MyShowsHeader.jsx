var React = require('react');
var Nav = require('react-bootstrap/lib/Nav');
var NavItem = require('react-bootstrap/lib/NavItem');
var _ = require('underscore');

var MyShowsHeader = React.createClass({

    render: function() {
        var newFilter = this.props.newFilter;

        return (
            <div className="my-shows__header clearfix">
                <Nav bsStyle='pills' className="pull-left" activeKey={newFilter} onSelect={this.props.toggleShows}>
                   <NavItem eventKey={false} >All upcoming episodes</NavItem>
                   <NavItem eventKey={true} >Only new episodes</NavItem>
                </Nav>
            </div>
        );
    }

});

module.exports = MyShowsHeader;