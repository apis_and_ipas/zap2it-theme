var React = require('react');
var _ =  require('underscore');
var MyShowsHeader = require('./MyShowsHeader.jsx');
var MyShowsListings = require('./MyShowsListings.jsx');
var MyShowsActions = require('../../actions/MyShowsActions');
var MyShowsStore = require('../../stores/MyShowsStore');
var storeMixin = require('../../shared/mixins/storeMixin');
var GridParamsStore = require('../../stores/GridParamsStore');


var MyShows = React.createClass({

    mixins: [storeMixin(MyShowsStore)],

    getInitialState: function() {
        return MyShowsStore;
    },


    toggleNewShows: function() {
       MyShowsActions.toggleNewShows(); 
    },

    getShowData: function(){
        var newFilter = this.state.get('newFilter');
        var showsData = this.state.get('showsData');


        // otherwise, filter for shows with new episodes airing.
        var newShowsData = [];
        showsData.forEach(function(show){
            var episodes = show.episodes;
            
            Object.keys(episodes).map(function(episode, index) {
                if (episodes[episode].isNew) {
                     newShowsData.push(show)
                }
            });
            
        });

        // if the 'new' filter isnt set, return all the showDat
        if (!newFilter) return showsData; 


        return newShowsData;
    },

    render: function(){
        var lineupId = GridParamsStore.get('lineupId');
        var isLoading = this.state.get('isLoading'); 
        var showsIds = this.state.get('myShowsIds'); 
        var showsData = this.getShowData();

        return <div className="my-shows">
            <MyShowsHeader newFilter={this.state.get('newFilter')} toggleShows={this.toggleNewShows}/>
            <MyShowsListings showsData={showsData} isLoading={isLoading}/>
        </div>;
    }

});

module.exports = MyShows;
