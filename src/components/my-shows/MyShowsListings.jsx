var React = require('react');
var MyShowsListing = require('./MyShowsListing.jsx');
var Spinner = require('../spinner.jsx');
var ListGroupBy = require('../ListGroupBy.jsx');
var _ = require('underscore');


var HeaderComponent = React.createClass({

    render: function() {
        return (
            <div>
            {this.props.item}
            </div>
        );
    }

});

var EmptyListings = React.createClass({
    render: function() {
        return (
            <div className="my-shows__listings--empty"> 
                <div className="row">
                    <div className="col-md-6 col-md-push-3">
                        No new episodes in the next 24 hours.
                    </div>
                </div>
            </div>
        );
    }
})

 
var MyShowsListings = React.createClass({

    render: function() {
        // return the spinner element if shows have yet to load
        if (this.props.isLoading) return <Spinner/>;

        var showsData = this.props.showsData;
        console.log('typeof showsData', typeof showsData)
        console.log('isEmpty showsData', _.isEmpty(showsData))
        var output = [];

        if (_.isEmpty(showsData)) {
            output.push(<EmptyListings/>);
        } else {
            for (var key in showsData) {
                if (showsData.hasOwnProperty(key)) {
                    output.push( <MyShowsListing key={key} showData={showsData[key]} /> );
                }
            }
        }

        return (
            <div className='my-shows__listings' >
                {output}
            </div>
        );
    }

});

module.exports = MyShowsListings;