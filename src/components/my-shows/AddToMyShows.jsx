var React = require('react');
var ModalActions = require('../../actions/ModalActions')
var MyShowsActions  = require('../../actions/MyShowsActions');
var UserStore  = require('../../stores/UserStore');
var MyShowsStore  = require('../../stores/MyShowsStore');
var classSet = require('react/lib/cx');

var AddToMyShows = React.createClass({

    getInitialState: function() {
        return {
            isHearted: MyShowsStore.isHearted(this.props.seriesId)
        };
    },

    addToMyShows: function (seriesId) {
        MyShowsActions.addToMyShows(seriesId);
    },

    removeFromMyShows: function (seriesId) {
        MyShowsActions.removeFromMyShows(seriesId);
    },

    handleClick: function (event) {
        var seriesId =  this.props.seriesId;

        if ( UserStore.isCurrentUserAuthorized() ) {

            if (MyShowsStore.isHearted(seriesId)) {

                //optimistically update UI
                this.setState({
                    isHearted: false
                });

                this.removeFromMyShows(seriesId);
            } else {

                //optimistically update UI
                this.setState({
                    isHearted: true
                });

                this.addToMyShows(seriesId);
            }

        } else {
            ModalActions.show('Sign Up or Log In');
        }
    },


    render: function() {

        var isHearted = this.state.isHearted;

        var btnText = isHearted ? "In my shows" : "Add to my shows";

        var btnClasses = classSet({
            'btn': true,
            'add-to-my-shows-btn': true,
            'btn-primary': !isHearted,
            'btn-default': isHearted,
            'is-hearted' : isHearted
        });

        var iconClasses = classSet({
            'fa': true,
            'fa-heart-o': !isHearted,
            'fa-heart': isHearted
        });

        return (
            <button className={btnClasses} onClick={this.handleClick}>
                <i className={iconClasses}></i>&nbsp;
                {btnText}
            </button>
        );
    }

});

module.exports = AddToMyShows;
