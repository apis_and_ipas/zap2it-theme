var moment = require('moment-timezone');
var TWENTY_FOUR_HOURS = 60 * 60 * 24;
var TWO_WEEKS = TWENTY_FOUR_HOURS * 14;
var rounder = require('../../shared/helpers/timestampRounder');
var log = require('../../shared/helpers/logger');
var nowTimestamp  = rounder.round();
var nowPlusTwoWeeks = nowTimestamp + TWO_WEEKS;
var ProviderStore = require('../../stores/ProviderStore');


function formatForDate (timestamp) { 
    var timezone = ProviderStore.get('timezone');
    return moment(timestamp * 1000).tz(timezone).format('ddd, MMM D');
}

function formatForTime (timestamp) {
    var timezone = ProviderStore.get('timezone');
    return moment(timestamp * 1000).tz(timezone).format('h:mm a');
}

function subDay(timestamp) {
    return formatForDate(timestamp - TWENTY_FOUR_HOURS);
}

function addDay(timestamp) {
    return formatForDate(timestamp + TWENTY_FOUR_HOURS);
}


var formattedNow = formatForDate(nowTimestamp);  
var nowMinusDay = subDay(nowTimestamp); 
var nowPlusDay = addDay(nowTimestamp); 


/**
 * builds an array of objects containing the timestamp and display names,
 * starting with 24 hours ago up to 13 days in the future.
 * 
 * @param  {String|Number}    timestamp Unix timestamp for starting point
 * @return {Array}
 */
var forTimes = function(timestamp, timezone) {
    var arr = [];

    // convert to timestamp Number in Milliseconds
    var timestamp = parseInt(timestamp, 10) * 1000;
    
    // primetime starts at 7pm Central/Mountain, 8pm Eastern/Pacific
    var primetime = (timezone === "America/Chicago") || (timezone === "America/Denver") ? 19 : 20;

    arr[0] = {
        label: 'Now',
        time: nowTimestamp
    };

    //"Primetime"
    arr[1] = {
        label: "Primetime",
        // set the target date to the timezone-adjusted timestamp
        time: moment.tz(timestamp, timezone).set({
            'hour': primetime,
            'minutes': 0,
            'seconds': 0,
            'milliseconds': 0
        }).unix()
    };

    //"Daytime"
    arr[2] = {
        label: "Daytime",
        // set the target date to the timezone-adjusted timestamp
        time: moment.tz(timestamp, timezone).set({
            'hour': 9,
            'minutes': 0,
            'seconds': 0,
            'milliseconds': 0
        }).unix()
    };

    for (var i = 0; i <= 23; i++) { 
        
        var setting = {
            'hour': i,
            'minutes': 0,
            'seconds': 0,
            'milliseconds': 0
        };

        arr.push({ 
            label:  moment(timestamp).set(setting).tz(timezone).format('h:mm a'),  
            time: moment(timestamp).set(setting).tz(timezone).unix()
        });
    }

    return arr;
};


/**
 * builds an array of objects containing the timestamp and display names,
 *
 * @param  {String|Number}    timestamp Unix timestamp for starting point
 * @return {Array}
 *
 *  useful for debugging: "dddd, MMMM Do YYYY, h:mm:ss a"
 */
var forDates = function (timestamp) {
    var arr = [];
    var timestamp = parseInt(timestamp, 10);

    arr[0] = {
        label: 'Yesterday',
        time: nowTimestamp - TWENTY_FOUR_HOURS
    };

    arr[1] = {
        label: 'Today',
        time: nowTimestamp
    };

    arr[2] = {
        label: 'Tomorrow',
        time: nowTimestamp + TWENTY_FOUR_HOURS
    };

    for (var i = 2; i <= 15; i++) {
        
        var time = nowTimestamp + TWENTY_FOUR_HOURS * (i - 1);
 
        // never include more than 14 days, but don't include days
        //  further than 14 days in the future
        if ( !(time > nowPlusTwoWeeks) ) {
             arr.push({ 
                label: formatForDate(time),
                time: moment(time * 1000).unix()
            });
        }
    }

    return arr;
};


/**
 * returns the appropriate label for the JumpToDate menu
 * @param  {Number} timestamp the date to be formatted
 * @return {[type]}           [description]
 */
var formatDateLabel =  function(timestamp) {
    var formattedDate = formatForDate(timestamp);

    if (formattedDate === nowMinusDay) {
        return "Yesterday";
    } else if (formattedDate === formattedNow) {
        return "Today";
    } else if (formattedDate === nowPlusDay) {
        return "Tomorrow";
    }

    return formattedDate;
};


/**
 * [formatTimeLabel description]
 * @param  {[type]} timestamp [description]
 * @param  {[type]} timezone  [description]
 * @return {[type]}           [description]
 */
var formatTimeLabel = function(timestamp, timezone) {
    var formattedTime = formatForTime(timestamp);
    var formattedNow = formatForTime(nowTimestamp);

    // primetime starts at 7pm Central/Mountain, 8pm Eastern/Pacific
    var primetime = (timezone ==="America/Chicago" ) || (timezone ==="America/Denver" ) ? "7:00 pm" : "8:00 pm";

    if (formattedTime === formattedNow){
        return "Now";
    } else if  (formattedTime === primetime) {
        return "Primetime";
    } else if ( formattedTime === "9:00 am") {
        return "Daytime";
    }

    return formattedTime;
}

module.exports = {
    forTimes: forTimes,
    forDates: forDates,
    formatDateLabel: formatDateLabel,
    formatTimeLabel: formatTimeLabel
};
