var React = require('react');
var moment = require('moment');
var GridActions = require('../../actions/GridActions');
var timestampRounder = require('../../shared/helpers/timestampRounder');
var log = require('../../shared/helpers/logger');

var JumpToDate = React.createClass({

    getCurrentSelection: function(){
        return this.state.currentSelection;
    },

    handleClick: function (selection, event) {
        event.preventDefault();

        this.setState({
            currentSelection: selection
        });

        GridActions.timeJump(selection.time);
    },

    render: function(){

        var menuLabels = this.props.menuLabels;
        var label = this.props.label;

        var menuitems = menuLabels.map(function (item, index) {
            if (index === 3) return <li key={index} role="separator" className="divider"></li>;
            return (<li key={index} onClick={this.handleClick.bind(this, item)}>
                <a href="#"> {item.label} </a>
            </li>);
        }, this);

        return (
            <div className="dropdown jump-to-date" style={{display: 'inline-block'}}>
                <button className="btn btn-default dropdown-toggle" type="button" id="jump-to-date" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {label}
                    &nbsp; <span className="caret"></span>
                </button>
                <ul className="dropdown-menu" aria-labelledby="jump-to-date">
                    {menuitems}
                </ul>
            </div>
        );


    }
});

module.exports = JumpToDate;
