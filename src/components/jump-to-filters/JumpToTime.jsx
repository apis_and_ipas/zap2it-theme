var React = require('react');
var GridActions = require('../../actions/GridActions');
var GridParamsStore = require('../../stores/GridParamsStore');
var UserStore = require('../../stores/UserStore');
var log = require('../../shared/helpers/logger')


var JumpToTime = React.createClass({

    getInitialState: function () {
        return {
            currentSelection: {
                label: "Now"
            }
        };
    },

    getCurrentSelection: function(){
        return this.state.currentSelection;
    },

    handleClick:function (selection, event) {
        event.preventDefault();

        this.setState({
            currentSelection: selection
        });

        GridActions.timeJump(selection.time);
    },

    render: function(){

        var menuLabels = this.props.menuLabels;
        var label = this.props.label

        var menuitems = menuLabels.map(function (item, index) {
            if (index === 3) return <li key={index} role="separator" className="divider"></li>;
            return (<li key={index} onClick={this.handleClick.bind(this, item)}>
                <a href="#"> {item.label} </a>
            </li>);
        }, this);


        return (
            <div className="dropdown jump-to-time" style={{display: 'inline-block'}}>
                <button className="btn btn-default dropdown-toggle" type="button" id="jump-to-time" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {label}
                    &nbsp; <span className="caret"></span>
                </button>
                <ul className="dropdown-menu" aria-labelledby="jump-to-time">
                    {menuitems}
                </ul>
            </div>
        );
    }
});

module.exports = JumpToTime;
