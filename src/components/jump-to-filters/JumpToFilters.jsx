var React     = require('react');
var GridParamsStore = require('../../stores/GridParamsStore');
var ProviderStore = require('../../stores/ProviderStore');
var storeMixin = require('../../shared/mixins/storeMixin');
var buildFilterLabels = require('./buildFilterLabels');
var JumpToDate = require('./JumpToDate.jsx');
var JumpToTime = require('./JumpToTime.jsx');


var JumpToFilters = React.createClass({

    mixins: [storeMixin(GridParamsStore)],

    getInitialState: function () {
        return GridParamsStore
    },

    render: function(){

        var gridTime = this.state.get('time');
        var timezone = ProviderStore.get('timezone');

        var dateMenuLabels = buildFilterLabels.forDates(gridTime),
            timeMenuLabels = buildFilterLabels.forTimes(gridTime, timezone);

        return (<span className="jump-to-filters">
            <span className="jump-to-filters__label">Jump To:</span>
            <JumpToDate label={buildFilterLabels.formatDateLabel(gridTime)} menuLabels={dateMenuLabels}/>
            <JumpToTime label={buildFilterLabels.formatTimeLabel(gridTime, timezone)} menuLabels={timeMenuLabels}/>
        </span>);
    }
});

module.exports = JumpToFilters;
