var React = require('react');
var ModalActions  = require('../../actions/ModalActions');
var UserActions  = require('../../actions/UserActions');
var UserStore  = require('../../stores/UserStore');
var FavoriteChannelsStore = require('../../stores/FavoriteChannelsStore');
var storeMixin =  require('../../shared/mixins/storeMixin');

var StarChannel = React.createClass({

    mixins: [storeMixin(FavoriteChannelsStore)],

    getInitialState: function() {
        return FavoriteChannelsStore;
    },

    toggleMouseOver: function (channelId) {
        this.state.set('hoveredChannel', channelId);
    },

    mouseOut: function(channelId) {
        this.state.set('hoveredChannel', null);
    },

    handleClick: function (event) {
        var channelId =  this.props.channelId;
        var isStarred = this.state.isStarred(channelId);
        
        if ( UserStore.isCurrentUserAuthorized() ) {
            if (isStarred) {
                UserActions.removeFromFavChannels(channelId);
            } else {
                UserActions.addToFavChannels(channelId);
            }

        } else {
            ModalActions.show('Sign Up or Log In');
        }
    },

    render: function() {
        var channelId = this.props.channelId;
        var isStarred = this.state.isStarred(channelId);
        var isHovered = this.state.isHovered(channelId);

        var starOrNot = (isHovered || isStarred) ? 'fa-star' : 'fa-star-o';

        return (
            <div className={'star fa ' + starOrNot} onClick={this.handleClick} onMouseOver={this.toggleMouseOver.bind(null, channelId)} onMouseOut={this.mouseOut.bind(null, channelId)}></div>
        );
    }

});

module.exports = StarChannel;
