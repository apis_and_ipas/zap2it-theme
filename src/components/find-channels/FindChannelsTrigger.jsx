var React       = require('react');
var Button      = require('react-bootstrap/lib/Button');
var ModalActions = require('../../actions/ModalActions');
var FindChannelsModal = require('./FindChannelsModal.jsx');

var FindChannelTrigger = React.createClass({
    
    handleClick: function() {
        ModalActions.show('Find Channels');
    },

    render: function() {

        return (
            <Button className='find-channels-trigger' onClick={this.handleClick}>
                <i className="fa fa-search"></i>
                <span>Find Channels</span>
            </Button>
        );
    }

});

module.exports = FindChannelTrigger;
