var React = require('react');
var FavoriteChannel =  require('./FavoriteChannel.jsx');
var goToByScroll = require('../../shared/helpers/goToByScroll');
var ModalActions = require('../../actions/ModalActions');
var ProviderStore = require('../../stores/ProviderStore');
var GridStore = require('../../stores/GridStore');


var FindChannelsResult = React.createClass({
    
    handleClick: function(callSign) {
        var stickyState = GridStore.get('stickyState');

        if (stickyState) {
            goToByScroll('#channel_' + callSign, 110);
        } else {
            goToByScroll('#channel_' + callSign, 200);
        }
        
        ModalActions.hide();
    },

    render: function() {
        var channel = this.props.channel;

         // do not display channel # for default lineups
        var channelNo = !ProviderStore.get('isDefaultProvider') ? channel.channelNo : null;

        // default lineups may have an Affiliate callsign, which is better to display
        var callSign = channel.affiliateCallSign ? channel.affiliateCallSign : channel.callSign
        
        return (
            <div className="find-channels__result-item" >
                <div className="col-md-2" style={{minHeight: '40px'}} onClick={this.handleClick.bind(null, callSign)}>
                    <img src={channel.thumbnail} />
                </div>
                <div className="col-md-4" style={{minHeight: '40px'}} onClick={this.handleClick.bind(null, callSign)}>
                    {channelNo} &nbsp;
                    {callSign}  
                </div>
                <div className="col-md-6">
                    <FavoriteChannel channelId={channel.channelId}/>
                </div>
                  
            </div>
        );
    }

});

module.exports = FindChannelsResult;