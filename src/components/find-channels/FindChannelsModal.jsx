var React = require('react');
var GridActions = require('../../actions/GridActions');
var ModalActions = require('../../actions/ModalActions');
var ModalStore = require('../../stores/ModalStore');
var GridStore = require('../../stores/GridStore');
var ChannelStore = require('../../stores/ChannelStore');
var ProviderStore = require('../../stores/ProviderStore');
var storeMixin = require('../../shared/mixins/storeMixin');
var FindChannelsResult = require('./FindChannelsResult.jsx');

var $ = require('jquery');
var _ = require('underscore');

var EmptyResult = React.createClass({

    render: function() {
        return (
            <div className="empty-result text-center find-channels__result-item">
                <em style={{color:'#b1b1b1'}}>Nothing to see here...</em>
            </div>
        );
    }

});

var FindChannelsModal = React.createClass({

    mixins: [storeMixin(GridStore)],

    getInitialState: function() {
        return {
            query: ''
        };
    },

    componentDidMount: function() {
        this.refs.query.getDOMNode().focus();
        $(document).on("keyup", this.handleKeyup);
    },

    componentWillUnmount: function() {
        $(document).off("keyup", this.handleKeyup);
    },

    seeStarred: function() {
        GridActions.setFilter('starred');
        ModalActions.hide();
    },

    handleKeyup:function(event) {
        // get the search string and normalize it's formatting
        var query = this.refs.query.getDOMNode().value.trim().toUpperCase();

        this.setState({
            query: query
        });
    },

    clearQuery: function() {
        this.refs.query.getDOMNode().value = '';
        this.setState({
            query: ''
        });
    },

    render: function() {

        var allChannels = GridStore.get('allChannels');
        var query = this.state.query;
        var displayChannels;

        // if our query isnt empty
        if ( !_.isEmpty(query) ) {
            // apply the filter
            displayChannels  = _.filter(allChannels, function(channel) {
                //remove front-padding and convert back to string
                var channelNo = "" + parseInt(channel.channel, 10); 

                // return channel if it matched the callSign or channel #
                // after coersing channel number to string
                return channel.callSign.indexOf(query) >= 0 ||
                       (channelNo).indexOf(query) >= 0;
            });

        } else {
            // otherwise retuern all the channels
            displayChannels = allChannels;
        }

        // X btn to clear to search field
        var clearBtn = query === '' ? null : <button className="btn btn-default clear-form" onClick={this.clearQuery}>&times;</button>
        
        var content;
        if (displayChannels.length >= 1) {
            content = displayChannels.map(function(channel, index) {
                return <FindChannelsResult channel={channel} key={index}/>
            }, this)
        } else {
            content = <EmptyResult />
        }
        

        return (

                <div className="find-channels">
                    <div className='modal-body'>
     
                        <div className="form-group">
                            <input type="text" ref="query" className="form-control"   placeholder={query ? query : "Search for a channel"}/>
                            {clearBtn}
                        </div>

                        <div className="find-channels__results">
                            { content }
                        </div>

                        <div className="find-channels__footer">
                            <a href="#" className="btn btn-primary pull-right" onClick={this.seeStarred}>Go to my starred channels</a>
                            <br/>
                        </div>
                    </div>
                </div>
        );
    }

});

ModalStore.registerModal('Find Channels', FindChannelsModal);

module.exports =  FindChannelsModal;
