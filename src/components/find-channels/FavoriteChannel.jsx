var React = require('react');
var FavoriteChannelsStore = require('../../stores/FavoriteChannelsStore');
var storeMixin =  require('../../shared/mixins/storeMixin');
var UserStore  = require('../../stores/UserStore');
var ModalActions  = require('../../actions/ModalActions');
var UserActions  = require('../../actions/UserActions');

var FavoriteChannel = React.createClass({

    mixins: [storeMixin(FavoriteChannelsStore)],

    getInitialState: function() {
        return {
            isStarred: FavoriteChannelsStore.isStarred(this.props.channelId)
        };
    },

    handleClick: function() {
        var channelId = this.props.channelId;
        var isStarred = this.state.isStarred;

        if ( UserStore.isCurrentUserAuthorized() ) {
            
            //toggle star state  
            this.setState({ isStarred: !isStarred });

            if ( isStarred ) {
                UserActions.removeFromFavChannels(channelId);
            } else {
                UserActions.addToFavChannels(channelId);
            }
        } 
        else {
            ModalActions.show('Sign Up or Log In');
        }
    },

 
    render: function() {    
        var channelId = this.props.channelId;   
        var isStarred = this.state.isStarred;

        var btnText   = 'Add to starred channels',
            iconClass = 'fa-star-o',
            btnClass  = 'btn-primary';
        
        if (isStarred) {
            btnText   =  'In starred channels';
            iconClass = 'fa-star';
            btnClass  = 'btn-default';
        } 

         return (<button className={btnClass + " btn pull-right fav-channel"} onClick={this.handleClick}>
                <i className={"fa " + iconClass}></i>
                <span>{btnText}</span>
            </button>);
    }

});

module.exports = FavoriteChannel;