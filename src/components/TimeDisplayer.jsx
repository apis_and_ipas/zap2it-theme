var React = require('react');
var UserStore = require('../stores/UserStore');
var storeMixin = require('../shared/mixins/storeMixin');
var moment = require('moment-timezone');

var TimeDisplayer = React.createClass({

    mixins: [storeMixin(UserStore)],

    getInitialState: function () {
        return UserStore;
    },

    render: function() {
        console.log('TimeDisplayer rerender!');
        var rawtime = this.props.time;
        var format  = this.props.format || 'h:mm A';
        var timezone = this.state.getTimezone();
        console.log("timezone", timezone);
        // transpose all passed in times values  to user-set timezone
        var displayTime = moment.tz(rawtime, timezone);
        return (
             <span>{displayTime.format(format)}</span>
        );
    }

});

module.exports = TimeDisplayer;
