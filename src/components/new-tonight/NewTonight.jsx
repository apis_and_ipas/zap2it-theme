var React = require('react');
var storeMixin = require('../../shared/mixins/storeMixin');
var NewTonightActions = require('../../actions/NewTonightActions');
var NewTonightStore = require('../../stores/NewTonightStore');
var NewTonightHeader = require('./NewTonightHeader.jsx');
var NewTonightListings = require('./NewTonightListings.jsx');
var ProviderStore = require('../../stores/ProviderStore');
var GridParamsStore = require('../../stores/GridParamsStore');


var NewTonight = React.createClass({

    mixins: [storeMixin(NewTonightStore)],

    getInitialState: function() {
        return NewTonightStore;
    },

    componentDidMount: function() {
        NewTonightActions.init({
            lineupId: GridParamsStore.get('lineupId')
        });
    },

    toggleFilter: function(filter) {
        NewTonightActions.toggleFilter(filter);
    },

    render: function(){

        var filter = this.state.get('filter');
        var isDefaultProvider = ProviderStore.get('isDefaultProvider');
        var timezone = ProviderStore.get('timezone');
        var showData;

        if (filter) {
            showData = this.state.get('filteredShows');
        } else {
            showData = this.state.get('allShows');
        }

        return <div className="new-tonight">
            <NewTonightHeader toggleFilter={this.toggleFilter} filter={filter} timezone={timezone}/>
            <NewTonightListings allShows={showData} isDefaultProvider={isDefaultProvider} />
        </div>;
    }

});

module.exports = NewTonight;
