var React = require('react/addons');
var _ = require('underscore');
var moment = require('moment');
var Spinner = require('../spinner.jsx');
var ListGroupBy = require('../ListGroupBy.jsx');
var GroupHeader = require('./GroupHeader.jsx');
var GroupItem = require('./GroupItem.jsx');

var EmptyResult = React.createClass({

    render: function() {
        return (
            <div className="empty-result text-center find-channels__result-item">
                <em style={{color:'#b1b1b1'}}>No Listings Found</em>
            </div>
        );
    }

});;

var NewTonightListings = React.createClass({

    render: function() {

        var allShows = this.props.allShows;

        // add slice so a cloned array is returned and sorted
        var sortedShows = allShows.slice().sort(function(a, b){    
            var titleA = a.title.toLowerCase();
            var titleB = b.title.toLowerCase();
            var timeA = +new Date(a.startTime);// to unix timstamp
            var timeB = +new Date(b.startTime);

            // fall through to sort by time
            if (timeA < timeB) return -1;
            if (timeA > timeB) return 1;

            // sort on by title
            if (titleA < titleB) return -1;
            if (titleA > titleB) return 1; 

            return 0;
        });


        return (
            <div>        
                <ListGroupBy className='new-tonight__listings' 
                    data={sortedShows} 
                    groupBy={'timeslot'} 
                    emptyComponent={EmptyResult} 
                    itemComponent={GroupItem} 
                    headerComponent={GroupHeader}
                    {...this.props} /> 
            </div>
        );
    }

});





module.exports = NewTonightListings;