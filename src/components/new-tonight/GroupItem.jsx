var React = require('react');
var moment = require('moment-timezone');
var ProviderStore = require('../../stores/ProviderStore');
var AddToMyShows = require('../my-shows/AddToMyShows.jsx');
var blackListedTitles = require('../../shared/helpers/blackListedTitles');
var slugify = require('../../shared/helpers/slugify');
var _ = require('underscore');

var GroupItem = React.createClass({
    render: function() {
        var show = this.props.item;
        var isDefaultProvider = this.props.isDefaultProvider;
        var timezone = ProviderStore.get('timezone');

        var location  = window.location.origin;
        var showSlug, showPageUrl = "#";

        if (!_.contains(blackListedTitles, show.title)) {
            showSlug = slugify(show.title);
            showPageUrl = location + '/tv/' + showSlug + '/' + show.id;   
        }  

        return (<div className="new-tonight-show">

            <div className="row">
                <div className="new-tonight-show__img">
                    <a href={showPageUrl}><img src={show.thumbnail} alt=""/></a>
                </div> 

                <div className="new-tonight-show__info">
                    <h4 className="new-tonight-show__title"><a href={showPageUrl}>{show.title}</a></h4> 
                    <h5 className="new-tonight-show__episode-title">
                        {show.episodeTitle ? show.episodeTitle : null} <br/>
                        <small>
                            {show.season ? "Season " + show.season  : null}
                            {show.episode ? ", Episode " + show.episode : null}
                            {show.flag ? ' • ' + show.flag  : null}
                        </small>
                    </h5>

                    <h6 className="new-tonight-show__episode-title">
                        {moment(show.startTime).format('h:mm a')} - {moment(show.endTime).tz(timezone).format('h:mm a')}
        
                    </h6>

                    <p className="new-tonight-show__desc">{show.desc}</p>
                    
                    <AddToMyShows seriesId={show.seriesId}/>
                    
                </div>
                
                <div className="new-tonight-show__find">
                    <div className="new-tonight-show__find-inner">
                        <div className="new-tonight-show__find-header">Find it on</div>
                        
                        {show.airings.sort(function(a, b){
                            // ensure channels or ordered when displayed
                            return parseInt(a.channel, 10) - parseInt(b.channel, 10)
                        }).map(function(airing){
                            return (
                            <div className="new-tonight-show__airing">
                                <span className="new-tonight-show__logo">
                                    <img src={airing.logo}/> 
                                </span>
                                <span>
                                    <div>{!isDefaultProvider ? airing.channel : null }</div> 
                                    <span className="new-tonight-show__callSign">
                                        {airing.callSign}
                                    </span>
                                </span>
                            </div>
                            );
                        })}

                        
                       
                    </div>
                </div>
            </div>
             
        </div>);
    }
});


module.exports = GroupItem;