var React = require('react');
var Nav = require('react-bootstrap/lib/Nav');
var NavItem = require('react-bootstrap/lib/NavItem');
var ResponsiveMixin = require('react-responsive-mixin');
var UserStore = require('../../stores/UserStore');
var ModalActions = require('../../actions/ModalActions');
var getsEarlierPrimetimeWhitelist = require('../../shared/helpers/getsEarlierPrimetime');
var _ = require('underscore');

var NewTonightHeader = React.createClass({
    
    mixins: [ResponsiveMixin],

    getInitialState: function() {
        return {
            filter: false,
            breakpoint: 'large'
        }
    },

    componentDidMount: function () {

        this.media({maxWidth: 1200}, function () {
            this.setState({breakpoint: 'small'});
        }.bind(this));

        this.media({minWidth: 1201}, function () {
            this.setState({breakpoint: 'large'});
        }.bind(this));

    },

    toggleFilter: function() {
        this.setState({
            filter: !this.state.filter
        })
        this.props.toggleFilter(!this.state.filter);
    },

    toggleLoginModal: function(){
        ModalActions.show('Sign Up or Log In');
    },

    render: function() {
        var timezone = this.props.timezone;
        var getsEarlierPrimetime = _.contains(getsEarlierPrimetimeWhitelist, timezone);
        var newTonightJumptoMenu;

        var jumpTo = (this.state.breakpoint === "large") ? <li role="presentation" className="jump-to-label"> Jump To: </li> : null;

        if (getsEarlierPrimetime) {
            newTonightJumptoMenu = (
                <ul className="nav nav-pills pull-right">
                    {jumpTo}
                    <li role="presentation"><a href="#divider-700PM" className="btn-link">7:00 PM</a></li>
                    <li role="presentation"><a href="#divider-730PM" className="btn-link">7:30 PM</a></li>
                    <li role="presentation"><a href="#divider-800PM" className="btn-link">8:00 PM</a></li>
                    <li role="presentation"><a href="#divider-830PM" className="btn-link">8:30 PM</a></li>
                    <li role="presentation"><a href="#divider-900PM" className="btn-link">9:00 PM</a></li>
                    <li role="presentation"><a href="#divider-930PM" className="btn-link">9:30 PM</a></li>
                </ul>
            );
        } else {
            newTonightJumptoMenu = (
                <ul className="nav nav-pills pull-right">
                    {jumpTo}
                    <li role="presentation"><a href="#divider-800PM" className="btn-link">8:00 PM</a></li>
                    <li role="presentation"><a href="#divider-830PM" className="btn-link">8:30 PM</a></li>
                    <li role="presentation"><a href="#divider-900PM" className="btn-link">9:00 PM</a></li>
                    <li role="presentation"><a href="#divider-930PM" className="btn-link">9:30 PM</a></li>
                    <li role="presentation"><a href="#divider-1000PM" className="btn-link">10:00 PM</a></li>
                    <li role="presentation"><a href="#divider-1030PM" className="btn-link">10:30 PM</a></li>
                </ul>

            );
        }

        var myShows = (UserStore.get('isAuthenticated')) 
            ? (<NavItem eventKey={true} ><span className="fa fa-heart"></span>&nbsp; My Shows</NavItem>)
            : (<li onClick={this.toggleLoginModal}><a href="#"><span className="fa fa-heart"></span>&nbsp; My Shows</a></li>)
            
        return (
            <div className="new-tonight__header clearfix">
                <Nav bsStyle='pills' className="pull-left" activeKey={this.state.filter} onSelect={this.toggleFilter}>
                   <NavItem eventKey={false} >All Shows</NavItem>
                    {myShows}
                </Nav>

                {newTonightJumptoMenu}

            </div>
        );
    }

});

module.exports = NewTonightHeader;