var React = require('react/addons');;
var moment = require('moment-timezone');
var ProviderStore = require('../../stores/ProviderStore');
 

var GroupHeader = React.createClass({
    render: function() {
        var group = this.props.item;
        var timezone = ProviderStore.get('timezone');
        var formated_slug = moment(group * 1000).format('hmmA');
        var formated_display = moment(group * 1000).format('h:mm a');


        return (<div className="new-tonight-divider row">
            <div className="col-md-12" id={'divider-' + formated_slug}>
                <h4>{formated_display}</h4>
            </div>
        </div>);
    }
});

module.exports = GroupHeader;