var React = require('react');
var _ = require('underscore');

// Props

// data - the array of objects
// groupBy -  the propert by which to group the objects
// itemComponent (optional) - component to render for each list Item
// headerComponent (optional) - component to render for Group header
// emptyComponent (optional) - component to render an Empty list (spinner, error view etc)
 

var ListGroupBy = React.createClass({

    propTypes: {
        data: React.PropTypes.array
    },

    getDefaultProps: function() {
        return {
            data: []
        };
    },

    getInitialState: function() {
        var keys = [];

        return {
            groups: this._group(this.props.groupBy, this.props.data, keys),
            sortedKeys: keys
        };
    },

    componentWillReceiveProps: function(nextProps) {
        var keys = [];

        if(nextProps.data !== this.props.data || nextProps.groupBy !== this.props.groupBy) {
            this.setState({
                groups: this._group(nextProps.groupBy, nextProps.data, keys),
                sortedKeys: keys
            });
        }
    },

    render: function() {
        var groups = this.state.groups,
            className = this.props.className,
            items = [],
            idx = -1,
            group;

        if ( this.props.data.length ){

            items = this.state.sortedKeys
                .reduce(function(items, key) {

                    group = groups[key];
                    items.push(this._renderGroupHeader(key));

                    for (var itemIdx = 0; itemIdx < group.length; itemIdx++) {
                        items.push(this._renderItem(key, group[itemIdx], ++idx));
                    }

                    return items;
                }.bind(this), []);

        } else {
            items = this._renderEmpty();
        }

        return (
            <ul { ..._.omit(this.props, ['data']) }
                className={ (className || '') + ' rw-list  rw-list-grouped' }
                ref='scrollable'
                role='listbox'>
                {items}
              </ul>
        );
    },


    _group: function(groupBy, data, keys){
        var iter =  typeof groupBy === 'function' ? groupBy : function(item) { return item[groupBy]; };

        // the keys array ensures that groups are rendered in the order they came in
        // which means that if you sort the data array it will render sorted,
        // so long as you also sorted by group
        keys = keys || [];

        return data.reduce(function(groups, item) {
            var group = iter(item);

            _.has(groups, group) ? groups[group].push(item)
                               : (keys.push(group), groups[group] = [item]);


            return groups;
        }, {});

    },

    _renderGroupHeader: function(group) {
        var HeaderComponent = this.props.headerComponent;

        return (<li className="grouped-list-header" key={'item_' + group}>
            { HeaderComponent ? <HeaderComponent item={group}/> : group }
        </li>);
    },


    _renderItem: function(group, item, idx) {
        var ItemComponent = this.props.itemComponent;

        return(<li className="grouped-list-item" key={'item_' + group + '_' + idx}>
            { ItemComponent ? <ItemComponent isDefaultProvider={this.props.isDefaultProvider ? this.props.isDefaultProvider : null} item={item}/> : item } 
        </li>);
    },

    _renderEmpty: function() {
        var EmptyComponent = this.props.emptyComponent;

        return  EmptyComponent ? <EmptyComponent/> : <div>Nothing to see here...</div> ;
    }

});

module.exports = ListGroupBy;