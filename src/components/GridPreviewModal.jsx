var React = require('react');
var Modal = require('react-bootstrap/lib/Modal');
var OverlayMixin = require('react-bootstrap/lib/OverlayMixin');
var getParam =  require('../shared/helpers/getParam');


var GridPreviewModal = React.createClass({

    mixins: [OverlayMixin],

    getInitialState: function() {
        return {
            isModalOpen: true
        };
    },


    handleToggle: function (event) {
        this.setState({
            isModalOpen: !this.state.isModalOpen
        });
    },

    renderOverlay: function () {

        // expose parameter to hide the modal
        var isPreview = getParam('preview');

        if (isPreview || !this.state.isModalOpen) {
          return null;
        }

        return (

            <Modal {...this.props} title="Pardon our dust!" animation={true} onRequestHide={this.handleToggle}>

                <div className='modal-body'>

                    <div className="row">
                        <div className="col-md-12">
                            <h4>{"An all new grid is coming! But it\'s not quite ready yet..."}</h4>
                            <p>{"We\'re working on a brand new version of the TV listing grid \
                                that will be faster and easier to use. In the meantime we \
                                recommend using the Classic grid, but you can check out \
                                the new grid we're working on."}</p>
                            <br/>
                            <a href="/classic-grid" className="btn btn-primary btn-block">Go to the classic grid</a>
                            <button onClick={this.handleToggle} className="btn btn-link btn-block">Or check out what we're working on</button>
                        </div>

                    </div>

                </div>

            </Modal>
        );
    },

    render: function() {
        return (<span/>);
    }

});

module.exports = GridPreviewModal;
