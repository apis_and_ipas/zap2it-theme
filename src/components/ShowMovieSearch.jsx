var React = require('react');
var ResponsiveMixin = require('react-responsive-mixin');
var $ = require('backbone').$;
var ShowMovieSearch = React.createClass({

    mixins: [ResponsiveMixin],

    getInitialState: function () {
        return {
            breakpoint: 'large'
        };
    },

    componentDidMount: function () {

        this.media({maxWidth: 1100}, function () {
            this.setState({breakpoint: 'small'});
        }.bind(this));

        this.media({minWidth: 1101}, function () {
            this.setState({breakpoint: 'large'});
        }.bind(this));

        this.bindInput();
    },

    componentWillUnmount: function() {
        this.unbindInput();
    },

    bindInput: function() {
        $(document).on("keyup", this.handleKeyup);
    },

    unbindInput: function() {
        $(document).off("keyup", this.handleKeyup);
    },

    handleKeyup: function (event) {
        var query = this.refs.query.getDOMNode().value.trim();
        if ((query.length > 0) && event.keyCode == 13) {
            this.submitSearch();
        }

    },

    submitSearch: function () {
        var query = this.refs.query.getDOMNode().value.trim();
        var urlEncodedQuery = encodeURIComponent(query);
        window.location = window.location.origin + '/?s=' + urlEncodedQuery;
    },


    /*
        UI Stubbed for styling purposes
    */
    render: function() {

        var bp = this.state.breakpoint,
            output;

        switch (true) {

            case bp == 'small':
                this.unbindInput();
                output = null;
                // output =  (
                //     <button className="btn btn-default navbar-btn"><i className="fa fa-search"></i></button>
                // );
            break;

            case bp == 'large':
                this.bindInput();
                output = (
                    <form ref="form">
                        <i className="fa fa-search"></i>
                        <input type="text" ref="query" placeholder="Find Shows and movies..." />
                    </form>

                );
            break;

        }
        return  (
            <div>
                {output}
            </div>
        );
    }

});

module.exports = ShowMovieSearch;
