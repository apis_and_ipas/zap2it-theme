var React = require('react');
var storeMixin  = require('../../shared/mixins/storeMixin');
var UserStore = require('../../stores/UserStore');
var Input = require('react-bootstrap/lib/Input');
var Alert = require('react-bootstrap/lib/Alert');
var classSet = require('react/lib/cx');
var UserActions = require('../../actions/UserActions');
var AccountSettingsStore = require('../../stores/AccountSettingsStore');
var _ = require('underscore');
var isEmail = require('../../shared/helpers/isEmail');

 

var buildValidPayload = function (rawPayload) {

    var payload = {};

    if ( ! _.isEmpty(rawPayload.newName) ) {
        payload.firstName = rawPayload.newName;
    }

    if ( ! _.isEmpty(rawPayload.newEmail) ) {
        payload.email = rawPayload.newEmail;
    }

    if ( ! _.isEmpty(rawPayload.newPassword) ) {
        payload.password = rawPayload.newPassword;
    }

    return payload;
};



var AccountSettings = React.createClass({

    mixins: [storeMixin(AccountSettingsStore)],

    getInitialState: function () {
        return AccountSettingsStore;
    },

    getInputValueByRef: function(refName) {
        return this.refs[refName] && this.refs[refName].getDOMNode().value.trim();
    },

    handleSubmit: function (event) {
        event.preventDefault();

        var rawPayload = {
            newName: this.getInputValueByRef('name'),
            newEmail: this.getInputValueByRef('email'),
            newPassword: this.getInputValueByRef('new_password'),
            newPasswordConfirm: this.getInputValueByRef('new_password_confirm')
        };

        // Catch password match errors
        if (rawPayload.newPassword !== rawPayload.newPasswordConfirm) {

            this.state.set({ passwordMatchError: true });

        } else if ( !isEmail(rawPayload.newEmail) ) {

            this.state.set({ emailFormatError: true });

        } else {

            // build validated payload from rawPayload
            var payload  = buildValidPayload(rawPayload);

            // fire off update to Parse
            UserActions.updateProfile(payload);

        }
    },

    render: function() {

        // current user with current values
        var user = UserStore.get('user');
        var currentName = user.get('firstName');
        var currentEmail = user.get('email');

        // error msg for New Password mismatch
        var passwordMsg = <Alert bsStyle="danger">{"Password confirmation doesn\'t match!"}</Alert>;
        var passwordMatchErrorMsg = this.state.get('passwordMatchError') ? passwordMsg : false;

        // conditional classes for password input
        var passwordInputClasses = classSet({
            'form-group': true,
            'form-group-lg': true,
            'has-error': this.state.get('passwordMatchError')
        })

        // error msg for New Password mismatch
        var emailErrorMsg = <Alert bsStyle="danger">{"Please enter a valid email address"}</Alert>;
        var emailErrorMsgOutput = this.state.get('emailFormatError') ? emailErrorMsg : false;

        // conditional classes for password input
        var emailInputClasses = classSet({
            'form-group': true,
            'form-group-lg': true,
            'has-error': this.state.get('emailFormatError')
        })

        // Success msg
        var successMsg = <Alert bsStyle="success">{"Profile updated successfully!"}</Alert>;
        var successMsgOutput = this.state.get('successState') ? successMsg : false;



        return (
            <div className="account-settings">

                <h3>Account Settings</h3>

                {passwordMatchErrorMsg}
                {emailErrorMsgOutput}
                {successMsgOutput}

                <form className="form-horizontal" ref="form">
                    <fieldset>
                        <legend>Your details</legend>

                        <div className="form-group form-group-lg">
                            <label htmlFor="name" className="col-sm-4 control-label">Name</label>
                            <div className="col-sm-8">
                                <input type="text" className="form-control" id="name" ref="name" placeholder={currentName} />
                            </div>
                        </div>
                        <div className={emailInputClasses}>
                            <label htmlFor="email" className="col-sm-4 control-label">Email</label>
                            <div className="col-sm-8">
                                <input type="email" className="form-control" id="email" ref="email" placeholder={currentEmail} />
                            </div>
                        </div>

                    </fieldset>

                    <fieldset>
                        <legend>Change password</legend>
                        <div className={passwordInputClasses}>
                            <label htmlFor="name" className="col-sm-4 control-label">New Password</label>
                            <div className="col-sm-8">
                                <input type="password" className="form-control" id="new_password" ref="new_password" />
                            </div>
                        </div>
                        <div className={passwordInputClasses}>
                            <label htmlFor="email" className="col-sm-4 control-label">Confirm Password</label>
                            <div className="col-sm-8">
                                <input type="password" className="form-control" id="new_password_confirm" ref="new_password_confirm" />
                            </div>
                        </div>
                    </fieldset>

                    <div className="form-group">
                        <div className=" col-sm-12">
                            <button onClick={this.handleSubmit} type="submit" className="btn btn-primary btn-block btn-lg">Save Changes</button>
                        </div>
                    </div>

                </form>
            </div>
        );
    }

});

module.exports = AccountSettings;
