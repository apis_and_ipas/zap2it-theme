var React = require('react');
var storeMixin = require('../shared/mixins/storeMixin');
var GridStore = require('../stores/GridStore');
var GridParamsStore = require('../stores/GridParamsStore');
var ProviderStore = require('../stores/ProviderStore');
var moment = require('moment');

/**
 * This is a helper component used to display debug info
 */

var GridParamsDebug = React.createClass({
    
    mixins: [
        storeMixin(GridStore),
        storeMixin(GridParamsStore),
        storeMixin(ProviderStore)
    ],

    getInitialState: function() {
        return {
            grid: GridStore,
            gridParams: GridParamsStore,
            provider: ProviderStore
        };
    },
    
    render: function() {
        
        return (
            <div className="well" style={{ 'position': 'fixed', 'bottom': 0, 'left': 0, 'zIndex': '99999'}}>
                GridStore: <br/>
                <pre><code>
                    scrollTop: {this.state.grid.get('scrollTop')} <br/>
                    gridHeight: {this.state.grid.get('gridHeight')}<br/>
                </code></pre>

                GridParamsStore: <br/>
                <pre><code>
                    lineupId: {this.state.gridParams.get('lineupId')} <br/>
                    timespan: {this.state.gridParams.get('timespan')}<br/>
                    time:     {this.state.gridParams.get('time')} <br/>
                    time (formatted): {moment(this.state.gridParams.get('time') * 1000).format('MMMM Do YYYY, h:mm:ss a')} <br/>
                </code></pre>

                ProviderStore: <br/>
                 <pre><code>
                    timezone:   {this.state.provider.get('timezone')} <br/>
                    lineupId:   {this.state.provider.get('lineupId')}<br/>
                    postalCode: {this.state.provider.get('postalCode')} <br/>
                    name:       {this.state.provider.get('name')} <br/>
                    isDefaultProvider:  {""+this.state.provider.get('isDefaultProvider')} <br/>
                </code></pre>
            </div>
        );
    }

});

module.exports = GridParamsDebug;