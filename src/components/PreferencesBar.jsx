var React = require('react');
var storeMixin = require('../shared/mixins/storeMixin');
var GridStore = require('../stores/GridStore');
var ProviderStore = require('../stores/ProviderStore');
var ProviderSearchStore = require('../stores/ProviderSearchStore');
var ProviderSearchActions = require('../actions/ProviderSearchActions');
var ChangeProvider = require('./change-provider/ChangeProvider.jsx');


var React = require('react');

var ProviderSearch = React.createClass({

    mixins: [storeMixin(ProviderSearchStore)],

    getInitialState: function() {
        return ProviderSearchStore;
    },

    render: function() {
        return (<span>
            <ChangeProvider/>
        </span>);
    }

});



var PreferencesBar = React.createClass({

    mixins: [storeMixin(ProviderStore)],

    getInitialState: function() {
        return ProviderStore;
    },

    render: function() {

        var postalCode = this.state.get('postalCode'),
            lineupName = this.state.get('name'),
            isDefaultProvider = this.state.get('isDefaultProvider');

        return (
            <div className="row">
                <div className="preferences-bar col-md-12">
                    <h3 className="pull-left">

                        {lineupName} {!isDefaultProvider ? "(" + postalCode + ")" : null}  &nbsp;&nbsp;
                        <ProviderSearch />
                    </h3>


                    <div className="switch-to-classic pull-right">
                        <a href="/new-grid-feedback/"><span className="fa fa-commenting"></span> Contact Us</a>
                    </div>
                    <div className="switch-to-classic pull-right">
                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </div>
                    <div className="switch-to-classic pull-right">
                        <a href="/classic-grid"><span className="fa fa-sign-out"></span> Switch to classic grid</a>
                    </div>
                </div>

            </div>
        );
    }

});

module.exports = PreferencesBar;
