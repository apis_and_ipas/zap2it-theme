var React = require('react');

var UpcomingEpisodesItem =  require('./UpcomingEpisodesItem.jsx');

var UpcomingEpisodesList = React.createClass({

    render: function() {

        // get the number of upcoming episodes to display
        var size = this.props.size;

        return (
            <div>
                UpcomingEpisodesList
                <UpcomingEpisodesItem/>
            </div>
        );
    }

});

module.exports = UpcomingEpisodesList;
