var React = require('react');
var UpcomingEpisodesNav = require('./UpcomingEpisodesNav.jsx');
var UpcomingEpisodesList = require('./UpcomingEpisodesList.jsx');

var UpcomingEpisodesTab = React.createClass({

    render: function() {
        return (
            <div>
                <UpcomingEpisodesNav/>
                <UpcomingEpisodesList size="4"/>
            </div>
        );
    }

});

module.exports = UpcomingEpisodesTab;
