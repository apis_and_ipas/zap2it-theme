var React = require('react');
var Nav = require('react-bootstrap/lib/Nav');
var NavItem = require('react-bootstrap/lib/NavItem');


var UpcomingEpisodesNav = React.createClass({

    render: function() {
        return (
            <div>
                <div className="pull-left">
                    <Nav bsStyle='pills' activeKey={1}>
                        <NavItem eventKey={1}>All upcoming episode</NavItem>
                        <NavItem eventKey={2}>Only new episodes</NavItem>
                    </Nav>
                </div>

                <div className="pull-right">
                    <button className='btn btn-default'>
                        <i className='fa fa-map-marker'></i> &nbsp;
                        Central Default Lineup (USA) (60001)
                    </button>
                </div>

                <div className="clearfix"></div>
            </div>
        );
    }

});

module.exports = UpcomingEpisodesNav;
