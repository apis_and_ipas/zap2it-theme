var React       = require('react');
var _ = require('underscore');
var storeMixin  = require('../../shared/mixins/storeMixin');
var ScrollListenerMixin = require('../../shared/mixins/ScrollListenerMixin');
var ResizeMixin = require('../../shared/mixins/ResizeMixin');
var ProviderStore = require('../../stores/ProviderStore');
var GridStore = require('../../stores/GridStore');
var GridParamsStore = require('../../stores/GridParamsStore');
var GridActions = require('../../actions/GridActions');
var Spinner = require('../Spinner.jsx');
var Channel = require('./Channel.jsx');
var ChannelAd = require('./ChannelAd.jsx');
var GridWrapper =  require('./GridWrapper.jsx');
var GridHeaderWrapper =  require('./GridHeaderWrapper.jsx');
var GridResults =  require('./GridResults.jsx');
var EmptyGridFilter =  require('./EmptyGridFilter.jsx');
var SponsoredChannel = require('./SponsoredChannel.jsx');
var Debug = require('../Zap2ItDebug.jsx');
var getParam =  require('../../shared/helpers/getParam');



var Grid = React.createClass({

    mixins: [
        storeMixin(GridStore),
        ResizeMixin
    ],

    getInitialState: function() {
        return GridStore;
    },

    componentDidMount: function() {

        this.getDOMNode().scrollTop = this.state.get('scrollTop');
    },

    onPageScroll: function(newScrollTop, oldScrollTop) {
        // console.log('newScrollTop', newScrollTop);
        // console.log('oldScrollTop', oldScrollTop);
    },

    onResize: function(event) {
        // console.log('resize event');
        var newGridHeight = React.findDOMNode(this).offsetHeight;
        var oldGridHeight = this.state.get('gridHeight');
        // console.log("newGridHeight", newGridHeight);
        // console.log("oldGridHeight", oldGridHeight);
        if (newGridHeight > oldGridHeight) {
            this.state.set({
                gridHeight: newGridHeight
            });
        }
    },

    render: function(){
        var displayTimes = this.state.get('displayTimes') || [],
            channels = this.state.get('channels') || [],
            channelList, content;

        var isLoading = this.state.get('isLoading');
        var activeChannel = this.state.get('activeChannel');
        var activeShow = this.state.get('activeShow');

        var isDefaultProvider = ProviderStore.get('isDefaultProvider');

        if ( !_.isEmpty(channels) && _.isArray(channels) ) {
           content = channels.map(function(channel, index){

                if ((index + 1) % 12 === 0) {
                     // every 12 channels, return the channel along with an Ad row
                     return (
                        <div key={index}>
                            <Channel isDefaultProvider={isDefaultProvider} data={channel} selected={activeChannel == channel.id} activeChannel={activeChannel} activeShow={activeShow}/>
                            <ChannelAd adId={'adId-' + index}/>
                        </div>
                     );

                } else {
                    return <Channel isDefaultProvider={isDefaultProvider} key={index} data={channel} selected={activeChannel == channel.id} activeChannel={activeChannel} activeShow={activeShow}/>;
                }

           });
        } else {
            content = <EmptyGridFilter/>;
        }

        var sponsoredChannel = null;
        if (this.state.get('sponsoredChannel')) {
            sponsoredChannel = <SponsoredChannel isDefaultProvider={isDefaultProvider} activeChannel={activeChannel} activeShow={activeShow} data={this.state.get('sponsoredChannel')} />
        }

        var showDebugComponent = getParam('debug');

        return (
            <GridWrapper>
                {showDebugComponent ? <Debug /> : null}
                <GridHeaderWrapper channels={channels} displayTimes={displayTimes} />
                <GridResults>
                    {sponsoredChannel}
                    {!isLoading ? content : <Spinner />}
                </GridResults>
            </GridWrapper>
        );
    }

});

module.exports = Grid;
