var React     = require('react');
var ButtonToolbar = require('react-bootstrap/lib/ButtonToolbar');
var ButtonGroup = require('react-bootstrap/lib/ButtonGroup');


var FindChannelsTrigger = require('../find-channels/FindChannelsTrigger.jsx');
var GridPreferencesTrigger = require('../grid-preferences/GridPreferencesTrigger.jsx');
var JumpToFilters = require('../jump-to-filters/JumpToFilters.jsx');
var GridFilters = require('./GridFilters.jsx')

var GridControls = React.createClass({

    render: function(){
        return (
            <div className="row grid-controls">
                <div className="col find-channels-col">
                    <FindChannelsTrigger channels={this.props.channels}/>
                </div>

                <div className="col grid-filters-col">
                    <GridFilters />
                </div>

                <div className="col grid-prefs-col">
                    <GridPreferencesTrigger/>
                </div>

                <div className="col jump-to-col">
                    <JumpToFilters/>
                </div>

            </div>
        );
    }
});

module.exports = GridControls;
