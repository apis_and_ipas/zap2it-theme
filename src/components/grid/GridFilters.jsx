var React = require('react');
var Nav = require('react-bootstrap/lib/Nav');
var NavItem = require('react-bootstrap/lib/NavItem');

var GridActions =  require('../../actions/GridActions');
var ModalActions =  require('../../actions/ModalActions');
var ResponsiveMixin = require('react-responsive-mixin');
var UserStore = require('../../stores/UserStore');
var GridStore = require('../../stores/GridStore');

var GridFilters = React.createClass({

    mixins: [ ResponsiveMixin ],

    getInitialState: function() {
        return {
            layout: 'full',
        };
    },


    componentDidMount: function () {

        this.media({maxWidth: 767}, function () {
            this.setState({layout: 'small'});
        }.bind(this));

        this.media({minWidth:768, maxWidth: 1199}, function () {
            this.setState({layout: 'medium'});
        }.bind(this));

        this.media({minWidth: 1200}, function () {
            this.setState({layout: 'large'});
        }.bind(this));
    },

    handleSelect: function(key) {
        var currentKey = GridStore.get('filterName');

        // block "Starred" for unauthorized users. since it will be empty
        if (key === 'starred' && !UserStore.isCurrentUserAuthorized()) {
            ModalActions.show('Sign Up or Log In');
            return;
        }

        // the active filter was clicked
        if (currentKey === key) {
            key = 'all';
        }
        
        // should probably be adjust for reach layout mode
        window.scrollTo(0, 270);

        this.setState({ activeKey: key });
        GridActions.setFilter(key);
        
    },

    render: function() {

        var navItems = [];
        navItems.push(<NavItem eventKey={'all'} key={'all'} className="grid-filter grid-filter-controls__all">All Channels</NavItem>);
        navItems.push(<NavItem eventKey={'starred'} key={'starred'} className="grid-filter grid-filter-controls__starred"><i className="fa fa-star"></i>&nbsp;&nbsp;Starred</NavItem>);

        if (this.state.layout === 'large') {
            navItems.push(<NavItem eventKey={'movie'} key={'movie'} className="grid-filter grid-filter-controls__movies"><i className="fa fa-circle"></i>&nbsp;&nbsp;Movies</NavItem>);
            navItems.push(<NavItem eventKey={'sports'} key={'sports'} className="grid-filter grid-filter-controls__sports"><i className="fa fa-circle"></i>&nbsp;&nbsp;Sports</NavItem>);
            navItems.push(<NavItem eventKey={'family'} key={'family'} className="grid-filter grid-filter-controls__family"><i className="fa fa-circle"></i>&nbsp;&nbsp;Family</NavItem>);
            navItems.push(<NavItem eventKey={'news'} key={'news'} className="grid-filter grid-filter-controls__news"><i className="fa fa-circle"></i>&nbsp;&nbsp;News</NavItem>);
            navItems.push(<NavItem eventKey={'talk'} key={'talk'} className="grid-filter grid-filter-controls__talk"><i className="fa fa-circle"></i>&nbsp;&nbsp;Talk</NavItem>);
        }

        return (
            <Nav bsStyle='pills' className="grid-filter-controls" activeKey={GridStore.get('filterName')} onSelect={this.handleSelect} >
                 {navItems}
            </Nav>
        );
    }

});

module.exports = GridFilters;