 
var React = require('react');
var ChannelCard = require('./ChannelCard.jsx');
var ShowCard = require('./ShowCard.jsx');
var classSet = require('react/lib/cx');
var ShowDetailCard = require('./ShowDetailCard.jsx');

var SponsoredChannel = React.createClass({

    render: function() {


        var shows = this.props.data.events;
        var channelId = this.props.data.id;
        var callSign = this.props.data.callSign;

        var activeShow = this.props.activeShow;
        var activeChannel = this.props.activeChannel;

        var isSelectedChannel = channelId == activeChannel,
            isSelectedShow;

        var selectedShowData;

        var classList = classSet({
            'channel-selected': isSelectedChannel,
            'channel-row ': true,
            'channel-row--sponsored': true,
        });

        return (
            <div className={classList}>
                 
                <div className="row " >
                    <ChannelCard isSponsored={true} {...this.props}/>

                    <div className="channel-listings col-xs-10 col-sm-10" style={{ padding: 0 }} >
                        
                        {shows.map(function(show, index) {

                            isSelectedShow = isSelectedChannel && activeShow === index;
                            selectedShowData = shows[activeShow];

                            return (
                                <ShowCard isSponsored={true} data={show} channel={channelId} selected={isSelectedShow} index={index} key={index} />
                            );
                        })}
                    </div>
                </div>
                <div className="row">

                    {isSelectedChannel && selectedShowData ? <ShowDetailCard callSign={callSign} data={selectedShowData} isDefaultProvider={this.props.isDefaultProvider}/>  : null}

                </div>
                 
            </div>
        );
    }

});

module.exports = SponsoredChannel;
