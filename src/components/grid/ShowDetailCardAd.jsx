var React = require('react');

var ShowDetailCardAd = React.createClass({

    componentDidMount: function() {

        var googletag = window.googletag || {};
        googletag.cmd = window.googletag && window.googletag.cmd || [];

        var self = this;

        googletag.cmd.push(function() {
            var slot = googletag.defineSlot( '/7287/' + ACMTargeting.site_name + '/' + ACMTargeting.zone1, [[300,250]], "showDetailAd-"+self.props.showId)
                .addService(googletag.pubads())
                .setTargeting( 'fold', 'mid' )
                .setTargeting( 'show', self.props.showId)
                .defineSizeMapping(responsiveMappings["Box"]);
                googletag.display("showDetailAd-"+self.props.showId);
        });
    },

    render: function() {
        return (
            <div id={"showDetailAd-"+this.props.showId}></div>
        );
    }

});

module.exports = ShowDetailCardAd;