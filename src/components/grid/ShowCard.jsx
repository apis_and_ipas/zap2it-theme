var React  = require('react');
var moment = require('moment-timezone');
var classnames = require('react/lib/cx');
var _ = require('underscore');
var GridParamsStore = require('../../stores/GridParamsStore');
var ProviderStore = require('../../stores/ProviderStore');
var GridActions = require('../../actions/GridActions');

var DISPLAY_TIME_FORMAT = 'h:mm a';
/**
 * Takes the endtime for a show and returns the number of minutes to the end.
 * @param  {string} showEndtime timestamp for shows endtime
 * @return {number}             number of mintues remaining in the show
 */
var getTimeRemaining = function(showEndtime, gridStartTime){
    // gets timestamp for the shows endtime
    var end = Math.floor(+new Date(showEndtime) / 1000);
    return (end - gridStartTime) / 60 ;
};

/**
 * If a show starts but doesnt end before the grid
 * @param  {string} showEndtime - timestamp for shows endtime
 * @param  {number} duration    - a shows duration in seconds
 * @param  {string} endTime     - ending time of the current grid as Unix timestamp
 * @return {number}             - number of the show is shown in the current grid views
 */
var getTimeDisplayed = function(showStartTime, gridEndTime){
    // gets timestamp for the shows starttime
    var start = Math.floor(+new Date(showStartTime) / 1000); ;
    return (gridEndTime - start) / 60;
};



var ShowCard = React.createClass({

    handleClick: function (channel, show) {

        GridActions.toggleDetail(channel, show);
    },

    render: function() {

        var show       = this.props.data;
        var title      = (show.program.title === "SIGN OFF") ? "Local Programming" : show.program.title;

        var gridStartTime = GridParamsStore.get('time');
        var timespanInMS = 60 * 60 * GridParamsStore.get('timespan');
        var timespanInMins = 60 * GridParamsStore.get('timespan');
        var gridEndTime = gridStartTime + timespanInMS;

        var showHasStarted = moment(show.startTime).isBefore(gridStartTime * 1000);
        var showRunsLater = moment(show.endTime).isAfter(gridEndTime * 1000);

        var width = (function(){

                var width;

                if (showHasStarted && showRunsLater) {
                    width = timespanInMins;
                 } else if (showHasStarted) {
                    width = getTimeRemaining(show.endTime, gridStartTime);
                 } else if (showRunsLater) {
                    width = getTimeDisplayed(show.startTime, gridEndTime);
                 } else {
                    width = show.duration;
                 }

                return  parseFloat( (width * 100) / timespanInMins ).toFixed(5);
            })();

        // filter times through timezone as set by user
        var timezone = ProviderStore.get('timezone');

        var startTimeRaw =  moment.tz(show.startTime, timezone)
        var startTime = startTimeRaw.format(DISPLAY_TIME_FORMAT);

        var endTimeRaw   = moment.tz(show.endTime, timezone)
        var endTime   = endTimeRaw.format(DISPLAY_TIME_FORMAT);



        var isSponsored = this.props.isSponsored;
        var showImage;

        var shouldShowImage = isSponsored && (width > 33);

        // the show if sponsored and at least an hour long, show and image.
        if (shouldShowImage) {
            showImage = (
                    // "Foo"
                    // <img src={"http://zap2it.tmsimg.com/"+show.program.preferredImage.uri+"?w=55"}/>
                    <img className="show-card__sponsored-img"src={show.thumbnail} width="60"/>
                 
            )
        }

        var showDisplay =  !(width >= 5.1) ? "none" : "inline-block";

        // indicators show if a program started or ends outside of the grid window
        var indicators = [];
        
        if (showHasStarted) {
            indicators.push( <div className="pull-left show-card__ellipsis"></div> );
        }

        if (showRunsLater) {
            indicators.push( <div className="pull-right show-card__ellipsis text-right"></div> );
        } 

        var classList = classnames({
            'show-card': true,
            'has-sponsored-img': shouldShowImage,
            'selected' : this.props.selected,
            'flag-new' : show.flag == "New",
            'flag-live' : show.flag == "Live",
            'filter-movie'  : _.contains(show.filter, "filter-movie"), 
            'filter-family' : _.contains(show.filter, "filter-family"), 
            'filter-sports' : _.contains(show.filter, "filter-sports"), 
            'filter-family' : _.contains(show.filter, "filter-family"), 
            'filter-news'   : _.contains(show.filter, "filter-news"), 
            'filter-talk'   : _.contains(show.filter, "filter-talk")
        });

        return (
            <div className={classList} style={{ width: width + "%", display: showDisplay }} onClick={this.handleClick.bind(null, this.props.channel, this.props.index)}>                 
                {showImage}
                <span className="show-card__inner">
                    <h5 className="show-card__title">{title}</h5>
                    <div className="show-card__time">{startTime} - {endTime}</div>
                    <div className="clearfix">{indicators}</div>
                </span>
            </div>
        );
    }

});

module.exports = ShowCard;
