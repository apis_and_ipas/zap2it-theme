var React       = require('react');
var GridResults = React.createClass({

    render: function(){
        return (
            <div className="clearfix grid-results">
                <div style={{minHeight: '600px'}}>
                    {this.props.children}
                </div>
            </div>
        );

    }
});

module.exports = GridResults;