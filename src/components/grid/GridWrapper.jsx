var React = require('react');
var ResponsiveMixin = require('react-responsive-mixin');
var GridActions = require('../../actions/GridActions');
var GridParamsStore = require('../../stores/GridParamsStore');
var $ = require('jquery');

var GridWrapper = React.createClass({
    
    mixins: [ResponsiveMixin],

    componentDidMount: function () {
        $(window).on('resize', this.resizeHandler)

        GridActions.setTimespan(3);
    },

    componentWillUnmount: function() {
        $(window).off('resize', this.resizeHandler);
    },

    /**
     * Sets up listeners for window resize events and requeries the grid as needed
     */
    resizeHandler: function() {
        var currentTimespan = GridParamsStore.get('timespan');

        this.media({maxWidth: 767}, function () {
            if (currentTimespan !== 1) GridActions.setTimespan(1);
        }.bind(this));

        this.media({minWidth:768, maxWidth: 1199}, function () {
             if (currentTimespan !== 2) GridActions.setTimespan(2);
        }.bind(this));

        this.media({minWidth: 1200}, function () {
             if (currentTimespan !== 3)  GridActions.setTimespan(3);
        }.bind(this));
    },

    render: function() {
        return (
            <div className="GridWrapper">
                {this.props.children}
            </div>
        );
    }

});

module.exports = GridWrapper;