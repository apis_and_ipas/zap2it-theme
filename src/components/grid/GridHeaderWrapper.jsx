var React = require('react');
var Sticky = require('../Sticky.jsx');
var GridHeader = require('./GridHeader.jsx');
var GridControls = require('./GridControls.jsx');
var ResponsiveMixin = require('react-responsive-mixin');
var GridActions = require('../../actions/GridActions');


var GridHeaderWrapper = React.createClass({

     mixins: [ResponsiveMixin],

    getInitialState: function() {
        return {
            breakpoint: "large"
        }
    },

    componentDidMount: function () {

        this.media({maxWidth: 767}, function () {
            this.setState({
                breakpoint: 'small'
            })
        }.bind(this));

        this.media({minWidth:768, maxWidth: 1199}, function () {
            this.setState({
                breakpoint: 'medium'
            })
        }.bind(this));

        this.media({minWidth: 1200}, function () {
            this.setState({
                breakpoint: 'large'
            })
        }.bind(this));

    },

    stickHandler: function(event){
        // tells the state of the sticky header.
        GridActions.setStickyState(event);
    },


    render: function() {
        var channels = this.props.channels;
        var displayTimes = this.props.displayTimes;


        if (this.state.breakpoint === "small") {
            return (
                <div>
                    <GridControls channels={channels}/>
                    <Sticky onStickyStateChange={this.stickHandler}>
                        <GridHeader displayTimes={displayTimes}/>
                    </Sticky>
                </div>
            );
        }

        return (
            <div>
                <Sticky onStickyStateChange={this.stickHandler}>
                    <GridControls channels={channels}/>
                    <GridHeader displayTimes={displayTimes}/>
                </Sticky>
            </div>
        );
    }

});

module.exports = GridHeaderWrapper;