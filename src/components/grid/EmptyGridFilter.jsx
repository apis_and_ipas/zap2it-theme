var React = require('react');

var EmptyGridFilter = React.createClass({

    render: function() {
        return (
            <div className="row empty-grid-filter">
                <div className="col-md-6 col-md-push-3 text-center well" style={{ marginTop: '20px'}}>
                    <h3>Nothing in {"this"} filter!</h3>
                    <p>Hint: Try scrolling down to show more channels before chosing a filter.</p>
                    
                    <button className="btn btn-primary " onClick={this.props.loadMore}>
                        Load More
                    </button>
                </div>
            </div>
        );
    }

});

module.exports = EmptyGridFilter;

