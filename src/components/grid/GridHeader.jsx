var React     = require('react');
var ResponsiveMixin = require('react-responsive-mixin');
var moment = require('moment-timezone');

var ButtonGroup = require('react-bootstrap/lib/ButtonGroup');
var Button = require('react-bootstrap/lib/Button');
var GridActions = require('../../actions/GridActions');
var GridParamsStore = require('../../stores/GridParamsStore');
var ProviderStore = require('../../stores/ProviderStore');

var THIRTY_MINS = 30 * 60;
var TIME_DISPLAY_FORMAT = 'h:mm a';

    
var buildDisplayTimes = function (timestamp, endTime) {
    var displayTimes = [];

    // lets do math!
    // assure our timestamps are integers.
    timestamp = parseInt(timestamp, 10);
    endTime = parseInt(endTime, 10);

    // add the begining timestamp
    displayTimes.push(timestamp);

    // if we've not yet hit the endTime, inc timestamp by 30 mins
    while(timestamp < (endTime - THIRTY_MINS)) {
       timestamp += THIRTY_MINS;                 // 30 mins ins seconds
       displayTimes.push(timestamp);             // add to the collection
    }

    // return displayTimes;
    // reformat displayTimes items for display
    return displayTimes.map(function (time) {
        // convert back to ms
        return time * 1000;//moment(time * 1000).format(TIME_DISPLAY_FORMAT);
    });

};


var GridHeader = React.createClass({

    mixins: [ResponsiveMixin],



    getInitialState: function() {
        return {
            breakpoint: 'large'
        };
    },

    componentDidMount: function () {

        this.media({maxWidth: 767}, function () {
            this.setState({
               'breakpoint': 'small'
            });
        }.bind(this));

        this.media({minWidth:768, maxWidth: 1199}, function () {
            this.setState({
                'breakpoint': 'medium'
            });
        }.bind(this));

        this.media({minWidth: 1200}, function () {
              this.setState({
                  'breakpoint': 'large'
              });
        }.bind(this));

    },

    getItemWidth: function(isLastItem) {
        switch (this.state.breakpoint) {
         
            case 'small':
                return isLastItem ? "38%" : "50%";
                break;

            case 'medium':
                return isLastItem ? "19%" : "25%";
                break;

            case 'large':
            default:
                return isLastItem ? "132px" : "166px";
            
        }
    },

    pagePrev: function(){
        GridActions.pagePrev();
    },

    pageNext: function(){
        GridActions.pageNext();
    },


    render: function(){

        var timespan = GridParamsStore.get('timespan');
        var timestamp = GridParamsStore.get('time');

        var timezone = ProviderStore.get('timezone');

        var timespan_in_mins = 60 * 60 * timespan;
        var endTime = parseInt(timespan_in_mins, 10) +  parseInt(timestamp, 10);

        var displayTimes = buildDisplayTimes(timestamp, endTime);

        var last = displayTimes.length - 1; // reference to displayTimes item index

        var times = displayTimes.map(function(time, index) {
            // if the index is the last item, use a smaller width.
            var width = (index == last) ? this.getItemWidth(true) : this.getItemWidth(); 

            var displayTime = moment.tz(time, timezone);
            
            return (<Button className="grid-header__item" key={index} style={{ width:  width }}>
                {displayTime.format(TIME_DISPLAY_FORMAT)}
            </Button>);
        }, this);

        return (
            <div className="grid-header">
                <div className="btn-group-first">
                    <Button onClick={this.pagePrev}><i className="fa fa-chevron-left"></i></Button>
                </div>
                <div className="btn-group-last">
                    {times}
                    <Button onClick={this.pageNext}><i className="fa fa-chevron-right"></i></Button>
                </div>
                <div className="clearfix"/>
            </div>
        );
    }
});

module.exports = GridHeader;
