var React       = require('react');
var classnames  = require('react/lib/cx');
var StarChannel = require('../star-channel/StarChannel.jsx');
 
 

var ChannelCard = React.createClass({

    render: function() {
        var isDefaultProvider = this.props.isDefaultProvider;
        var data = this.props.data;
        var callSign = isDefaultProvider && data.affiliateCallSign ? data.affiliateCallSign : data.callSign.substr(0,6);
        var channelNum = data.channelNo;
        var channelId = data.channelId;
        var imageUrl = data.thumbnail;
        var isSponsored = this.props.isSponsored;

        var classList = classnames({
            'channel-card': true,
            'is-default-lineup': isDefaultProvider,
        });
 
                    
        return (
            <div className={classList}>
                <div className="row">

                    <div className="channel-card__logo">
                        <img src={imageUrl}/>
                    </div>

                    <div className="channel-card__meta">
                        <span className="channel-card__channelNum">{!isDefaultProvider ? channelNum : null}</span>
                        <span className="channel-card__callSign">{callSign}</span>
                    </div>

                    <div className="channel-card__star star-wrapper">
                         
                       <StarChannel channelId={channelId}/> 
                    </div>

                    
                </div>
            </div>
        );
    }

});

module.exports = ChannelCard;
