var React = require('react');
var ChannelCard = require('./ChannelCard.jsx');
var ShowCard = require('./ShowCard.jsx');
var classSet = require('react/lib/cx');
var ShowDetailCard = require('./ShowDetailCard.jsx');


var Channel = React.createClass({

    render: function() {

        var channel = this.props.data;
        var shows = channel.events;
        var channelId = channel.id;
        var callSign = channel.callSign;
       
        var activeShow = this.props.activeShow;
        var activeChannel = this.props.activeChannel;
        var isDefaultProvider = this.props.isDefaultProvider;
        var isSelectedChannel = channelId == activeChannel,
            isSelectedShow;

        var selectedShowData;

        var classList = classSet({
            'channel-selected': isSelectedChannel,
            'channel-row': true
        });

        return (
            <div className={classList} id={'channel_' + callSign}>
                <div className="row" >
                    <ChannelCard {...this.props}/> 
                        
                    <div className="channel-listings col-xs-10 col-sm-10" style={{ padding: 0 }} >
                        {shows.map(function(show, index) {

                            isSelectedShow = isSelectedChannel && activeShow === index;
                            selectedShowData = shows[activeShow];

                            return (
                                <ShowCard data={show} channel={channelId} selected={isSelectedShow} index={index} key={index} />
                            );
                        }, this)}
                    </div>
                </div>
                <div className="row">

                    {isSelectedChannel && selectedShowData ? <ShowDetailCard callSign={!isDefaultProvider ? channel.callSign : channel.affiliateCallSign} data={selectedShowData} isDefaultProvider={isDefaultProvider}/>  : null}

                </div>
            </div>
        );
    }

});

module.exports = Channel;
