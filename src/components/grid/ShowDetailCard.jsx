var React = require('react');
var moment = require('moment-timezone');
var ProviderStore = require('../../stores/ProviderStore');
var AddToMyShows = require('../my-shows/AddToMyShows.jsx');
var ShowDetailCardAd = require('./ShowDetailCardAd.jsx');
var GridActions = require('../../actions/GridActions');
var ResponsiveMixin = require('react-responsive-mixin');
var slugify = require('../../shared/helpers/slugify');
var _ = require('underscore');
var blackListedTitles = require('../../shared/helpers/blackListedTitles');

var ShowDetailCard = React.createClass({

    mixins: [ResponsiveMixin],

    getInitialState: function() {
        return {
            breakpoint: 'large'
        };
    },

    togglePanel: function(channel, show) {
        GridActions.toggleDetail(channel, show);
    },

    componentDidMount: function () {

        this.media({maxWidth: 767}, function () {
             this.setState({
                breakpoint: 'small'
             });
        }.bind(this));

        this.media({minWidth:768, maxWidth: 1199}, function () {
             this.setState({
                breakpoint: 'medium'
             });
        }.bind(this));

        this.media({minWidth: 1200}, function () {
             this.setState({
                breakpoint: 'large'
             });
        }.bind(this));
    },

    render: function() {
        var show = this.props.data;
        var seriesId = show.seriesId;
        var tmsId = show.program.tmsId;
        var title = show.program.title;
        var episodeTitle = show.program.episodeTitle;
        var season = show.program.season;
        var episode = show.program.episode;
        var desc = show.program.shortDesc;
        var imgUrl = show.thumbnail;
        var callSign = show.callSign;
        var affiliateCallSign = show.affiliateCallSign;
        var channelNo = show.channelNo;

        var timezone  = ProviderStore.get('timezone');
        var location  = window.location.origin;
        //  http://zap2it.vip.local/tv/show-slug/tmsId/
        
        var showSlug, showPageUrl = "#";

        if (!_.contains(blackListedTitles, title)) {
            showSlug = slugify(title);
            showPageUrl = location + '/tv/' + showSlug + '/' + tmsId;   
        }  

        if (this.state.breakpoint === "small"){
         
            return (
                <div className="show-detail-card">

                    <div className="col-xs-12 col-sm-12">
                        <div className="close-show-detail" 
                             onClick={this.togglePanel.bind(null, channelNo, show)}>
                             &times;
                        </div>

                        <div className="show-detail-card__img">
                           <a href={showPageUrl}><img src={imgUrl} alt=""/></a>
                       </div>


                        <h3 className="show-detail-card__title"><a href={showPageUrl}>{title}</a></h3> 
                        <h4 className="show-detail-card__episode-title">
                            {episodeTitle ? episodeTitle : null} <br/>
                            <small>
                                {season ? "Season " + season + ", " : null}
                                {episode ? "Episode " + episode : null}
                                {show.flag ? ' • ' + show.flag  : null}
                            </small>
                        </h4>

                    </div>

                    <div className="col-xs-12 col-sm-12">
                        <p className="show-detail-card__desc">{desc}</p>
                    </div>
                   
                </div>
            );
        }


        return (

                <div className="show-detail-card">

                    <div className="show-detail-card__img">
                        <a href={showPageUrl}><img src={imgUrl} alt=""/></a>
                    </div>

                    <div className="col-md-7">

                        <div className="close-show-detail" 
                             onClick={this.togglePanel.bind(null, channelNo, show)}>
                             &times;
                        </div>

                        <h3 className="show-detail-card__title">
                            <a href={showPageUrl}>{title} {(show.entityType === "Movie") ?
                                 " ("+show.program.releaseYear+")" : null}</a>
                        </h3>
                        <h4 className="show-detail-card__episode-title">
                            {episodeTitle ? episodeTitle : null}
                            <small>
                                {season ? "Season " + season + ", " : null}
                                {episode ? "Episode " + episode : null}
                                {show.flag ? ' • ' + show.flag  : null}
                            </small>
                        </h4>

                        <div className="show-detail-card__meta">
                            {moment(show.startTime).tz(timezone).format('h:mm A')}
                            &nbsp;on {this.props.callSign} 
                            {!this.props.isDefaultProvider? channelNo : null} 
                            {show.rating ? ' • ' + show.rating : null }
                            {show.tags ? show.tags.map(function (tag) {
                                return ' • ' + tag;
                            }, this) : null }
                        </div>

                        <p className="show-detail-card__desc">{desc}</p>
                        {/* only show AddToMyShows btn for series */}
                        {(show.seriesId) ? <AddToMyShows seriesId={seriesId}/> : null }
                    </div>

                    <div className="col-md-3">
                        <ShowDetailCardAd showId={show.program.id}/>
                        
                    </div>


                </div>

        );
    }

});

module.exports = ShowDetailCard;
