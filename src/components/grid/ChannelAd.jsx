var React = require('react');

var ChannelAd = React.createClass({

    componentDidMount: function() {
    
        var googletag = window.googletag || {};
        googletag.cmd = window.googletag && window.googletag.cmd || [];

        var self = this;

        googletag.cmd.push(function() {
          var slot = googletag.defineSlot( '/7287/' + ACMTargeting.site_name + '/' + ACMTargeting.zone1, [[728,90]], "channelAd-" + self.props.adId )
            .addService(googletag.pubads())
            .setTargeting( 'fold', 'mid' )
            .setTargeting( 'show', self.props.adId)
            .defineSizeMapping(responsiveMappings["Leaderboard"]);
            googletag.display("channelAd-"+ self.props.adId);
        });
    },

    render: function() {
        return (
            <div className="channel-ad"><div id={"channelAd-"+this.props.adId}></div></div>
        );
    }

});

module.exports = ChannelAd;