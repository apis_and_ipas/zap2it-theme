var React         = require('react');
var storeMixin    = require('../shared/mixins/storeMixin');
var NotifyStore   = require('../stores/NotifyStore');
var NotifyActions = require('../actions/NotifyActions');

var Notify = React.createClass({

    mixins: [storeMixin(NotifyStore)],

    getInitialState: function() {
        return NotifyStore;
    },

    onClose: function(event) {
        event.preventDefault();
        NotifyActions.hide();
    },

    viewLogIn: function (event) {
        event.preventDefault();
        NotifyActions.hide();
        this.props.goToLogIn();
    },

    render: function() {
        if (this.state.get('visible')) {

            var channel = this.props.channel;
            var itemchannel = this.state.get('channel');
            var classList = "alert alert-" + this.state.get('type');

            if (this.state.get('closable')) {
                var closeButton = <a onClick={this.onClose}  className="close" ><span aria-hidden="true">&times;</span></a>
            }

            if (channel === itemchannel){
                return (<div className={classList}>
                    {this.state.get('text')}
                    {closeButton}
                </div>);
            }
        }
        return null;
    }

});

module.exports = Notify;
