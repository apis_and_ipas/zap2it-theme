var React = require('react');
var Modal = require('react-bootstrap/lib/Modal');
var ModalStore = require('../stores/ModalStore.js');
var ModalActions = require('../actions/ModalActions.js');
var storeMixin = require('../shared/mixins/storeMixin.js');

var UniversalModal = React.createClass({

    mixins: [storeMixin(ModalStore)],

    getInitialState: function() {
        return {
            modalStore: ModalStore
        };
    },
     
    renderModal: function() {
        var store = this.state.modalStore;
        var currentModal = store.get('currentModal');
        var modals = store.get('modals');
        var Component = modals[currentModal];

        return (
            <Modal onRequestHide={ModalActions.hide} className={Component.displayName + "-modal"} title={currentModal} animation={true}>
                { Component ? <Component/> : <div/>}
            </Modal>
        );
    },

    render: function() {
        return ( 
            this.state.modalStore.get('isOpen') ? this.renderModal() : null
        );
    }

});

module.exports = UniversalModal;