var React      = require('react');


var GridPreferencesModal = require('./GridPreferencesModal.jsx');
var ModalActions =  require('../../actions/ModalActions')

var GridPreferencesTrigger = React.createClass({

    handleClick: function() {
        ModalActions.show('Grid Preferences');
    },

    render: function() {
        return ( 
            <button className='grid-preferences-trigger btn btn-default' onClick={this.handleClick}>
                <i className="fa fa-cog"></i>
                <span>Grid Preferences</span>
            </button>
        );
    },

});

module.exports = GridPreferencesTrigger;
