var React  = require('react');
var Notify = require('../../components/Notify.jsx');
var PreferencesStore = require('../../stores/PreferencesStore');
var PreferencesActions = require('../../actions/PreferencesActions');
var storeMixin = require('../../shared/mixins/storeMixin');
var ModalActions = require('../../actions/ModalActions');
var ModalStore = require('../../stores/ModalStore');

var  GridPreferencesModal = React.createClass({
    
    mixins: [storeMixin(PreferencesStore)],

    getInitialState: function() {
        return PreferencesStore;
    },

    checkboxClick:function(key, event) {
        this.state.toggle(key);
    },

    radioClick: function(key, event) {
        this.state.set('timespan', key);
    },

    closeModal: function() {
        ModalActions.hide();
    },

    savePreferences: function(event) {
        ModalActions.hide();

        PreferencesActions.update({ 
            exclusions: this.state.getExclusionsJSON(),
            timespan:  this.state.get('timespan')
        });
    },

    render: function() {

        return (

             <div className="grid-preferences">
                <form className="form-horizontal">
                    <div className='grid-preferences__body modal-body'>

                        <Notify channel='modal'/>
                        
                        <div className="form-section">
                            <h4>Filters </h4>
                            
                            <div className="checkbox">
                                <label>
                                    <input type="checkbox" ref="musicCheckbox" onChange={this.checkboxClick.bind(this, 'music')} checked={this.state.isChecked('music')}/>
                                    Show music channels
                                </label>
                            </div>
                            
                            <div className="checkbox">
                                <label>
                                    <input type="checkbox" ref="ppvCheckbox"  onChange={this.checkboxClick.bind(this, 'ppv')} checked={this.state.isChecked('ppv')}/>
                                    Show Pay-Per-View channels
                                </label>
                            </div>
                            
                            <div className="checkbox">
                                <label>
                                    <input type="checkbox" ref="nonhdCheckbox" onChange={this.checkboxClick.bind(this, 'nonhd')} checked={this.state.isChecked('nonhd')}/>
                                    Only show HD channels
                                </label>
                            </div>

                        </div>

                             
                    </div>

                    <div className="modal-footer">
                        <div className="btn btn-default pull-left" onClick={this.closeModal}>Cancel</div>
                        <div className="btn btn-primary pull-right" onClick={this.savePreferences}>Save Preferences</div>
                    </div>
                
                </form>
            </div>
          
        );
    }

});

ModalStore.registerModal('Grid Preferences', GridPreferencesModal);

module.exports =  GridPreferencesModal;
