var React     = require('react');
var Bootstrap = require('react-bootstrap');
var TabPane = Bootstrap.TabPane;
var TabbedArea = Bootstrap.TabbedArea;

var classSet = require('react/lib/cx');
var UserActions = require('../../actions/UserActions');
var ModalActions = require('../../actions/ModalActions');
var Button = require('react-bootstrap/lib/Button');
var Alert = require('react-bootstrap/lib/Alert');
var Notify = require('./../Notify.jsx');
var _ = require('underscore');

var LoginForm = require('./LoginForm.jsx');
var SignUpForm = require('./SignupForm.jsx');

var AccountModal = React.createClass({

    getInitialState: function () {
        return {
            errMsg: null
        };
    },


    handleFBLogin: function() {

        UserActions.fbLogin();

    },

    showPasswordResetScreen: function() {

        ModalActions.show('Password Reset');

    },


    render: function() {

        var errorMsgOutput = this.state.errMsg ? <Alert bsStyle="danger">{this.state.errMsg}</Alert> : false;

        return (
            <div className='modal-body modal-body-wide'>
                <div className="container-fluid">
                    <div className="row">

                        <div className="col-md-6 col-md-push-6 after-or">
                            <div className="row">
                                <div className="col-md-8 col-md-push-2">
                                    <br/>
                                    <br/>
                                    <Alert bsStyle="info"> 
                                        All users, new and existing, will need to register with the new Zap2it grid to ensure all grid preferences remain in place.
                                    </Alert> 
                                </div>
                            </div>
                            
                            <div className="row">
                                <div className="col-md-8 col-md-push-2">
                                    <Button block className="fb-btn" onClick={this.handleFBLogin}> <i className="fa fa-facebook-official"></i>&nbsp;&nbsp; Log In with Facebook</Button>
                                </div>
                            </div>

                        </div>

                        <div className="col-md-6 col-md-pull-6">

                            <TabbedArea defaultActiveKey={1}>
                                <TabPane  eventKey={1} tab="Sign Up">
                                    <SignUpForm />
                                </TabPane>
                                <TabPane  eventKey={2} tab="Log In">
                                    <LoginForm />
                                </TabPane>
                            </TabbedArea>

                        </div>
                        
                        
                    </div>
                </div>
            </div>
        );
    }

});

module.exports = AccountModal;
