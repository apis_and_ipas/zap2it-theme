var React      = require('react');
var ResponsiveMixin = require('react-responsive-mixin');
var ModalActions = require('../../actions/ModalActions');

var UserModalTrigger = React.createClass({

    mixins: [ResponsiveMixin],

    getInitialState: function () {
        return {
            btnText: "Sign Up or Log In"
        };
    },

    componentDidMount: function () {
        
        this.media({maxWidth: 990}, function () {
            this.setState({
                btnText: ''
            });
        }.bind(this));

        this.media({minWidth: 991}, function () {
            this.setState({
                btnText: "Sign Up or Log In"
            });
        }.bind(this));
    },

    triggerModal: function() {
        ModalActions.show('Sign Up or Log In');
    },

    render: function() {
        return (
            <li>
                <button className='btn btn-primary navbar-btn' onClick={this.triggerModal}>
                    <i className='fa fa-user'></i>
                    <span>{this.state.btnText}</span>
                </button>
            </li>
        );
    }

});

module.exports = UserModalTrigger;
