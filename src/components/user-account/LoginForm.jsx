var React     = require('react');
var classSet = require('react/lib/cx');
var UserActions = require('../../actions/UserActions');
var ModalActions = require('../../actions/ModalActions');
var Button = require('react-bootstrap/lib/Button');
var Alert = require('react-bootstrap/lib/Alert');
var Notify = require('./../Notify.jsx');
var _ = require('underscore');
var isEmail = require('../../shared/helpers/isEmail');

var LoginForm = React.createClass({

    getInitialState: function () {
        return {
            errMsg: null
        };
    },

    handleLogin: function(event) {
        event.preventDefault();

        var email = this.refs.email.getDOMNode().value.trim();
        var password = this.refs.password.getDOMNode().value.trim();

        if ( _.isEmpty(email) || _.isEmpty(password) ) {

            this.setState({
                'errMsg': 'Both fields are required'
            })

        } else if ( !isEmail(email) ) {

            this.setState({
                'errMsg': 'Email must be a valid email address'
            });

        } else {

            this.setState({
                'errMsg': null
            });

            UserActions.login(email, password);
        }

    },

    handleFBLogin: function() {

        UserActions.fbLogin();

    },

    showPasswordResetScreen: function() {
         ModalActions.hide();
        
        setTimeout(function(){
             ModalActions.show('Password Reset');
        }, 100);
       
    },


    render: function() {

        var errorMsgOutput = this.state.errMsg ? <Alert bsStyle="danger">{this.state.errMsg}</Alert> : false;

        return (
            <div>

                <div className="row">
                    <div className="col-md-12">
                        <Notify channel='modal' renderHTML={true} goToLogIn={this.goToLogIn}/>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-8 col-md-push-2">
                        {errorMsgOutput}

                        <form id="userLoginForm">

                            <div className="form-group">
                                <input type="email" className="form-control input-lg" ref="email" placeholder="Email" />
                            </div>

                            <div className="form-group">
                                <input type="password" className="form-control input-lg" ref="password" placeholder="Password" />
                            </div>

                            <Button type="submit" className="btn btn-primary btn-lg" onClick={this.handleLogin} block>Log In</Button>

                        </form>

                        <div className="modal-footer-link">
                            <a href="#" onClick={this.showPasswordResetScreen}>Forgot your password?</a>
                        </div>
                    
                    </div>
                </div>

            </div>

        
        );
    }

});

module.exports = LoginForm;
