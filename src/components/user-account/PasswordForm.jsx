var React = require('react');
var classSet = require('react/lib/cx');
var Button = require('react-bootstrap/lib/Button');
var Alert = require('react-bootstrap/lib/Alert');
var UserActions = require('../../actions/UserActions');
var _ = require('underscore');
var isEmail = require('../../shared/helpers/isEmail');
var Notify = require('./../Notify.jsx');
var ModalActions = require('../../actions/ModalActions');
var PasswordForm = React.createClass({

    getInitialState: function () {
        return {
            errMsg: null
        };
    },

    handleSubmit: function(event) {
        event.preventDefault();

        var email = this.refs.email.getDOMNode().value.trim();

        if ( _.isEmpty(email) ) {

            this.setState({
                errMsg: "Email cannot be blank"
            });

        } else if ( !isEmail(email) ) {

            this.setState({
                errMsg: "Email must be a valid email address"
            });

        } else {

            this.setState({ errMsg: null });
            this.refs.form.getDOMNode().reset();

            UserActions.passwordReset(email);

        }
    },

    showLoginScreen: function() {
         ModalActions.show('Sign Up or Log In');
    },

    render: function() {

        var errorMsgOutput = this.state.errMsg ? <Alert bsStyle="danger">{this.state.errMsg}</Alert> : false;

        // conditional classes for password input
        var errorMsgClasses = classSet({
            'form-group': true,
            'form-group-lg': true,
            'has-error': !!this.state.errMsg
        });

        return (
            <div className='modal-body'>

                <div className="row">
                    <div className="col-md-12">
                        <Notify channel='modal' renderHTML={true} goToLogIn={this.goToLogIn}/>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-12">
                        {errorMsgOutput}
                    </div>

                    <div className="col-md-8 col-md-push-2">

                        <form id="passwordForm" ref="form">

                            <div className={errorMsgClasses}>
                                <input type="email" className="form-control input-lg" ref="email" placeholder="Email" />
                            </div>

                            <Button type="submit" className="btn btn-primary btn-lg" onClick={this.handleSubmit} block>Reset Password</Button>

                        </form>

                        <div className="modal-footer-link">Already have an account? <a href="#"  onClick={this.showLoginScreen}>Log In</a></div>
                    </div>
                </div>

            </div>

        );
    }

});

module.exports = PasswordForm;
