var React = require('react');
var Button = require('react-bootstrap/lib/Button');
var Alert = require('react-bootstrap/lib/Alert');
var UserActions = require('../../actions/UserActions');
var ModalActions = require('../../actions/ModalActions');
var _ = require('underscore');
var isEmail = require('../../shared/helpers/isEmail');
var Notify = require('./../Notify.jsx');

var SignupForm = React.createClass({

    getInitialState: function () {
        return {
            errMsg: null
        };
    },

    handleSignUp: function(event) {
        event.preventDefault();

        var email = this.refs.email.getDOMNode().value;
        var password = this.refs.password.getDOMNode().value;

        if ( _.isEmpty(email) ||  _.isEmpty(password) ) {

            this.setState({
                'errMsg': 'Both fields are required'
            });

        } else if ( !isEmail(email) ) {

            this.setState({
                'errMsg': 'Email must be a valid email address'
            });

        }  else {

            this.setState({
                'errMsg': null
            });

            UserActions.signup(email, password);
        }

    },

    handleFBSignUp: function() {

        UserActions.fbLogin();

    },



    render: function() {

        var errorMsgOutput = this.state.errMsg ? <Alert bsStyle="danger">{this.state.errMsg}</Alert> : false;

        return (
            <div>

                <div className="row">
                    <div className="col-md-12">
                        <Notify channel='modal' renderHTML={true} goToLogIn={this.showLoginScreen}/>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-8 col-md-push-2">
                        {errorMsgOutput}

                        <form id="userLoginForm">

                            <div className="form-group">
                                <input type="email" className="form-control input-lg" ref="email" placeholder="Email" />
                            </div>

                            <div className="form-group">
                                <input type="password" className="form-control input-lg" ref="password" placeholder="Password" />
                            </div>

                            <Button type="submit" className="btn btn-primary btn-lg" onClick={this.handleSignUp} block>Sign Up</Button>

                        </form>


                    </div>
                </div>

            </div>

        );
    }

});

module.exports = SignupForm;
