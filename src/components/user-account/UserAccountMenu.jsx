var React = require('react');
var storeMixin    = require('../../shared/mixins/storeMixin');
var UserActions = require('../../actions/UserActions');
var UserStore = require('../../stores/UserStore');
var UserModalTrigger = require('./UserModalTrigger.jsx');

var UserActionMenu = React.createClass({

    mixins: [storeMixin(UserStore)],

    componentWillMount: function(){
        UserActions.init();
    },

    getInitialState: function() {
        return UserStore;
    },

    handleSignOut: function() {
        UserActions.logout();
    },

    render: function() {

        // get current user, if it exists.
        var content;
       
        // if the user isnt authenticated...
        if (!this.state.get('isAuthenticated')) {
            // ..Render Login Button
            content = <UserModalTrigger/>;

        } else {
             var salutation = "Hello, " + (this.state.get('firstName') || this.state.get('username'));

            // render the User Account Dropdown
            content = (//dropdown dropdown-menu-right
                <li className="zap-dropdown dropdown-menu-right">
                    <a href="#" className="dropdown-toggle user-badge" data-toggle="dropdown" role="button" aria-expanded="false">

                        <span className="avatar">
                            <img src={ this.state.get('avatar') ||  "http://placehold.it/40x40"}/>
                        </span>
                
                        <span className="salutation">
                            {salutation}
                        </span>

                        <span className="caret"></span>
                    </a> 
                    <ul className="zap-dropdown-menu" role="menu">
                        <li><a href="/my-shows"><i className="fa fa-heart"></i> My Shows</a></li>
                        <li><a href="/account-settings"><i className="fa fa-gear"></i> Account Settings</a></li>
                        <li><a href="#" onClick={this.handleSignOut}><i className="fa fa-sign-out"></i> Sign Out</a></li>
                    </ul>
                </li>
            );
        }
        
        return (<ul className="nav navbar-nav navbar-right">{content}</ul>);
    }

});


module.exports = UserActionMenu;
