var Model = require('../shared/store').Model;
var SessionStore = require('../stores/SessionStore');
var store = new SessionStore('zap2it.gridData');
var log = require('../shared/helpers/logger');

var sync = function (data) {
    log('Page Model channels changed, syncing to SessionStore', data);
    store.set(this.get('name'), this.toJSON());
}

var Page = Model.extend({

    initialize: function (data) {
        this.set('name', data.name);
        this.set('channels', data.channels);
        this.on('change:channels', sync.bind(this))
    },

    incrementChunk: function(){
        // Stub
        // TODO: implement
    },

    hasMoreChunks: function () {
        // Stub
        // TODO: implement
    }

},
    // Class Methods
{
    create: function (data, cacheKey) {
        return new Page({
            name: cacheKey,
            channels: data.channels
            
        });
    }
});

module.exports = Page;
