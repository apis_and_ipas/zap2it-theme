/**
 * these timezones have a primetime that starts at 7 instead of 8.
 * this white list was created as a stand alone file as it is required more than once.
 * @type {Array}
 */
module.exports = [
    "America/Chicago",
    "America/Denver",
    "America/Anchorage",
    "Pacific/Honolulu"
];