function TimestampRounder() {}

TimestampRounder.prototype.increment = function(timestamp, increment) {
    return parseInt(timestamp, 10) + parseInt(increment, 10);
};

TimestampRounder.prototype.decrement = function(timestamp, increment) {
    return parseInt(timestamp, 10) - parseInt(increment, 10);
};

TimestampRounder.prototype.round = function(timestamp) {
    var now = Math.round(Date.now() / 1000), // now in seconds

        // if a timestamp was passed, parse it a date object
        date = timestamp ? new Date(timestamp * 1000) : new Date(now * 1000),
        minutes = date.getMinutes(),
        newDate;

    if (minutes > 30) {
        newDate = date.setMinutes(30, 0, 0);
    } else {
        newDate = date.setMinutes(0, 0, 0);
    }
    // convert back to seconds
    return newDate / 1000;
};

module.exports = new TimestampRounder();
