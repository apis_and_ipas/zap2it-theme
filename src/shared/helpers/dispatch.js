var Dispatcher = require('../dispatcher');
var log = require('./logger');
 
/**
 * a bit more standardized way to dispatch actions
 * @param {String} actionType
 * @param {Object} [load={}]
 * @returns {*}
 */
module.exports = function(actionType, load) {
    log(actionType, load);
    var load = load || {};  
    load.actionType = actionType;
    return Dispatcher.dispatch(load)
};
