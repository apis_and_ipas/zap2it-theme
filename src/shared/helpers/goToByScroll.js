var $ = require('jquery');

module.exports = function goToByScroll(el, offset) {

    var $el = $(el);
    var offset = offset || 110;
    
    if ($el) {
        var elTop =  $el.offset().top;

        $('html,body').animate({
            scrollTop: elTop - offset
        }, 1000);
    }
    
}