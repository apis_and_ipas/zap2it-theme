var _ = require('underscore');
var moment = require('moment-timezone');
var guessTimezone = require('./guessTimezone');
var getsEarlierPrimetime = require('./getsEarlierPrimetime');

 
var getPrimetime = function(timezone) {
    var timestamp = +new Date();

    timezone = timezone || guessTimezone.calc();

    var primetimeHour = (_.contains(getsEarlierPrimetime, timezone)) ? 19 : 20; 
    //.tz(timezone)
    var primetime = moment(timestamp).set({
        'hour': primetimeHour,
        'minutes': 0,
        'seconds': 0,
        'milliseconds': 0
    });

    return primetime.unix();
};
 

module.exports = getPrimetime;