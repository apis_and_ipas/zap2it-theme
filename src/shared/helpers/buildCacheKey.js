/**
 * builds cacheKey string from Grid Param properties
 * @param  {Object} params The GridParamsStore attributes
 * @return {String}        the cache key, duh
 */
module.exports = function (params) {
    return params.lineupId + ":" + params.time + ":" 
        + params.timespan + ":" + params.excludeChannels;
}
