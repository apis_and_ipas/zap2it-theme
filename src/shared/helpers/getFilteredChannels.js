var _ = require('underscore');
var FavoriteChannelsStore = require('../../stores/FavoriteChannelsStore');

module.exports = function getFilteredChannels(channels, filterName) {
 
    switch(filterName) {

        case 'movie':
        case 'sports':
        case 'family':
        case 'news':
        case 'talk':
            // return channels whos stationFilters array contains 'filter-' + filterName
            return _.filter(channels, function(channel) {
                return _.contains(channel.stationFilters, 'filter-' + filterName)
            });
            break;

        case 'starred':
            // filter channels whose channelId is in the favChannels array
            var starredChannels = FavoriteChannelsStore.get('favChannels');
            return _.filter(channels, function(channel) {
                return _.contains(starredChannels, channel.channelId);
            })
            break;

        case 'all': 
        default:
            return channels;
            break;
    }
}