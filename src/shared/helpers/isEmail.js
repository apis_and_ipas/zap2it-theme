/**
 * Test if value is an email address
 * @param  {String}  val  Text which may be an email address
 * @return {Boolean}     Is ths text and email address
 */
module.exports = function (val) {
    return /^(?:\w+\.?\+?)*\w+@(?:\w+\.)+\w+$/.test(val);
};
