var config = require('../../config');

var isDev = config.isDev || true;

// usage: log('inside coolFunc',this,arguments);
// http://paulirish.com/2009/log-a-lightweight-wrapper-for-consolelog/
module.exports = log = function(){
    if(this.console && isDev){
        console.log( Array.prototype.slice.call(arguments) );
    }
}
