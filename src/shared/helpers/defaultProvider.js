/**
 * given a timezone name, builds a default provider object
 * @param  {String} timezone
 * @return {Object}
 */
module.exports = function (timezone) {

    timezone = timezone || 'America/New_York';

    var provider = {};

    // default value, gets overridden as needed below
    provider.countryCode = 'USA';
    provider.isDefaultProvider = true;

    switch (timezone) {

        case 'America/Los_Angeles':
            provider.lineupId = "USA-DFLTP";
            provider.name = "Pacific"
            provider.postalCode = "90001"
            provider.timezone = "America/Los_Angeles";
            break;

        case "America/Denver":
            provider.lineupId = "USA-DFLTM";
            provider.name = "Mountain"
            provider.postalCode = "80001"
            provider.timezone = "America/Denver";
            break;

        case "America/Phoenix":
            provider.lineupId = "USA-DFLTM";
            provider.name = "Mountain"
            provider.postalCode = "85001"
            provider.timezone = "America/Phoenix";
            break;

        case 'America/Chicago':
            provider.lineupId = "USA-DFLTC";
            provider.name = "Central"
            provider.postalCode = "60001"
            provider.timezone = "America/Chicago";
            break;

        case 'America/Anchorage':
            provider.lineupId = "USA-DFLTA";
            provider.name = "Alaskan";
            provider.postalCode = "99501"
            provider.timezone = "America/Anchorage";
            break;

        case 'Pacific/Honolulu':
            provider.lineupId = "USA-DFLTH";
            provider.name = "Hawaiian";
            provider.postalCode = "96801"
            provider.timezone = "Pacific/Honolulu";
            break;

        case 'America/New_York':
        default:
            provider.lineupId = "USA-DFLTE";
            provider.name = "Eastern"
            provider.postalCode = "10001"
            provider.timezone = "America/New_York";
            break;
    }

    // concat discription from Name
    provider.name += " Default Lineup" ;

    return provider;
};
