var SessionStore = require('../stores/SessionStore');
var GridApi = require('../shared/api/grid');
var LineupsApi = require('../shared/api/lineups');
var Deferred = require('backbone').$.Deferred;
var Model = require('../shared/store').Model;
var Page = require('../shared/pageModel');
var log = require('../shared/helpers/logger');
var buildCacheKey = require('../shared/helpers/buildCacheKey');
var store = new SessionStore('zap2it.gridData');


/**
 * DataManager checks SessionStorage for lineup data before trying the remote API
 * It also normalizes gridData coming from the API or the SessionStorage
 */
var DataManager = {

    /**
     * @param  {object} params Params object from GridParamsStore
     * @return {Promise}       Resolves with an Page instance
     */
    fetch: function (params) {
        var deferred = new Deferred();
        var pageData;

        // build cacheKey via helper
        var cacheKey = buildCacheKey(params),
            cachedData = store.get(cacheKey);

        // if data was fetched from cache resolve,
        if (cachedData) {

            pageData = Page.create(cachedData, cacheKey);
            // log('Page.fromCache ', pageData.toJSON());

            deferred.resolve(pageData);

        } else { // other try the remote API and then cache the results

            GridApi.query(params).then(function(response) {

                pageData = Page.create(response, cacheKey);
                // log('Page.fromAPI', pageData.toJSON());

                // cache results and resolve with pageData
                store.set(cacheKey, pageData.toJSON());
                deferred.resolve(pageData);

            }).fail(function (jqXHR, textStatus, errorThrown) {
                deferred.reject(arguments);
            });
        }

        return deferred.promise();

    },

   
    fetchChannelList: function(params) {
        var deferred = new Deferred();
        var cacheKey = 'CL-' + params.lineupId;
        var cachedData = store.get(cacheKey);

        if (cachedData) {
            log('lineups channels list from CACHE', cachedData);
           // resolve cached data
           deferred.resolve(cachedData);
        } else {
            delete params.actionType;
            LineupsApi.channels(params).then(function(response) {
                log('lineups channels list from API', response);
                store.set(cacheKey, response);
                deferred.resolve(response);
            }).fail(function (jqXHR, textStatus, errorThrown) {
                deferred.reject(arguments);
            });
            //  // fetch from API
        }

        return deferred.promise();
    }
};

module.exports = window.dataManager =  DataManager;
