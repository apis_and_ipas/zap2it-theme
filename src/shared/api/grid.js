var Backbone = require('backbone');
var $ = Backbone.$;
var config = require('../../config');
var root = config.api_url + '/api';

module.exports = {

    query: function(query) {
        var url = root + '/grid';
        return $.getJSON(url, query);
    },

};
