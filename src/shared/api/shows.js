var $ = require('jquery');
var config = require('../../config');
var root = config.api_url + '/api'; 
var getPrimetime = require('../helpers/getPrimetime');
var _ = require('underscore');
var ProviderStore = require('../../stores/ProviderStore');

module.exports = {

    newTonight: function(params) {
        params = params || {};
        var timezone = ProviderStore.get('timezone');
        params.startTime = getPrimetime(timezone);
 
        var url = root + '/shows/newTonight';
        return $.getJSON(url, params);
    },

    details: function(showId) {
        var url = root + '/shows/' + showId;
        return $.getJSON(url);
    },

    seriesAirings: function(params) {
        var seriesId = params.seriesId,
            params = {  
                lineupId: params.lineupId  
            };

        var url = '/seriesAirings/' + seriesId;
        return $.getJSON(url, params);
    },

    myShowsAirings: function(params) {

        var seriesIds = params.seriesIds,
            requests = [];

        //clean up params obj
        delete params.seriesIds;

        //map our series ids to an array of $.Deferreds;
        seriesIds.forEach(function(seriesId){
            var url = root + '/seriesAirings/' + seriesId;

            requests.push($.getJSON(url, params));
        });

        // when our `n` deferreds resolve, send back the first argument (the ajax response)
        return $.when.apply($, requests).then(function() {
            var args = Array.prototype.slice.call(arguments)

            // returns array for each return result set, so just grab first member
            var unsorted =  args.map(function(arg){return arg[0];});

            //removes shows without airings
            var withoutFalsy  = _.compact(unsorted);

            // sort shows by title.
            var sorted  = _.sortBy(withoutFalsy, function(show){
                return show.title;
            });

            return sorted;
        });

    }

};
