var Backbone = require('backbone');
var $ = Backbone.$;
var config = require('../../config');
var root = config.api_url + '/api';

module.exports = {

    find: function(payload) {
        var url = root + '/lineups/find';
        return $.getJSON(url, payload);
    },

    channels: function(payload) {
        var url = root + '/lineups/channels';
        return $.getJSON(url, payload);
    }

};
