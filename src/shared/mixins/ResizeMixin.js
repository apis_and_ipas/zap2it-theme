'use strict';

var $ = jQuery = require('jquery');
require('../helpers/jquery.resize');


var ResizeMixin = {

    componentDidMount: function() {
        $(this.getDOMNode()).resize(this._onResize);
    },

    componentWillUnmount: function() {
        $(this.getDOMNode()).removeResize(this._onResize);
    },

    _onResize: function(event) {   
        if (this.onResize && typeof this.onResize === 'function') {
            this.onResize(event);
        }
    }

};

module.exports = ResizeMixin;