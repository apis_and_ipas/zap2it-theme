'use strict';

var $ = jQuery = require('jquery');
require('../helpers/jquery.attrChange');

var AttributeChangeMixin = {

    getDefaultProps: function() {
        return {
            watchAttr: 'height',
            trackValues: true,
            className: '.GridWrapper'
        }
    },

    /**
     * mount the jquery plugin to the component and wire up the listener
     */
    componentDidMount: function() {
        $('.grid-results > div').attrchange({
            trackValues: this.props.trackValues,
            callback: this._onAttrChange
        });
    },

    /**
     * clean up event listeners
     */
    componentWillUnmount: function() {
        $(this.getDOMNode()).attrchange('remove');
    },

    _onAttrChange: function(event){
        // if the attribute which changed is the
        // specific one we're listening to and..
        if (event.attributeName === this.props.watchAttr
            // call the components own handler function with the event object,
            // if it exists and is a function
            && this.onAttrChange
            && typeof this.onAttrChange === 'function') {
                this.onAttrChange(event);
        }
    }

};


module.exports = AttributeChangeMixin;