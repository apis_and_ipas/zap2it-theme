'use strict';

var ViewportMetrics = require('react/lib/ViewportMetrics');

var ScrollListenerMixin = {

    getDefaultProps: function() {
        return {
            endScrollTimeout: 10
        };
    },
    
    componentDidMount: function() {
        window.addEventListener('scroll', this._onPageScroll);
    },

    componentWillUnmount: function() {
        window.removeEventListener('scroll', this._onPageScroll);
    },

    _onPageScrollEnd: function() {
        var scrollTop = ViewportMetrics.currentScrollTop;

        if (scrollTop === this.state.get('scrollTop')) {
            window.clearTimeout(this._pageScrollTimeout);
            this.state.set({ isScrolling: false });

            if (typeof this.onPageScrollEnd === 'function') {
                this.onPageScrollEnd(scrollTop);
            }
        }
    },

    _onPageScroll: function() {
        var newScrollTop = ViewportMetrics.currentScrollTop;

        // get a reference to the last value before changing it, 
        var oldScrollTop = this.state.get('scrollTop');

        this.state.set({
            scrollTop: newScrollTop,
            isScrolling: true
        });

        window.clearTimeout(this._pageScrollTimeout);
        this._pageScrollTimeout = window.setTimeout(
            this._onPageScrollEnd, 
            this.props.endScrollTimeout
        );

        // if component has member function 'onPageScroll',
        // call it with the current offset;
        if (typeof this.onPageScroll === 'function') {
            this.onPageScroll(newScrollTop, oldScrollTop);
        }
    }

};

module.exports = ScrollListenerMixin;