var Backbone = require('backbone');
var Dispatcher = require('./dispatcher');

var BaseStore = {

    /**
     * backbone init method
     */
    initialize: function() {
        this.dispatchId = Dispatcher.register(this.handleDispatch.bind(this));
    },

    /**
     * handle the dispatcher actions
     * @param {Object} payload
     */
    handleDispatch: function(payload) {}
};

module.exports = {
    Model: Backbone.Model.extend(BaseStore),
    Collection: Backbone.Collection.extend(BaseStore)
};
