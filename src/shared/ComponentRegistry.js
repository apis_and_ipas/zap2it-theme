ComponentRegistry = (function() {

    var _components = {};

    var register = function (name, component) {
        if (!_components[name]) {
            _components[name] = component;
        }
    };

    var fetch = function (name) {
        if (!_components[name]) return;

        return _components[name];
    };

    return {
        register: register,
        fetch: fetch
    }

})();


module.exports = ComponentRegistry;
