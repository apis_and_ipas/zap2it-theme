var constants = require('../constants');
var dispatch = require('../shared/helpers/dispatch');
var Parse = require('parse').Parse;

module.exports = {
    update: function(payload) {
        dispatch(constants.USER_UPDATE_PREFS, payload);

        delete payload.actionType;

        var user = Parse.User.current();

        // we may be a guest user.. lets fail silently, 
        // as the Grid will query & update if needed
        if (!user) return;

        user.save(payload)
            .then(function (response) {
                dispatch(constants.USER_UPDATE_PREFS_SUCCESS, { response: response });
            })
            .fail(function (error) {
                dispatch(constants.USER_UPDATE_PREFS_FAIL, { error: error });
            });

    }
};