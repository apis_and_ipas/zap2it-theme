var constants = require('../constants');
var dispatch = require('../shared/helpers/dispatch');

var ModalActions = {
    show: function(modalName) {
        dispatch(constants.MODAL_SHOW, {
            modalName: modalName
        });
    },

    hide: function() {
        dispatch(constants.MODAL_HIDE);
    }
};

module.exports = window.modalActions = ModalActions;