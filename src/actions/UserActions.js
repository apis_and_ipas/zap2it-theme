var constants = require('../constants');
var dispatch = require('../shared/helpers/dispatch');
var Parse = require('parse').Parse;
var DataManager = require('../shared/dataManager');
var log = require('../shared/helpers/logger');


module.exports = {

    /**
     * Restores a user session from from cookie
     * and set user in UserStore
     */
    init: function() {
        // grab user from session cookie
        var currentUser = Parse.User.current();

        if (currentUser && currentUser.authenticated()) {
            dispatch(constants.USER_INIT, {
                user: currentUser
            });
        }
    },

    /**
     * Invalidates session cookie and resets
     * UserStore data
     * @return {[type]} [description]
     */
    logout: function() {
        Parse.User.logOut();
        dispatch(constants.USER_LOGOUT);
    },

    login: function(username, password) {
        dispatch(constants.USER_LOGIN);

        Parse.User.logIn(username, password, {
            success: function(user) {
                dispatch(constants.USER_LOGIN_SUCCESS, {
                    user: user
                });
            },
            error: function(user, error) {
                dispatch(constants.USER_LOGIN_FAIL, {
                    user: user,
                    error: error
                });
            }
        });

    },

    fbLogin: function() {
        dispatch(constants.USER_LOGIN);

        Parse.FacebookUtils.logIn("public_profile,email", {
            success: function(user) {

                // we should now have a signed in user.
                var currentUser = Parse.User.current();

                FB.api('/me', function(response) {

                    currentUser.set('username', response.email.split('@')[0]);
                    currentUser.set('email', response.email);
                    currentUser.set('firstName', response.first_name);
                    currentUser.set('avatar', 'http://graph.facebook.com/' + response.id + '/picture?type=small');
                    currentUser.save(null, {
                        success: function(user) {
                            dispatch(constants.USER_LOGIN_SUCCESS, {
                                user: user
                            });
                        }
                    });

                });

            },
            error: function(user, error) {
                dispatch(constants.USER_LOGIN_FAIL, {
                    user: user,
                    error: error
                });
            }
        });
    },



    signup: function(email, password) {
        dispatch(constants.USER_SIGNUP);

        var user = new Parse.User();

        var displayName = email.split('@')[0];
        user.set("firstName", displayName);

        // allows for login with username or email / both are email
        user.set("username", email);
        user.set("password", password);
        user.set("email", email);

        user.signUp(null, {

            success: function(user) {
                var payload = {
                    user: user
                };

                dispatch(constants.USER_SIGNUP_SUCCESS, payload);
                dispatch(constants.USER_LOGIN_SUCCESS, payload);
            },

            error: function(user, error) {

                module.exports.login(email, password);

            }

        });
    },

    passwordReset: function(email) {
        dispatch(constants.USER_PASSWORD_RESET);

        Parse.User.requestPasswordReset(email, {
            success: function() {
                dispatch(constants.USER_PASSWORD_RESET_SUCCESS);
            },
            error: function(error) {
                dispatch(constants.USER_PASSWORD_RESET_FAIL, {
                    error: error
                });
            }
        });
    },


    fetchFavoriteChannels: function () {
        var user = Parse.User.current();
        var query = new Parse.Query('FavoriteChannels');
        query.equalTo('user', user)
        query.find().then(function(results) {
            dispatch(constants.USER_FETCH_FAV_CHANNELS, { favChannels: results });
        });

    },

    addToFavChannels: function (channelId) {
        var user = Parse.User.current();

        dispatch(constants.USER_FAV_CHANNEL, { channelId: channelId, user: user  });

        var FavoriteChannels = Parse.Object.extend("FavoriteChannels");
        var favChannels = new FavoriteChannels();
        favChannels.setACL(new Parse.ACL(user));// set security on object

        favChannels.save({ channelId: channelId, user: user  })
            .then(log)
            .fail(log);
    },

    removeFromFavChannels: function (channelId) {
        var user = Parse.User.current();

        dispatch(constants.USER_UNFAV_CHANNEL, { channelId: channelId, user: user });

        var FavoriteChannels = Parse.Object.extend("FavoriteChannels");
        var query = new Parse.Query(FavoriteChannels);

        // query for FavoriteChannels belonging to the current user with the matching show ID.
        query.equalTo('user', user);
        query.equalTo('channelId', channelId);

        query.first().then(function (results) {
            // destroy it!
            results.destroy()
                .then(log)
                .fail(log);
        })
        .fail(log);
    },

    updateProfile: function (payload) {
        // payload attrs vary depending on which form fields are completed
        dispatch(constants.USER_UPDATE_PROFILE, payload);

        delete payload.actionType;

        var user = Parse.User.current();

        user.save(payload)
            .then(function (response) {
                dispatch(constants.USER_UPDATE_PROFILE_SUCCESS, { response: response });
            })
            .fail(function (error) {
                dispatch(constants.USER_UPDATE_PROFILE_FAIL, { error: error });
            });
    },

    updateProvider: function (payload) {

        dispatch(constants.USER_UPDATE_PROVIDER, payload);

        delete payload.actionType;

        var user = Parse.User.current();

        if (user) {
            user.save({provider: payload})
                .then(function (response) {
                    dispatch(constants.USER_UPDATE_PROVIDER_SUCCESS, { response: response });
                })
                .fail(function (error) {
                    dispatch(constants.USER_UPDATE_PROVIDER_FAIL, { error: error });
                });
        }


    },

};
