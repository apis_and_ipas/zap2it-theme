var constants = require('../constants');
var dispatch = require('../shared/helpers/dispatch');
var ShowsApi = require('../shared/api/shows');
var getPrimetime = require('../shared/helpers/getPrimetime');

var NewTonightActions = {

    init: function(params) {

        dispatch(constants.NEW_TONIGHT_INIT, getPrimetime());
        
        params && params.actionName && delete params.actionName;

        ShowsApi.newTonight(params).then(function(response) {

            dispatch(constants.NEW_TONIGHT_INIT_SUCCESS, {
                shows: response
            });
        
        }).fail(function (jqXHR, textStatus, errorThrown) {
            
            dispatch(constants.NEW_TONIGHT_INIT_FAIL, {
                jqXHR: jqXHR,
                textStatus: textStatus,
                errorThrown: errorThrown
            });
        });
    },

    toggleFilter: function(filter) {
        dispatch(constants.NEW_TONIGHT_FILTER, {
            filter: filter
        });
    }

};

 module.exports =  NewTonightActions;