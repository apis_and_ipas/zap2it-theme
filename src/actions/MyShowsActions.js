var Parse = require('parse').Parse;
var constants = require('../constants');
var dispatch = require('../shared/helpers/dispatch');
var ShowsAPI= require('../shared/api/shows');
var _ = require('underscore');


// helper to remove falsey object values.
_.mixin({
    compactObject : function(o) {
        var clone = _.clone(o);
        _.each(clone, function(v, k) {
            // remove any false keys, even empty arrays
            if ( !v || ( _.isArray(v) && _.isEmpty(v) ) ) {
                 delete clone[k];
            }  
        });
        return clone;
    }
});

var MyShowsActions = {

    init: function(shows) {
        dispatch(constants.MY_SHOWS_INIT, {
            shows: shows
        });
    },

    addToMyShows: function (seriesId) {
        var user = Parse.User.current();

        dispatch(constants.MY_SHOWS_ADD, { seriesId: seriesId, user: user });

        var MyShows = Parse.Object.extend("MyShows");
        var myShows = new MyShows();
        myShows.setACL(new Parse.ACL(user));// set security on object

        myShows.save({ seriesId: seriesId, user: user })
            .then(log)
            .fail(log);
    },

    removeFromMyShows: function (seriesId) {
        var user = Parse.User.current();

        dispatch(constants.MY_SHOWS_REMOVE, { seriesId: seriesId, user: user });

        var MyShows = Parse.Object.extend("MyShows");
        var query = new Parse.Query(MyShows);

        // query for MyShows belonging to the current user with the matching show ID.
        query.equalTo('user', user);
        query.equalTo('seriesId', seriesId);

        query.first().then(function (results) {
            // destroy it!
            results && results.destroy()
                .then(log)
                .fail(log);
        })
        .fail(log);
    },


    fetchMyShowsIds: function () {
        var user = Parse.User.current();
        var query = new Parse.Query('MyShows');

        query.equalTo('user', user)

        // dispatch(constants.MY_SHOWS_FETCH_IDS);

        query.find().then(function(results) {

            dispatch(constants.MY_SHOWS_FETCH_IDS_SUCCESS, { myShowsIds: results });

        }).fail(function() {

            dispatch(constants.MY_SHOWS_FETCH_IDS_FAIL, { error: arguments });
        });

    },

    fetchMyShowsAirings: function(seriesIds, lineupId) {

        var payload = { 
            seriesIds: seriesIds, 
            lineupId: lineupId 
        };

        ShowsAPI.myShowsAirings(payload)
             .then(function(response) {

            // remove falsey members from response 
            // ie. shows with no returned listings in the next 24 hours.
            response = _.compactObject(response);

            //avoid dispatch collision
            setTimeout(function(){
                dispatch(constants.MY_SHOWS_SHOW_DATA_FETCH_SUCCESS, {
                    shows: response
                });
                
            }, 1)
            
        });
    },

    toggleNewShows: function(){
        dispatch(constants.MY_SHOWS_TOGGLE_NEW_LISTINGS);
    }

};



module.exports = MyShowsActions;