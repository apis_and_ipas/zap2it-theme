var constants = require('../constants');
var dispatch = require('../shared/helpers/dispatch');
var DataManager = require('../shared/dataManager');
var buildCacheKey = require('../shared/helpers/buildCacheKey')

 
var GridActions = {

    query: function(args) {
        dispatch(constants.GRID_QUERY, {
            params: args
        });
        args.chunk = 1;
        DataManager.fetch(args).then(function(page) {
                page.set('cache_slug', buildCacheKey(args));
                dispatch(constants.GRID_QUERY_SUCCESS, {
                    data: page
                });
            })
            .fail(function(args) {
                dispatch(constants.GRID_QUERY_FAIL, {
                    err: args
                });
            });

    },

    pagePrev: function() {
        dispatch(constants.GRID_PAGE_CHANGE, { method: 'decrement' });
    },

    pageNext: function() {
        dispatch(constants.GRID_PAGE_CHANGE, { method: 'increment' });
    },

    toggleDetail: function (channel, show) {
        dispatch(constants.GRID_TOGGLE_DETAIL, { channel: channel, show: show });
    },

    timeJump: function (time) {
        dispatch(constants.GRID_TIME_JUMP, { time: time });
    },

    setFilter: function(filterName) {
        dispatch(constants.GRID_SET_FILTER, { filterName: filterName });
    },

    setTimespan: function(timespan) {
        dispatch(constants.GRID_SET_TIMESPAN, { timespan: timespan });
    },

    setStickyState: function(stickyState){
        dispatch(constants.SET_STICKY_STATE, {stickyState: stickyState});
    },

    fetchChannelList: function(lineupId) {

        var params = {
            lineupId: lineupId
        };

        dispatch(constants.FETCH_CHANNEL_LIST, params);

        DataManager.fetchChannelList(params).then(function(channels) {
                dispatch(constants.FETCH_CHANNEL_LIST_SUCCESS, {
                    channels: channels
                });
            })
            .fail(function(err) {
                dispatch(constants.FETCH_CHANNEL_LIST_FAIL, {
                    err: err
                });
            });
        
    } 

};

module.exports = window.gridActions = GridActions;
