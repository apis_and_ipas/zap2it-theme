var constants = require('../constants');
var dispatch = require('../shared/helpers/dispatch');
var lineupsAPI = require('../shared/api/lineups');


module.exports = {

    setProvider: function(provider, query) {
        dispatch(constants.SET_PROVIDER, {
            provider: provider,
            query: query
        });
    },

    findProviders: function(countryCode, postalCode) {

        var query = {
            countryCode: countryCode,
            postalCode: postalCode
        };

        dispatch(constants.FIND_PROVIDERS, query);
 
        lineupsAPI.find(query).then(function(data) {

             dispatch(constants.FIND_PROVIDERS_SUCCESS, {
                data: data,
                query: query
            });

        }).fail(function(error) {
            dispatch(constants.FIND_PROVIDERS_FAIL, {
                error: error
            });
        });
    }

};
