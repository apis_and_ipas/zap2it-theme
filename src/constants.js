var makeConstantsMap = require('./shared/helpers/makeConstantsMap');

module.exports = makeConstantsMap([


    'USER_INIT',

    'USER_UPDATE_PROFILE',
    'USER_UPDATE_PROFILE_SUCCESS',
    'USER_UPDATE_PROFILE_FAIL',

    'USER_UPDATE_PROVIDER',
    'USER_UPDATE_PROVIDER_SUCCESS',
    'USER_UPDATE_PROVIDER_FAIL',

    'USER_PASSWORD_RESET',
    'USER_PASSWORD_RESET_FAIL',
    'USER_PASSWORD_RESET_SUCCESS',

    // User Login..
    'USER_LOGOUT',
    'USER_LOGIN',
    'USER_LOGIN_SUCCESS',
    'USER_LOGIN_FAIL',

    // Grid Preferences
    'USER_UPDATE_PREFS',
    'USER_UPDATE_PREFS_SUCCESS',
    'USER_UPDATE_PREFS_FAIL',

    //User signup
    'USER_SIGNUP',
    'USER_SIGNUP_SUCCESS',
    'USER_SIGNUP_FAIL',

    // Favorite channels
    'USER_FAV_CHANNEL',
    'USER_UNFAV_CHANNEL',
    'USER_FETCH_FAV_CHANNELS',

    // Provider Search
    'SET_PROVIDER',
    'FIND_PROVIDERS',
    'FIND_PROVIDERS_SUCCESS',
    'FIND_PROVIDERS_FAIL',

    // Flash Alerts
    'NOTIFY_HIDE',
    'NOTIFY_SHOW',
    'NOTIFY_ERROR',

    // Tv Listings Grid
    'GRID_INIT',
    'GRID_TIME_JUMP',
    'GRID_QUERY',
    'GRID_QUERY_SUCCESS',
    'GRID_QUERY_FAIL',
    'GRID_QUERY_REMOTE',
    'GRID_QUERY_REMOTE_SUCCESS',
    'GRID_QUERY_REMOTE_FAIL',

    // Grid Pagination / Nagivation
    'GRID_NEXT_CHUNK',
    'GRID_NEXT_CHUNK_SUCCESS',
    'GRID_NEXT_CHUNK_FAIL',
    'GRID_PAGE_CHANGE',

    // Find channels
    'FETCH_CHANNEL_LIST',
    'FETCH_CHANNEL_LIST_SUCCESS',
    'FETCH_CHANNEL_LIST_FAIL',

    // Grid Settings
    'GRID_SET_TIMEZONE',
    'GRID_TOGGLE_DETAIL',

    // Color-coded channel filters
    'GRID_SET_FILTER',

    // responsive adjustment
    'GRID_SET_TIMESPAN',

    // JumpToFilters
    'FILTER_SET_DATE',
    'FILTER_SET_TIME',

    // New Tonight Actions
    'NEW_TONIGHT_INIT',
    'NEW_TONIGHT_INIT_SUCCESS',
    'NEW_TONIGHT_INIT_FAIL',
    'NEW_TONIGHT_FILTER',

    'MY_SHOWS_INIT',

    // Heart Shows
    'MY_SHOWS_FETCH_IDS',
    'MY_SHOWS_FETCH_IDS_SUCCESS',
    'MY_SHOWS_FETCH_IDS_FAIL',
    'MY_SHOWS_ADD', 
    'MY_SHOWS_REMOVE',
    'MY_SHOWS_SHOW_DATA_FETCH',
    'MY_SHOWS_SHOW_DATA_FETCH_SUCCESS',
    'MY_SHOWS_SHOW_DATA_FETCH_FAIL',
    'MY_SHOWS_TOGGLE_NEW_LISTINGS',

    'MODAL_HIDE',
    'MODAL_SHOW',

    'SET_STICKY_STATE'
]); 
