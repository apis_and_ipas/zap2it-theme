'use strict';

var React = window.React = require('react');
var $ = require('jquery');
var Parse = require('parse').Parse;
var config = require('./config');
var ComponentRegistry = require('./shared/ComponentRegistry');
var UserActions = require('./actions/UserActions');
var ModalActions = require('./actions/ModalActions');
var ModalStore = require('./stores/ModalStore');
var zapwp = require('./wordpress');

Parse.initialize(config.parse_app_id, config.parse_js_key);

/**
 *  You can auto-mount React components by registering them in the ComponentRegistry
 *  then adding a data attribute like so:
 *
 *  <div data-react-mount="UserAccountMenu"></div>
 *
 *  The registered component would then be mounted in the DOM element
 */
ComponentRegistry.register('Grid', require('./components/grid/Grid.jsx'));
ComponentRegistry.register('NewTonight', require('./components/new-tonight/NewTonight.jsx'));
ComponentRegistry.register('MyShows', require('./components/my-shows/MyShows.jsx'));
ComponentRegistry.register('UserAccountMenu', require('./components/user-account/UserAccountMenu.jsx'));
ComponentRegistry.register('ShowMovieSearch', require('./components/ShowMovieSearch.jsx'));
ComponentRegistry.register('PreferencesBar', require('./components/PreferencesBar.jsx'));
ComponentRegistry.register('Notify', require('./components/Notify.jsx'));
ComponentRegistry.register('AccountSettings', require('./components/account-settings/AccountSettings.jsx'));
ComponentRegistry.register( "ZapGallery", require( './components/gallery/Gallery.jsx' ) );
ComponentRegistry.register( "UniversalModal", require( './components/UniversalModal.jsx' ) );


ModalStore.registerModal('Sign Up or Log In', require('./components/user-account/AccountModal.jsx'));
ModalStore.registerModal('Password Reset', require('./components/user-account/PasswordForm.jsx'));


$(function () {

    /**
     * Auto-mount components via `data-react-mount`
     */
    $('[data-react-mount]').each(function (index, elem) {
        var className, Component, classPropsJSON, classProps;

        className = elem.getAttribute('data-react-mount')
        Component = ComponentRegistry.fetch(className);
        if (!Component) return;

        // parse component props from json in data-react-props attr
        classPropsJSON = elem.getAttribute('data-react-props')
        if (classPropsJSON) {
            classProps = JSON.parse(classPropsJSON);
        }

        React.render( <Component {...classProps}/>, elem);
    });

    $('body').on('click', '.tv-listings-nav li:nth-of-type(3) a', function(event){
        var currentUser = Parse.User.current();
        if (!currentUser) {
            event.preventDefault();
            ModalActions.show('Sign Up or Log In');
        } 
    });
    
});


// Setup for FB SDK / User integration
window.fbAsyncInit = function() {

    Parse.FacebookUtils.init({
        appId: config.facebook_api_id,
        cookie: true,
        xfbml: true,
        version: 'v2.3'
    });
};

(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
// end of setup for FB SDK / User integration


$(function () {
    // center twitter embeds
    var tweets = $('.twitter-tweet');
    tweets.addClass('tw-align-center');
});








