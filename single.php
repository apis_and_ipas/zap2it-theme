<?php
/**
 * The template for displaying all single posts.
 *
 * @package Tribune Media Zap2it
 */

get_header(); ?>


	<?php while ( have_posts() ) : the_post(); ?>

	<header class="entry-header content-area full-width">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

		<div class="entry-meta">
			<?php zap_posted_on(); ?>
		</div><!-- .entry-meta -->

		<div class="entry-social-media">
			<?php zap_social_media_btns(); ?>
		</div><!-- .entry-meta -->

	</header><!-- .entry-header -->

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php get_template_part( 'template-parts/content', 'single' ); ?>

		</main><!-- #main -->
	</div><!-- #primary -->

	<?php endwhile; // end of the loop. ?>

<?php get_sidebar(); ?>
<?php get_template_part( 'template-parts/chunk-related-stories' ); ?>
<?php get_footer();
