				<div class="article-item col-sm-4">
					<?php zap_print_extra_for_module() ?>
					<a href="<?php the_permalink() ?>">
						<?php the_post_thumbnail( 'thumb-360-202' ); ?>
						<div>
							<?php the_title() ?>
						</div>
					</a>
				</div>