<div class="ad-bar">
    <div class="visible-xs-block">
    <?php do_action( 'acm_tag', 'oop' ); ?>
    <?php if (jetpack_is_mobile() ) do_action( 'acm_tag', '320x50' ) ?></div>
    <?php if ( ! jetpack_is_mobile() ): ?>
    	<div class="hidden-xs"><?php do_action( 'acm_tag', '728x90-atf' ) ?></div>
  <?php endif ?>
</div>
