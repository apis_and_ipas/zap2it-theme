	<aside id="recent-posts-2" class="widget widget_recent_entries">
		<h1 class="widget-title">Popular</h1>
		<ul data-tb-region="Popular">

		<?php
		// Grab top 4 posts for the last week
		$top_posts_ids = wp_list_pluck( wpcom_vip_top_posts_array( 3, 4 ), 'post_id' );
		$query = new WP_Query( array( 'post__in' => $top_posts_ids, 'posts_per_page' => 4, 'is_popular' => true ) );
		?>
			<?php if ( $query->have_posts() ) : ?>
				<?php while ( $query->have_posts() ) : $query->the_post(); ?>
					<?php get_template_part( 'template-parts/content-zone-hero-li' ); ?>
				<?php endwhile; ?>

			<?php else : ?>

				<li><small>No popular posts</small></li>

			<?php endif; ?>
		</ul>
		<div class="ad text-center padding-bottom">
			<?php do_action( 'acm_tag', '300x250-btf' ); ?>
		</div>
	</aside>