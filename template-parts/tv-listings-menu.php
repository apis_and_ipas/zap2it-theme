    <div class="row">
        <div class="col-md-12">
            <div id="notify-bar" data-react-mount="Notify" ></div>
        </div>
    </div>


    <div id="preferences-bar" data-react-mount="PreferencesBar"></div>

<?php
 wp_nav_menu( array(
    'menu'       => 'TV Listings Tabs',
    'menu_class' => 'nav nav-tabs tv-listings-nav',
    'walker'     => new wp_bootstrap_navwalker()
 ) );
