<?php
/**
 * The template part for displaying results in search pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Tribune Media Zap2it
 */
?>

<?php if ( get_post_type() == 'show-details' ):
	get_template_part( 'template-parts/chunk', 'show-card' );
else:
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'search-result' ); ?>>
	<header class="entry-header">
		<?php the_title( sprintf( '<h3 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' ); ?>

	</header><!-- .entry-header -->

	<div class="entry-summary">
		<div class="row">
		<div class="search-image-wrapper pull-left">
			<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'thumb-265-150' ); ?></a>
		</div>

		<?php the_excerpt(); ?>

		<?php if ( 'show-details' == get_post_type() ) {
			echo esc_html( zap_get_post_meta( 'release_year' ) );
			the_terms( get_the_ID(), 'genre', '&nbsp;&bull;&nbsp;' );
		}
		?>


		</div>
	</div><!-- .entry-summary -->
	<footer class="entry-footer">
	<?php if ( 'post' == get_post_type() ) : ?>
	<?php zap_entry_footer(); ?>
	<div class="entry-meta">
		<?php zap_posted_on(); ?>
	</div><!-- .entry-meta -->
	<?php endif; ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
<?php endif ?>