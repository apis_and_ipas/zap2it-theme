<?php
/**
 * @package Tribune Media Zap2it
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'zap' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->


	<footer class="entry-footer">
		<hr>
		<?php zap_entry_footer(); ?>
		<hr>
		<div class="related-items">
		<?php
		// TODO: reactify
		/*
			<div class="related-item" style="background: url(http://lorempixel.com/900/200/people);">

			  	<div class="related-item__img col-md-3">
			    	<a href="#">
			      		<img src="http://placehold.it/165x120/" class="responsive-img">
			    	</a>
			  	</div>

			  	<div class="related-item__content col-md-9">

					<div class="related-item__header">
						<h4 class="related-item__title pull-left">
							<a href="#">Jimmy Kimmel Live</a>
							<small>
								<span class="related-item__year">2003</span>
								&bull;
								<span class="related-item__tags">Talk, Comedy</span>
							</small>
						</h4>

						<div class="related-item__action">
							<div class="add-to-my-shows">
								<!-- @TODO: implement Add To My Shows as React component for code reuse -->
								<button class="btn btn-primary pull-right">
									<i class="fa fa-heart-o"></i>

									<!-- template implemention so as to not break styling -->
									<span class="toggle-label">Add To My Shows</span>
								</button>
							</div>
						</div>

					</div>



					<p class="related-item__description">
						Celebrity guests and comedy skits.
					</p>

				</div>

			</div><!-- related-item -->

			*/ ?>

			<?php zap_post_navigation(); ?>
<hr class="bold">
			<div class="related-item">
<?php
// Displaying two Taboola widgets
tribune_display_taboola_widget( 'taboola-below-article-thumbnails' );
?> <hr class="bold"> <?php
tribune_display_taboola_widget( 'zap-taboola-below-article-text-links' );
?>
			</div>

		</div><!-- related-items -->
	</footer><!-- .entry-footer -->

</article><!-- #post-## -->
