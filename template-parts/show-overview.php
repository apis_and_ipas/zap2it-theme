<div class="show-overview">
    <div class="upcoming-episodes hidden">
        <header class="upcoming-episodes__header">
            <h4>Upcoming Episodes</h4>

            <a class="btn btn-primary" href="<?php the_permalink(); ?>upcoming-episodes">See all upcoming episodes</a>
        </header>

        <div class="upcoming-episodes__body">
            <!-- TODO: implement react components for episodes listings -->
        </div>
    </div>
    <div class="related-news show-related-news show-overview">

        <header class="related-news__header">
            <h4>Related News</h4>
        </header>

        <div class="related-news__body">

            <?php $i = 0; ?>
            <div class="row">
            <?php
            if ( ! have_posts() ) {
                echo '<div class="related-news__item"><p>No related news available</p></div>';
            } else {
                while ( have_posts() ): the_post();
                    get_template_part( 'template-parts/content', 'related-news' );

                if ( ++$i % 2 == 0 )
                    echo '</div><div class="row">';

                endwhile;
            }
?>
            </div>

        </div>
    </div>

</div>
