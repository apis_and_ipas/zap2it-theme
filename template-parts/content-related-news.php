        <div class="related-news__item">
            <a href="<?php the_permalink() ?>">
                <?php the_post_thumbnail( 'thumb-360-202' ) ?>
                <div>
                    <?php the_title() ?>
                </div>
            </a>
        </div>
