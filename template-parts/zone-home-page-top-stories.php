<?php
global $post;

$zone_query = z_get_zone_query( 'home-page-top-stories', array(
	'posts_per_page' => 9,
	'force_posts_per_page' => true,
	'no_found_rows' => true
) );

?>
			<section class="page-home__hero">
				<div class="row">

				<?php
				$post = $zone_query->posts[0];
				setup_postdata( $post );
				?>

					<div class="article-hero col-md-6" data-tb-region="Top Story">
						<div data-tb-region-item>
						<?php zap_print_extra_for_module() ?>
							<a href="<?php the_permalink() ?>">
								<?php the_post_thumbnail( 'thumb-360-202' ); ?>
								<h3>
									<?php the_title() ?>
								</h3>
							</a>
						</div>
					</div>

					<div class="article-hero-list col-md-6" data-tb-region="Top Stories Small">
						<ul>
						<?php
						for ( $i = 1; $i < 5; $i++ ) {
							// Bail, we don't have any posts left
							if ( ! $zone_query->posts[$i] )
								break;

							$post = $zone_query->posts[$i];
							setup_postdata( $post );
							get_template_part( 'template-parts/content-zone-hero-li' );
						} ?>
						</ul>
					</div>
				</div>

			</section>

			<hr/>


			<div class="row" data-tb-region="Top Stories Row 1">

		<?php
		$r = 2;

		for ( $i = 5; $i < count( $zone_query->posts ); $i++ ):
			$post = $zone_query->posts[$i];
			setup_postdata( $post );
		?>

				<div class="article-item col-xs-6" data-tb-region-item>
					<?php get_template_part( 'template-parts/content-article-item-inner' ); ?>
				</div>
		<?php if ( $i % 2 === 0 ):
			 ?>
			</div>
			<div class="row" data-tb-region="Top Stories Row <?php echo (int) $r++; ?>">
			<?php endif ?>

				<?php endfor ?>
			</div>

<?php

wp_reset_postdata();
