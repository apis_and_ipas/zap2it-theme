<?php
// Jetpack Related Posts
$posts = tribune_get_related_posts_for_queried_object( 10 );

if ( ! $posts )
	return;

?>
<div class="container related-stories">
<?php
$i = 0;

?>
<div class="row">
<?php
	foreach( $posts as $post ) {
		setup_postdata( $post );
		get_template_part( 'template-parts/content', 'grid-generic' );

	 if ( ++$i % 5 == 0 ) {
	 	get_template_part( 'template-parts/ad', '300x250-btf' );

	 	echo '</div><div class="row">';
	 	$i = 0;
	 	continue;
	 }

		if ( $i % 3 === 0 )
			echo '</div><div class="row">';
	}
?> </div>
</div>