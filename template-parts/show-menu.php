<ul class="nav nav-tabs nav-justified">
<?php
/**
 * Tab-menu items
 */
$show_menu_items = array(
    'Overview',
    'Upcoming Episodes',
    'Episode Guide',
    'Cast &amp; Crew',
    'Related News'
);

function zap_slugify( $item ) {
    return sanitize_title_with_dashes( $item );
}

$show_menu_items_slugs = array_map( 'zap_slugify', $show_menu_items );


function zap_set_active( $item_slug ) {
    //get the last key, which should be our tab segment
    $current_tab = get_query_var( 'show_tab' );

    if ( !$current_tab &&  $item_slug === 'overview' ) {
        echo 'active';
    } else if ( $current_tab === $item_slug ) {
            echo 'active';
        }
}

/**
 *  Build the tabbed menu
 */
foreach ( $show_menu_items as $key => $menu_item ):
    $item_slug = sanitize_title_with_dashes( $menu_item );
?>
        <li class="<?php zap_set_active( $item_slug ); ?>">
            <?php /* this outputs a url like http://zap2it.com/show/show-name/tab-name */ ?>
            <a href="<?php the_permalink(); echo esc_attr( $item_slug );?>">
                <?php echo esc_html__( $menu_item, 'zap' );?>
            </a>
        </li>
    <?php endforeach; ?>

    <?php zap_set_active( $menu_item ); ?>

</ul>
