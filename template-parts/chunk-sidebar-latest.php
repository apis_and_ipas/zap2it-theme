	<aside id="recent-posts-3" class="widget widget_recent_entries">
		<h1 class="widget-title">Latest</h1>
		<ul>

		<?php
		// 4 latest posts
		$query = new WP_Query( array( 'ignore_sticky_posts' => true, 'posts_per_page' => 4 ) );
		?>
			<?php if ( $query->have_posts() ) : ?>
				<?php while ( $query->have_posts() ) : $query->the_post(); ?>

					<?php get_template_part( 'template-parts/content-zone-hero-li' ); ?>
				<?php endwhile; ?>

			<?php else : ?>

				<li><small>No latest posts</small></li>

			<?php endif; ?>
		</ul>
		<div class="ad text-center padding-bottom">
			<?php do_action( 'acm_tag', '300x250-btf' ); ?>
		</div>
	</aside>