							<li data-tb-region-item>
								<div class="media">
									<div class="media-left">
									    <a href="<?php the_permalink() ?>">
									    <?php the_post_thumbnail( 'thumb-100-55', array( 'class' => 'media-object' ) ) ?>
									    </a>
									</div>
									<div class="media-body">
										<a href="<?php the_permalink() ?>">
											<div class="media-heading"><?php the_title() ?></div>
										</a>
									</div>
								</div>
							</li>