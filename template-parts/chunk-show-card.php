<?php
$banner = wp_get_attachment_image_src( zap_get_post_meta( 'banner' ), 'thumb-900-200' );
?>
			<div class="related-item" style="background: url(<?php echo esc_url( $banner[0] ) ?>);">

			  	<div class="related-item__img col-md-3">
			    	<a href="<?php the_permalink() ?>">
			    	<?php the_post_thumbnail( 'thumb-165-120', [ 'class' => 'responsive-img' ] ); ?>
			    	</a>
			  	</div>

			  	<div class="related-item__content col-md-9">

					<div class="related-item__header">
						<h4 class="related-item__title pull-left">
							<a href="<?php the_permalink() ?>"><?php the_title() ?></a>
							<small>
								<span class="related-item__year"><?php  echo esc_html( zap_get_post_meta( 'release_year' ) ); ?></span>

								<span class="related-item__tags"><?php the_terms( $post->ID, 'genre', ' &bull; ' ); ?></span>
							</small>
						</h4>

						<div class="related-item__action">
							<div class="add-to-my-shows">

							</div>
						</div>

					</div>



					<p class="related-item__description">
						<?php the_content() ?>
					</p>

				</div>

			</div>