<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package Tribune Media Zap2it
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if ( !( is_page( 'tv-listings-new' ) || is_page( 'tv-listings' ) || is_page( 'new-tonight' ) || is_page( 'my-shows') || is_page( 'account-settings' ) ) ): ?>
        <header class="entry-header">

            <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
        </header><!-- .entry-header -->
    <?php endif; ?>

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'zap' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php edit_post_link( esc_html__( 'Edit', 'zap' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
