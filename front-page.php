<?php
/**
 * The Home page template file.
 * @package Tribune Media Zap2it
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<?php get_template_part( 'template-parts/zone', 'home-page-top-stories' ); ?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar( 'front-page' ); ?>

<div class="clearfix"></div>


</div><!-- close main container-->

<?php
$zone_query = z_get_zoninator()->get_zone_query( 'features-block-home-page', array( 'posts_per_page' => 4 ) );
if ( $zone_query->have_posts() ) : ?>

<section class="page-home__featured well">
	<div class="container">
		<h3>Featured</h3>

		<div class="row" tb-data-region="Featured">

		<?php while ( $zone_query->have_posts() ):
			$zone_query->the_post();
		?>
			<div class="col-xs-6 col-sm-3" data-tb-region-item>
				<a href="<?php the_permalink() ?>">
					<?php the_post_thumbnail( 'thumb-265-150' ); ?>
					<div>
						<?php the_title(); ?>
					</div>
				</a>

			</div>
			<?php endwhile; ?>
		</div><!-- /.row -->
	</div>
</section> <!-- / .page-home__featured -->

<?php endif ?>

<!-- reopen main container -->
<div class="container" id="infinite-stories">
<?php zap_infinite_scroll_render() ?>

<?php get_footer();
