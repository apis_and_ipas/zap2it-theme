<?php
/**
 * Tribune Media Zap2it functions and definitions
 *
 * @package Tribune Media Zap2it
 */

define( 'ZAP_VERSION', '0.0.1' );
define( 'ZAP_INC_DIR', __DIR__ . '/inc/' );

//Init WP.com VIP environment
require_once WP_CONTENT_DIR . '/themes/vip/plugins/vip-init.php';


// VIP Shared Plugins
wpcom_vip_enable_opengraph();
wpcom_vip_load_plugin( 'co-authors-plus' ); // Sandbox loads dev version of the plugin
wpcom_vip_load_plugin( 'co-authors-plus-social-pack' );
wpcom_vip_load_plugin( 'wpcom-thumbnail-editor' );
wpcom_vip_load_plugin( 'ooyala', 'plugins', true );
wpcom_vip_load_plugin( 'zoninator' );
wpcom_vip_load_plugin( 'add-meta-tags-mod' );
wpcom_vip_load_plugin( 'ad-code-manager' );
wpcom_vip_load_plugin( 'wp-large-options' );
wpcom_vip_load_plugin( 'expiring-posts' );
wpcom_vip_load_plugin( 'hidden-posts' );
wpcom_vip_load_plugin( 'safe-redirect-manager' );
wpcom_vip_load_plugin( 'fieldmanager' );
wpcom_vip_load_plugin( 'disable-comments-query' );

/**
 * Elasticsearch
 */
wpcom_vip_load_plugin( 'wpcom-elasticsearch' );


/**
 * Tribune Media shared plugins
 */
wpcom_vip_load_plugin( 'shared', 'tribune-plugins' );

/**
 * Ad Code Manager config
 */
wpcom_vip_load_plugin( 'acm-config', 'tribune-plugins' );

/**
 * Tribune Media Wire syndication plugin
 */
wpcom_vip_load_plugin( 'tribune-media-wire', 'tribune-plugins' );

// Admin only plugins
if ( current_user_can( tribune_get_global_admin_cap() ) ) {
	add_filter( 'pmi_view_cap', 'tribune_get_global_admin_cap' );
	wpcom_vip_load_plugin( 'post-meta-inspector' );
}

wpcom_vip_load_plugin( 'trib-migrate', 'tribune-plugins' );

/**
 * Facebook Trends Dashboard Widget
 */
wpcom_vip_load_plugin( 'facebook-trends-widget', 'tribune-plugins' );

/**
 * Custom template tags for this theme.
 */
require ZAP_INC_DIR . 'template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require ZAP_INC_DIR . 'extras.php';

/**
 * Load Jetpack compatibility file.
 */
require ZAP_INC_DIR . 'jetpack.php';

/**
 *   Bootstrap 3 Walker Class
 */
require_once  ZAP_INC_DIR . 'lib/bootstrap-walker.php';

/**
 *   Add Shows CPT
 */
require_once  ZAP_INC_DIR . 'shows.php';

/**
 *   Add Galleries CPT
 */
require_once  ZAP_INC_DIR . 'galleries.php';

/**
 * Custom Taxonomies
 */
require_once ZAP_INC_DIR . 'taxonomies.php';

/**
 * Fieldmanager config
 */
require_once ZAP_INC_DIR . 'fieldmanager-config.php';


/**
 * Redirects
 */
require_once ZAP_INC_DIR . 'redirects.php';

/**
 * Pixels, beacons, and such
 */
require_once ZAP_INC_DIR . 'tracking.php';

/**
 * TMS API client
 */
require_once ZAP_INC_DIR . 'lib/tms.php';
/**
 * CLI Command
 */
require_once ZAP_INC_DIR . 'cli/zap-command.php';

/**
 * Parse integration
 */
wpcom_vip_load_plugin( 'wp-parse-com', 'doejo-plugins' );

// Disable Livechat
wpcom_vip_remove_livechat();

// Make permalinks match legacy urls
wpcom_vip_load_permastruct( '/%year%/%monthnum%/%postname%/' );
/**
 * // end bootstrap
 */

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function zap_setup() {

	/*
 * Make theme available for translation.
 * Translations can be filed in the /languages/ directory.
 * If you're building a theme based on Tribune Media Zap2it, use a find and replace
 * to change 'zap' to the name of your theme in all the template files
 */
	load_theme_textdomain( 'zap', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
 * Let WordPress manage the document title.
 * By adding theme support, we declare that this theme does not use a
 * hard-coded <title> tag in the document head, and expect WordPress to
 * provide it for us.
 */
	add_theme_support( 'title-tag' );

	/**
	 * Add Thumnail support
	 */
	add_theme_support( 'post-thumbnails' );

	/**
	 * Thumbnail sizes
	 *
	 * NB: all sizes should conform to following convention:
	 * thumb-{width}-{height}
	 */
	add_image_size( 'thumb-360-202', 360, 202, true );
	add_image_size( 'thumb-100-55', 100, 55, true );
	add_image_size( 'thumb-265-150', 265, 150, true );
	add_image_size( 'thumb-150-100', 150, 100, true );
	add_image_size( 'thumb-165-120', 165, 120, true );
	add_image_size( 'thumb-1600-530', 1600, 530, true );
	add_image_size( 'thumb-900-200', 900, 200, true );

	add_image_size( 'thumb-0-432', 0, 432, true );
	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
			'primary' => esc_html__( 'Primary Menu', 'zap' ),
			'footer' => esc_html__( 'Footer Menu', 'zap' ),
			'tv-listings' => esc_html__( 'TV Listings Tabs', 'zap' ),

		) );

	/*
 * Switch default core markup for search form, comment form, and comments
 * to output valid HTML5.
 */
	add_theme_support( 'html5', array(
			'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
		) );

}
add_action( 'after_setup_theme', 'zap_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */

add_action( 'after_setup_theme', function() {
		$GLOBALS['content_width'] = apply_filters( 'zap_content_width', 769 );
	}, 0 );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function zap_widgets_init() {
	register_sidebar( array(
			'name'          => esc_html__( 'Sidebar', 'zap' ),
			'id'            => 'sidebar-1',
			'description'   => '',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h1 class="widget-title">',
			'after_title'   => '</h1>',
		) );
}
add_action( 'widgets_init', 'zap_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function zap_scripts() {
	wp_enqueue_style( 'font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css', array(), ZAP_VERSION, 'all' );
	wp_enqueue_style( 'zap-style', get_template_directory_uri() . '/assets/css/styles.css' );

	/**
	 * Require dependencies for React  components
	 */
	wp_enqueue_script( 'backbone' );
	wp_enqueue_script( 'bootstrap-js', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js', array( 'jquery' ), ZAP_VERSION, true  );
	wp_enqueue_script( 'zap2it-bundle', get_template_directory_uri() . '/assets/js/bundle.js',  array( 'backbone' ), ZAP_VERSION, true );

}
add_action( 'wp_enqueue_scripts', 'zap_scripts' );


/**
 * Filter to put a placeholder in, when there's no thumbnail set for post (which is the case for a lot of legacy posts)
 */
add_filter( 'post_thumbnail_html', function( $html, $post_id, $post_thumbnail_id, $size, $attr ) {
		// We have a thumb, return it
		if ( $html )
		return $html;

		$attr = wp_parse_args( $attr, [ 'class' => '' ] );

		$dim = zap_get_image_size_dimensions( $size );

		$style_string = '';

		foreach ( $dim as $key => $value ) {
			if ( $key == 'crop' || ! $value )
			continue;

			$style_string .= "max-{$key}: 100%; ";
		}


		return sprintf( '<img src="%s" class="%s" style="%s">',
			esc_url( get_stylesheet_directory_uri() . '/assets/images/sprites/zap2it-placeholder.png' ),
			esc_attr( $attr[ 'class' ] ),
			esc_attr( $style_string ) );
	}, 10, 5 );


function zap_get_image_size_dimensions( $image_size ) {
	global $_wp_additional_image_sizes;

	return isset( $_wp_additional_image_sizes[$image_size] ) ? $_wp_additional_image_sizes[$image_size] : [];
}

/**
 * Get one of fieldmanager options
 *
 * @param string  $slug option key
 * @return mixed        option value
 */
function zap_get_option( $slug ) {
	$allopts = get_option( 'zap2it_options' );
	return isset( $allopts[$slug] ) ? $allopts[$slug]: '';
}

add_filter( 'oembed_dataparse', 'zap_oembed_dataparse_hulu', 10, 3 );
function zap_oembed_dataparse_hulu( $return, $data, $url ) {

	// Bail if it's not a hulu shortcode
	if ( ! wp_in( 'hulu.com/embed.html', $return ) )
		return $return;

	// Grab an iframe src
	preg_match( '/.*src="([^"]+)".*/i', $return, $matches );

	// Append partner to the url and return it
	return str_replace( $matches[1], add_query_arg( [ 'partner' => 'zap2it' ], $matches[1] ), $return );
}

add_filter( 'oembed_dataparse', 'zap_oembed_add_wrapper', 15, 3 );
function zap_oembed_add_wrapper( $return, $data, $url ) {
	return '<span class="oembed-wrapper">' . $return . '</span>';
}

/**
 * Add an extra wrapper for any oembed
 */
add_filter( 'embed_oembed_html', function( $html, $url, $attr, $post_ID ) {
		return '<span class="oembed-wrapper">' . $html . '</span>';
	}, 0, 4 );

/**
 *  Remove default display positions for Sharing buttons
 */
add_action( 'loop_start', function() {
		remove_filter( 'the_content', 'sharing_display', 19 );
		remove_filter( 'the_excerpt', 'sharing_display', 19 );
		if ( class_exists( 'Jetpack_Likes' ) ) {
			remove_filter( 'the_content', array( Jetpack_Likes::init(), 'post_likes' ), 30, 1 );
		}
	} );



add_action( 'admin_init', function () {
		add_editor_style( 'assets/css/editor-styles.css' );
	} );


/**
 * Comscore beacon
 */
add_action( 'wp_footer', function() {
?>
<script>
var _comscore = _comscore || [];
_comscore.push( { c1: "2", c2: "6036462" } );
( function() {
var s = document.createElement( "script"), el = document.getElementsByTagName("script" )[0]; s.async = true;
s.src = ( document.location.protocol == "https:" ? "https://sb" : "http://b" ) + ".scorecardresearch.com/beacon.js";
el.parentNode.insertBefore( s, el );
})();
</script>
<noscript>
<img src="http://b.scorecardresearch.com/p?c1=2&c2=6036462&cv=2.0&cj=1" />
</noscript>
<?php
	} );

/**
 * Override default rel_canonical behavior
 *
 * @return [type] [description]
 */
function zap_rel_canonical() {
	remove_action( 'wp_head', 'rel_canonical' );

	if ( !is_singular() )
		return;

	// Default link
	$link = get_permalink( get_queried_object_id() );

	// Canonical override
	if ( ( $override = zap_get_post_meta( 'canonical' ) ) && filter_var( $override, FILTER_VALIDATE_URL ) ) {
		$link = $override;
	}

	echo '<link rel="canonical" href="' . esc_url( $link ) . "\" />\n";
}
add_action( 'wp_head', 'zap_rel_canonical', 5 );

/**
 * Get fieldmanager post meta field
 *
 * @param string  $key     meta key to fetch
 * @param string  $context defaults to post_meta, but might be any fieldmanager meta group slug
 * @return mixed
 */
function zap_get_post_meta( $key = '', $context = 'post_meta' ) {
	if ( ! $key )
		return '';

	$meta = get_post_meta( get_the_ID(), $context, true );

	if ( isset( $meta[$key] ) )
		return isset( $meta[$key] ) ? $meta[$key] : '';
}


add_filter( 'pre_get_posts', function( $q ) {
		if ( $q->is_main_query() && is_front_page() ) {

			if ( false === ( $exclude = wp_cache_get( 'front_page_exclude', 'front-page' ) ) ) {
				// Exclude hidden posts and posts that are displayed in zoninator zones
				$viral_ids = wp_list_pluck( z_get_posts_in_zone( 'features-block-home-page' ), 'ID' );
				$featured_ids = wp_list_pluck( z_get_posts_in_zone( 'home-page-top-stories', array( 'posts_per_page' => 15 ) ), 'ID' );

				// Make sure we don't override any other previously set post__not_in since this callback runs really late
				$exclude = array_unique( array_merge( $featured_ids, $viral_ids, (array) $q->get( 'posts_per_page' ) ) );
				wp_cache_add( 'front_page_exclude', $exclude, 'front-page', 360 );
			}

			$q->set( 'post__not_in', $exclude );
		}

		return $q;
	}, 20 );

/**
 * ES WP Search runs on priority 5, we need to run earlier
 */
add_filter( 'pre_get_posts', function( $q ) {

	if ( $q->is_search() ) {
		$q->set('post_type', [ 'post', 'show-details' ] );
	}

	return $q;
}, 4 );

/**
 * Users/Caps
 *
 */

/**
 * Modify capabilities needed for management of tags
 */
add_action( 'init', function() {
		$tag_tax = get_taxonomy( 'post_tag' );
		$tag_tax->cap->manage_terms = "manage_tags";
		$tag_tax->cap->edit_terms = "manage_tags";
		$tag_tax->cap->delete_terms = "manage_tags";
	} );


add_filter( 'taboola_loader_url', function( $url ) {
	return '//cdn.taboola.com/libtrc/zap2it/loader.js';
});

/**
 * Appending [gallery] shortcodes for legacy posts
 */
add_filter( 'the_content', function( $content ) {
	if ( ( get_query_var( 'gallery') || has_category( 'photos' ) ) && ! wp_in( '[gallery', $content ) ) {
		$content .= "\n\n" . do_shortcode( '[gallery]' );
	}

	return $content;
}, 15 );


add_filter( 'tribune_taboola_newsroom_id', function() { return 'zap2it'; });


/**
 * Print global Grid settings in footer
 */
add_action( 'wp_footer', function() {
	// Bail if not a Grid page
	if ( ! ( is_page( 'tv-listings' )  || is_page( 'tv-listings-new' ) ) )
		return;
?>

<script>
	window.Zap2itGridOptions = <?php echo wp_json_encode( get_option( 'zap2it_grid_options' ) ); ?>;
</script>

<?php
});

/**
 * Render Nativo/Postrelease JS
 */
add_action( 'wp_head', function() {
	wp_enqueue_script( 'nativo', 'http://a.postrelease.com/serve/load.js?async=true' );
} );



/**
 * ES Query args filter
 */
add_filter( 'wpcom_elasticsearch_query_args', function( $args, $query ) {
		// Modify sort query
		$args['sort'] = array(
			"_score",
			array(
				'post_type' => array( 'order' => 'desc' ) ),
		);

		// Account for guest authors, boost the field 5 times, to ensure more results on first page of search if search term is authors name
		$args['query']['multi_match']['fields'][] = "taxonomy.author.name^5";

		// Fine tune 'fields' arg
		$args['query']['multi_match']['fields'] = array_map( function( $e ) {
			switch ( $e ) {
			case 'title':
				// Boost title
				$e = 'title^5';
				break;
			case 'author':
				$e = 'author^5';
				break;

			default:
			}
			return $e;
		}, $args['query']['multi_match']['fields'] );

		// We'll just nest multi_match query inside function_score
		$args['query']['function_score']['query'] = [ "multi_match" => $args['query']['multi_match'] ];

		unset( $args['query']['multi_match']);

		$args['query']['function_score']['functions'][] = [
			"gauss" => [
				'date' => [
					'origin' => date( 'Y-m-d' ),
					'scale' => '180d',
					'decay' => 0.8
					]
			]
		];

		$args['fields'][] = 'date';
		$args['fields'][] = 'post_type';

		return $args;
	}, 10, 2 );

add_filter( 'wpcom_elasticsearch_wp_query_args', function( $es_wp_query_args, $query ) {
	$es_wp_query_args['post_type'] = [ 'post', 'show-details' ];
	return $es_wp_query_args;
}, 10, 2 );

add_filter( 'wpcom_elasticsearch_found_posts', function( $results ) {
	$show_details = [];
	/**
	 * Show details should be ranked high enough already,
	 * But we need to make sure that they're on top
	 * So we iterate over the hits a move all show details to the beginning
	 *
	 */
	foreach( $results['results']['hits'] as $index => $hit ) {
		if ( 'show-details' != $hit['fields']['post_type'] )
			continue;

		unset( $results['results']['hits'][$index] );

		$show_details[] = $hit;
	}

	$results['results']['hits'] = array_merge( $show_details, $results['results']['hits'] );

	return $results;
} );

// End of ElasticSearch integration
//

/**
 * Filter the taxonomy_template, and use show taxonomy template for movies too
 */
add_filter( 'taxonomy_template', function( $template ) {
	$q = get_queried_object();
	if ( $q->taxonomy == 'movie-title' )
		$template = locate_template( 'taxonomy-show.php');

	return $template;
});

/**
 * Feedback form tweaks - add subject to email,
 * alternate $to based on feedback subject
 */
add_filter( 'contact_form_subject', function( $contact_form_subject, $all_values ) {
	$extra = isset( $all_values['3_Subject'] ) ? sanitize_text_field( $all_values['3_Subject'] ) : '';
	$extra = ' - ' . $extra;
	return $contact_form_subject . $extra;
}, 10, 2 );

add_filter( 'contact_form_to', function( $to ) {

	$subject = array_filter( (array) $_POST, function( $e ) { return wp_in( 'Incorrect Channels/Lineup/Programming', $e ); } );

	if ( $subject ) {
		$to = [ 'feedback@zap2it.com' ];
	} else {
		$to[] = 'contactus@zap2it.com';
	}

	return array_unique( $to );
});