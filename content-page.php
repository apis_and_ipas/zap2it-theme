<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package Tribune Media Zap2it
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php // Don't show page tite on TV Listings pages. ?>
    <?php if ( !( is_page( 'tv-listings-new' ) || is_page( 'tv-listings' ) || is_page( 'new-tonight' ) || is_page( 'my-shows') || is_page( 'account-settings' ) ) ): ?>
        <header class="entry-header">
            <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
        </header><!-- .entry-header -->
    <?php endif; ?>

    <div class="entry-content">
        <?php the_content(); ?>
    </div><!-- .entry-content -->

    <footer class="entry-footer">
        <?php edit_post_link( __( 'Edit', 'zap' ), '<span class="edit-link">', '</span>' ); ?>
    </footer><!-- .entry-footer -->
</article><!-- #post-## -->
