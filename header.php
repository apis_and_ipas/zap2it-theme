<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Tribune Media Zap2it
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
    <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'zap' ); ?></a>

    <header id="masthead" class="site-header" role="banner">
        <nav class="navbar navbar-default">
            <div class="container">

                    <div class="navbar-header">

                        <a class="logo" href="/">
                            <h1 class="sr-only">Zap2It</h1>
                        </a>

                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="navbar-menu">

                        <?php

                            wp_nav_menu( array(
                                'menu'            => 'Primary Menu',
                                'menu_id'         => 'primary-menu',
                                'container'       => false,
                                'container_class' => 'collapse navbar-collapse',
                                'menu_class'      => 'nav navbar-nav',
                                'walker'          => new wp_bootstrap_navwalker()
                             ) );
                        ?>
                        <div id="show-movie-search" data-react-mount="ShowMovieSearch"></div>

                        <div class="header-right ">
                            <?php if ( is_page( 'tv-listings') || is_page( 'tv-listings-new') || is_page( 'new-tonight' ) || is_page( 'my-shows' ) ): ?>
                            <div id="user-actions" data-react-mount="UserAccountMenu"></div>
                            <?php endif; ?>
                            <div id="universal-modal" data-react-mount="UniversalModal"></div>
                        </div>

                    </div><!-- /.navbar-menu -->




            </div><!-- /.container-fluid -->
        </nav>

    </header><!-- #masthead -->

    <?php if ( ! is_singular( 'show' ) ): ?>
        <?php get_template_part( 'template-parts/ad_bar' ) ?>
    <?php endif; ?>

    <div id="content" class="container site-content">

        <?php
            // Only load lineup bar partial on TV Listings pages.
            if ( is_page( 'tv-listings') || is_page( 'tv-listings-new') || is_page( 'new-tonight' ) || is_page( 'my-shows' ) )  {
                get_template_part( 'template-parts/tv-listings', 'menu' );
            }
        ?>
