<?php
/**
 * The template for displaying all single posts.
 *
 * @package Tribune Media Zap2it
 */

get_header(); ?>


	<?php while ( have_posts() ) : the_post(); ?>

	<header class="entry-header content-area full-width">
		<h1 class="entry-title"><!-- @Title --></h1>
			<div class="entry-meta">
						<span class="byline">POSTED <!-- @Datetime --> , SPONSORED BY <!-- @Author --> <!-- @AuthorLogo --></span>
						</div>
		<!-- .entry-meta -->


	</header><!-- .entry-header -->

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		/**
		 * @package Tribune Media Zap2it
		 */
		?>



		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<div class="entry-content">
				<!-- @Content -->
			</div><!-- .entry-content -->


		</article><!-- #post-## -->




		</main><!-- #main -->
	</div><!-- #primary -->

	<?php endwhile; // end of the loop. ?>

<?php get_sidebar(); ?>
<?php get_template_part( 'template-parts/chunk-related-stories' ); ?>
<?php get_footer();
