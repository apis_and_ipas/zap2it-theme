<?php
/**
 * The template for displaying a single show
 *
 * @package Tribune Media Zap2it
 */

get_header(); ?>
</div><!-- close .container opened in header-->
	<?php while ( have_posts() ) : the_post();
		$banner =  wp_get_attachment_image_src( zap_get_post_meta( 'banner' ), 'thumb-1600-530' );
	?>

	<header class="single-show__banner" style="background: url(<?php echo esc_url( $banner[0] ) ?>) 50% 50%; ">

		<div class="single-show__banner-ad">
			<a href="#" class="visible-xs-block"><img src="http://placehold.it/320x50&text=320x50" alt="" /></a>
			<a href="#" class="hidden-xs"><img src="http://placehold.it/980x30&text=980x30" alt="" /></a>
		</div>

		<div class="single-show__banner-inner">

			<div class="container">
				<div class="row">
					<div class="single-show__thumbnail  col-xs-4 col-sm-3">
						<?php the_post_thumbnail(); ?>
					</div>

					<div class="single-show__info col-xs-8 col-sm-9">

						<?php the_title( '<h1 class="single-show__title">', '</h1>' ); ?>

						<div class="add-to-my-shows">
							<!-- @TODO: implement Add To My Shows as React component for code reuse -->
							<button class="btn btn-primary">
								<i class="fa fa-heart-o"></i>

								<!-- template implemention so as to not break styling -->
								<span class="toggle-label">Add To My Shows</span>
							</button>
						</div>

						<div class="clearfix"></div>

						<div class="single-show__meta">
							2014 &bull; Sitcom
						</div><!-- .single-show__meta -->

						<div class="single-show__content">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
			</div>


		</div>

		<div class="single-show__menu">
			<div class="container">

				<?php get_template_part( 'template-parts/show', 'menu') ?>
				
			</div>
		</div>
	</header>

	<div class="container">
		<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">

				<?php zap_display_show_tab(); ?>

			</main><!-- #main -->
		</div><!-- #primary -->

		<?php get_sidebar(); ?>
	</div>

	<?php endwhile; // end of the loop. ?>

<?php get_footer();
