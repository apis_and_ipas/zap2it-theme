<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Tribune Media Zap2it
 */
?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="container site-info">

			<div class="row">
				<div class="col-md-10">
					<?php
						wp_nav_menu( array(
							'menu'          => 'Footer Menu',
							'menu_id'       => 'footer-menu',
							'menu_class'    => 'nav nav-pills',
							'container'     => false,
							'items_wrap'    => zap_footer_nav_wrap()
						) );
					?>
				</div>

				<div class="col-md-2">
					<?php
						/**
						 * @TODO: add social media links URLs
						 */
					?>
					<ul class="social-media-links">
						<li><a href="<?php echo esc_url( zap_get_option( 'facebook' ) ) ?>"><i class="fa fa-facebook-square"></i></a></li>
						<li><a href="<?php echo esc_url( zap_get_option( 'twitter' ) ) ?>"><i class="fa fa-twitter"></i></a></li>
						<li><a href="<?php echo esc_url( zap_get_option( 'googleplus' ) ) ?>"><i class="fa fa-google-plus"></i></a></li>
						<li><a href="<?php echo esc_url( zap_get_option( 'pinterest' ) ) ?>"><i class="fa fa-pinterest"></i></a></li>
					</ul>
				</div>
			</div>

		</div><!-- .site-info -->
	</footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
