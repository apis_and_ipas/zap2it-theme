<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package Tribune Media Zap2it
 */
get_header(); ?>

	<header class="entry-header content-area full-width">
		<h1 class="entry-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'zap' ); ?></h1>
	</header><!-- .entry-header -->

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'zap' ); ?></p>

			<?php get_search_form(); ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer();