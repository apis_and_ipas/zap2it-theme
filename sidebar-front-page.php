<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package Tribune Media Zap2it
 */
?>

<div id="secondary" class="widget-area" role="complementary">

<?php // TODO: apply proper Bootstrap classes ?>
	<aside class="widget">
		<div class="ad text-center padding-bottom">
			<?php do_action( 'acm_tag', '300x250-atf' ); ?>
		</div>
	</aside>

	<?php get_template_part( 'template-parts/chunk-sidebar', 'popular' ); ?>

</div><!-- #secondary -->
