<?php
/**
 * The template for displaying a single show
 *
 * @package Tribune Media Zap2it
 */

get_header(); ?>
</div><!-- close .container opened in header-->
	<?php
		$show_details =  new WP_Query( [
 				'name' => get_queried_object()->slug,
				'post_type' => [ 'show-details' ],
				'posts_per_page' => 1
			] );

		$show_details->the_post();

		$banner =  wp_get_attachment_image_src( zap_get_post_meta( 'banner' ), 'thumb-1600-530' );

	?>

	<header class="single-show__banner" style="background: url(<?php echo esc_url( $banner[0] ) ?>) 50% 50%; ">

		<div class="single-show__banner-inner">

			<div class="container">
				<div class="row">
					<div class="single-show__thumbnail  col-xs-4 col-sm-3">
						<?php the_post_thumbnail(); ?>
					</div>

					<div class="single-show__info col-xs-8 col-sm-9">

						<?php the_title( '<h1 class="single-show__title">', '</h1>' ); ?>
						<?php if ( is_tax( 'show' ) ): ?>
						<div class="add-to-my-shows">
							<!-- @TODO: implement Add To My Shows as React component for code reuse -->
							<button class="btn btn-primary">
								<i class="fa fa-heart-o"></i>

								<!-- template implemention so as to not break styling -->
								<span class="toggle-label">Add To My Shows</span>
							</button>
						</div>
						<?php endif ?>

						<div class="clearfix"></div>

						<div class="single-show__meta">
							<?php  echo esc_html( zap_get_post_meta( 'release_year' ) ); ?> <?php the_terms( $show_details->ID, 'genre', '&bull;' ); ?>
						</div><!-- .single-show__meta -->

						<div class="single-show__content">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
			</div>


		</div>

		<div class="single-show__menu">
			<div class="container">

			</div>
		</div>
	</header>

	<div class="container">
		<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">

				<?php zap_display_show_tab(); ?>

			</main><!-- #main -->
		</div><!-- #primary -->

		<?php get_sidebar(); ?>
	</div>

<?php get_footer();
